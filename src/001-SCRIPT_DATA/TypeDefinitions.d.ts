/* eslint-disable @typescript-eslint/no-unused-vars */

declare namespace App.Data {
	interface NPCDesc {
		name: string;
		mood: number;
		dailyMood: number;
		lust: number;
		dailyLust: number;
		gender: Gender;
		title: string;
		longDesc: string;
		location: DynamicText;
		briefDesc?: string;
		store?: string;
	}

	interface StoreInventoryItem {
		category: Items.Rarity.Common | Items.Rarity.Rare;
		type: Items.Category;
		qty: number;
		max: number;
		price: number;
		mood: number;
		lock: 0 | 1;
		tag: string;
	}

	interface StoreDesc {
		name: string;
		open: DayPhase[];
		restock: number;
		maxRares?: number;
		unlockingQuest?: string;
		/**
		 * @default QuestStatus.Completed
		 */
		unlockingQuestStatus?: QuestStatus;
		inventory: StoreInventoryItem[];
	}

	// #region Synergy
	interface SynergyElementChoice<T extends StatTypeStr> {
		type: T;
		name: StatTypeStrMap[T];
		bonus: number;
	}

	type BodySynergyElement = SynergyElementChoice<Stat.Body>;

	type SynergyElement = SynergyElementChoice<"body"> |
		SynergyElementChoice<"skill"> |
		SynergyElementChoice<"core">;

	type SkillSynergyData = Partial<Record<Skills.Any | CoreStat, SynergyElement[]>>;

	//#endregion

	interface StatLevelingRecord {
		cost: number;
		step: number;
	}
	interface StatLevelingRecordAdjective {
		adjective: string;
	}

	type MinimalStatLevelingData = Record<number, StatLevelingRecordAdjective>;

	interface NoneStatLevelingData {
		none: {cost: 0, step: 0}
	}

	interface FixedStatLevelingRecord {
		cost: number;
		step: number;
	}

	interface FixedStatLevelingData {
		fixed: FixedStatLevelingRecord;
	}

	type ExplicitStatLevelingRecord = StatLevelingRecord & StatLevelingRecordAdjective;

	type ExplicitStatLevelingData = Record<number, ExplicitStatLevelingRecord>;

	type LevelingCostFunction = (level: number) => number;
	type LevelingDataAny = MinimalStatLevelingData | ExplicitStatLevelingData | FixedStatLevelingData | NoneStatLevelingData

	type StatLevelingRecordAny = StatLevelingRecord | ExplicitStatLevelingRecord | StatLevelingRecordAdjective;

	export interface StatConfig {
		min: number;
		max: number;
		levelingCost?: (level: number) => number;
		leveling: LevelingDataAny;
		/**
		 * Smaller values are better when the stat is inverted.
		 */
		inverted?: boolean;
	}

	export interface BodyStatConfig extends StatConfig {
		cmMin: number;
		cmMax: number;
	}

	export interface SkillStatConfig extends StatConfig {
		altName?: string;
	}

	export interface CoreStatConfig extends StatConfig {
	}

	interface StatConfigMap {
		[Stat.Core]: CoreStatConfig;
		[Stat.Skill]: SkillStatConfig;
		[Stat.Body]: BodyStatConfig;
	}

	interface StatConfigStrMap {
		'core': CoreStatConfig;
		'skill': SkillStatConfig;
		'body': BodyStatConfig;
	}

	//#region Events

	type EventChecker = (player: Entity.Player) => boolean;

	interface EventDesc {
		/** A unique ID for the event. */
		id: string;
		/** The passage you are traversing from */
		from: string;
		force?: boolean;
		/** Number of times the event can repeat, 0 means forever. */
		maxRepeat: number;
		/** Minimum day the event shows up on */
		minDay: number;
		/** Maximum day the event shows up on, 0 means forever */
		maxDay: number;
		/** Interval between repeats on this event. */
		cool: number;
		/** Time phases the event is valid for */
		phase: DayPhase[];
		/** Override passage that the player is routed to. */
		passage: string;
		/** Condition function that is called to check if the event fires. */
		check: EventChecker;
	}
	//#endregion

	interface ModDesc {
		level: number;
		encounter: string;
		name: string;
		symbol: string;
		color: string;
	}

	interface TarotCardDesc {
		name: string;
		css: string;
		chat: string;
		msg: string;
		effects: string[];
	}

	//#region Ratings
	type FullStatStr<Type extends Stat, Values extends string> = `${Type}/${Values}`;

	type FullBodyStatStr = FullStatStr<Stat.Body, BodyStat | DerivedBodyStat>;
	type FullCoreStatStr = FullStatStr<Stat.Core, CoreStat>;
	type FullSkillStatStr = FullStatStr<Stat.Skill, Skills.Any>;
	type FullStatStrAny = FullBodyStatStr | FullCoreStatStr | FullSkillStatStr;
	type FullBodyCoreStatStr = FullBodyStatStr | FullCoreStatStr;

	type RatingFinalValue = string | NonEmptyArray<string>;
	type RatingValue = RatingFinalValue | {[key: number]: RatingValue};
	type Rating = Record<number, RatingValue>;

	interface SimpleRated {
		leveling: Record<number, string>;
	}
	interface FullRated {
		index: FullBodyCoreStatStr[];
		leveling: Rating;
	}

	interface BodyAdjectiveReference {
		rating: FullBodyCoreStatStr[];
		index?: FullBodyCoreStatStr[];
		applicableLevel?: {min: number, max: number}[];
	}

	interface RatedNounConfig {
		adjective: BodyAdjectiveReference;
		noun: RatingFinalValue | SimpleRated | FullRated;
	}
	//#endregion

	type NamingAspect = "adjective" | "noun";
	interface NamingConfig {
		bodyConfig: Record<BodyPart, RatedNounConfig>;
	}
}

/* eslint-enable @typescript-eslint/no-unused-vars */
