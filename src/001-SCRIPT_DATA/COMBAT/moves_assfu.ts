App.Data.Combat.moves[App.Combat.Style.AssFu] = {
	moves: {
		[App.Combat.Moves.AssFu.ShakeIt]: {
			description: "\
        Light attack. These hips don't lie.<br>\
        Dazes enemies.<br>\
        <span style='color:darkred'>DMG LOW</span> \
        <span style='color:darkgoldenrod'>STA AVERAGE</span> \
        <span style='color:cyan'>SPD FAST</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "shakeit_icon",
			stamina: 10,
			combo: 0,
			speed: 10,
			damage: 1.2,
			unlock: function (player) {
				return player.ass >= 30;
			},
			miss: [
				[
					"You attempt shake your pASS ass at NPC_NAME, but miss!",
					"NPC_NAME tries to shake NPC_PRONOUN jiggly ass at you, but misses!",
				],
			],
			hit: [
				[
					"You shake your pASS ass at NPC_NAME!",
					"NPC_NAME NPC_PRONOUN jiggly ass at you!",
				],
			],
		},
		[App.Combat.Moves.AssFu.BootySlam]: {
			description: "\
        Medium attack.<br>\
        <span style='color:darkred'>DMG MEDIUM</span> \
        <span style='color:darkgoldenrod'>STA AVERAGE</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "bootyslam_icon",
			stamina: 10,
			combo: 0,
			speed: 20,
			damage: 1.5,
			unlock: function (player) {
				return player.ass >= 30;
			},
			miss: [
				[
					"You attempt slam NPC_NAME with your pASS booty, but miss!",
					"NPC_NAME tries to slam you with NPC_PRONOUN bouncy booty, but misses!",
				],
			],
			hit: [
				[
					"You slam NPC_NAME with your pASS booty!",
					"NPC_NAME slams you with NPC_PRONOUN bouncy booty!",
				],
			],
		},
		[App.Combat.Moves.AssFu.Twerk]: {
			description: "\
        Light attack. Defense Buff.<br>\
        Recovers combo points after a consumer.<br>\
        <span style='color:darkred'>DMG LOW</span> \
        <span style='color:darkgoldenrod'>STA AVERAGE</span> \
        <span style='color:cyan'>SPD AVERAGE</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "twerk_icon",
			stamina: 10,
			combo: 0,
			speed: 15,
			damage: 1.0,
			unlock: function (player) {
				return player.ass >= 30;
			},
			miss: [
				[
					"You attempt to twerk NPC_NAME with your pASS ass, but miss!",
					"NPC_NAME tries to twerk you with NPC_PRONOUN jiggly ass, but misses!",
				],
			],
			hit: [
				[
					"You twerk NPC_NAME with your pASS ass!",
					"NPC_NAME twerks you with NPC_PRONOUN jiggly ass!",
				],
			],
		},
		[App.Combat.Moves.AssFu.AssQuake]: {
			description: "\
        Medium attack. Modest chance of stun<br>\
        <span style='color:darkred'>DMG MED</span> \
        <span style='color:darkgoldenrod'>STA AVERAGE</span> \
        <span style='color:cyan'>SPD AVERAGE</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "assquake_icon",
			stamina: 10,
			combo: 2,
			speed: 15,
			damage: 1.5,
			unlock: function (player) {
				return player.ass >= 30;
			},
			miss: [
				[
					"You attempt to smother NPC_NAME with your pASS ass, but miss!",
					"NPC_NAME tries to smother you with NPC_PRONOUN jiggly ass, but misses!",
				],
			],
			hit: [
				[
					"You grab the cheeks of your pASS ass and smother NPC_NAME with them!",
					"NPC_NAME grabs the cheeks of NPC_PRONOUN gigantic ass and smothers you!",
				],
			],
		},
		[App.Combat.Moves.AssFu.ThunderBuns]: {
			description: "\
        Strong attack. Good chance of stun<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA AVERAGE</span> \
        <span style='color:cyan'>SPD AVERAGE</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "thunderbuns_icon",
			stamina: 10,
			combo: 3,
			speed: 15,
			damage: 2.0,
			unlock: function (player) {
				return player.ass >= 30;
			},
			miss: [
				[
					"You attempt to leap at NPC_NAME with your pASS ass, but miss!",
					"NPC_NAME tries to leap at you with NPC_PRONOUN jiggly ass, but misses!",
				],
			],
			hit: [
				[
					"You leap at NPC_NAME and crash into them with your pASS ass!",
					"NPC_NAME leaps at you with NPC_PRONOUN gigantic ass and crashes into you!",
				],
			],
		},
		[App.Combat.Moves.AssFu.BunsOfSteel]: {
			description: "\
        Strong attack. Good chance of stun<br>\
        Strong defense buff.<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA AVERAGE</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "bunsofsteel_icon",
			stamina: 10,
			combo: 4,
			speed: 20,
			damage: 2.0,
			unlock: function (player) {
				return player.ass >= 30;
			},
			miss: [
				[
					"You attempt to charge at NPC_NAME with your pASS ass, but miss!",
					"NPC_NAME tries to charge at you with NPC_PRONOUN jiggly ass, but misses!",
				],
			],
			hit: [
				[
					"You charge at NPC_NAME with your pASS ass and send them flying!",
					"NPC_NAME charges at you with NPC_PRONOUN gigantic ass and sends you flying!",
				],
			],
		},
	},
};
