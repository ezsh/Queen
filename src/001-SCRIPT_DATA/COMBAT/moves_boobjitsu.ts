App.Data.Combat.moves[App.Combat.Style.BoobJitsu] = {
	moves: {
		[App.Combat.Moves.BoobJitsu.Jiggle]: {
			description: "\
        Light attack. Distracts enemies.<br>\
        <span style='color:darkred'>DMG LOW</span> \
        <span style='color:darkgoldenrod'>STA LOW</span> \
        <span style='color:cyan'>SPD FAST</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "jiggle_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 5,
			damage: 1.0,
			unlock: function (player) {
				return player.bust >= 30;
			},
			miss: [
				[
					"You jiggle your nBUST at NPC_NAME, but miss!",
					"NPC_NAME jiggles NPC_PRONOUN heaving hooters at you, but misses!",
				],
			],
			hit: [
				[
					"You jiggle your nBUST at NPC_NAME, distracting them!",
					"NPC_NAME jiggles NPC_PRONOUN heaving hooters at you! So distracting…",
				],
			],
		},
		[App.Combat.Moves.BoobJitsu.Wobble]: {
			description: "\
        Light attack. Increases damage.<br>\
        <span style='color:darkred'>DMG LOW</span> \
        <span style='color:darkgoldenrod'>STA MED</span> \
        <span style='color:cyan'>SPD AVERAGE</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "wobble_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.0,
			unlock: function (player) {
				return player.bust >= 30;
			},
			miss: [
				[
					"You wobble your nBUST at NPC_NAME, but miss!",
					"NPC_NAME wobbles NPC_PRONOUN heaving hooters at you, but misses!",
				],
			],
			hit: [
				[
					"You wobble your nBUST at NPC_NAME, slapping NPC_PRONOUN head violently!",
					"NPC_NAME wobbles NPC_PRONOUN heaving hooters at you, slaping you violently!",
				],
			],
		},
		[App.Combat.Moves.BoobJitsu.BustOut]: {
			description: "\
        Medium attack. Increases Accuracy.<br>\
        Restores combo points after a consumer.<br>\
        <span style='color:darkred'>DMG MED</span> \
        <span style='color:darkgoldenrod'>STA HIGH</span> \
        <span style='color:cyan'>SPD AVERAGE</span><br>\
        <span style='color:deeppink'>Combo Builder</span>\
        ",
			icon: "bustout_icon",
			stamina: 15,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.5,
			unlock: function (player) {
				return player.bust >= 30;
			},
			miss: [
				[
					"You swing your nBUST at NPC_NAME, but miss!",
					"NPC_NAME swings NPC_PRONOUN heaving hooters at you, but misses!",
				],
			],
			hit: [
				[
					"You swing your nBUST at NPC_NAME, beating them harshly!",
					"NPC_NAME swings NPC_PRONOUN heaving hooters at you, beating you harshly!",
				],
			],
		},
		[App.Combat.Moves.BoobJitsu.Twirl]: {
			description: "\
        Strong attack. Increases Damage.<br>\
        Restores health comparable to damage done.<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA HIGH</span> \
        <span style='color:cyan'>SPD VERY SLOW</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "twirl_icon",
			stamina: 15,
			combo: 2,
			speed: 20,
			damage: 2.0,
			unlock: function (player) {
				return player.bust >= 30;
			},
			miss: [
				[
					"You spin around, twirling your nBUST at NPC_NAME, but miss!",
					"NPC_NAME spins around, twirling PC_PRONOUN heaving hooters at you, but misses!",
				],
			],
			hit: [
				[
					"You spin around, twirling your nBUST at NPC_NAME, smashing into them with great force!",
					"NPC_NAME spins around, twirling NPC_PRONOUN heaving hooters and smashing into you with great force!",
				],
			],
		},
		[App.Combat.Moves.BoobJitsu.BoobQuake]: {
			description: "\
        Medium attack. Good chance of Stun.<br>\
        <span style='color:darkred'>DMG MED</span> \
        <span style='color:darkgoldenrod'>STA MED</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "boobquake_icon",
			stamina: 10,
			combo: 3,
			speed: 15,
			damage: 1.5,
			unlock: function (player) {
				return player.bust >= 30;
			},
			miss: [
				[
					"You attempt to grab NPC_NAME with your nBUST, but miss!",
					"NPC_NAME tries to grab you with PC_PRONOUN jutting jugs, but misses!",
				],
			],
			hit: [
				[
					"You grab NPC_NAME inbetween your nBUST and then shimmy wildly!",
					"NPC_NAME grabs you between NPC_PRONOUN jutting jugs and then shimmies wildly!",
				],
			],
		},
		[App.Combat.Moves.BoobJitsu.TittyTwister]: {
			description: "\
        Strong attack. Great chance of Stun.<br>\
        <span style='color:darkred'>DMG HIGH</span> \
        <span style='color:darkgoldenrod'>STA VERY HIGH</span> \
        <span style='color:cyan'>SPD SLOW</span><br>\
        <span style='color:deeppink'>Combo Consumer</span>\
        ",
			icon: "tittytwister_icon",
			stamina: 20,
			combo: 4,
			speed: 15,
			damage: 2.5,
			unlock: function (player) {
				return player.bust >= 30;
			},
			miss: [
				[
					"You attempt to smash NPC_NAME with your nBUST, but miss!",
					"NPC_NAME tries to smash you with PC_PRONOUN massive mammaries, but misses!",
				],
			],
			hit: [
				[
					"You grab your nBUST with both hands and then smash wildly at NPC_NAME!",
					"NPC_NAME grabs NPC_PRONOUN massive mammaries with both hands and then smashes wildly at you!",
				],
			],
		},
	},
};
