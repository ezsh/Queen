namespace App.Data.Combat {
	moves[App.Combat.Style.Boobpire] = {
		moves: {
			[App.Combat.Moves.Boobpire.Touch]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 5,
				combo: 0, // Costs no combo points
				speed: 10,
				damage: 1.0,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME reaches for you, but misses!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME lands an icy touch on you, so cold it burns!",
					],
				],
			},
			[App.Combat.Moves.Boobpire.Toss]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0, // Costs no combo points
				speed: 10,
				damage: 1.2,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tries to grab you, but you break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME grabs you and tosses you to the ground!",
					],
				],
			},
			[App.Combat.Moves.Boobpire.Bite]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 2,
				speed: 20,
				damage: 0.5,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tries to bite you, but misses!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME sinks NPC_PRONOUN fangs into your chest!",
					],
				],
			},
			[App.Combat.Moves.Boobpire.Claw]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 20,
				combo: 3,
				speed: 20,
				damage: 2.5,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME flails at you with NPC_PRONOUN claws, but misses!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME rakes you with NPC_PRONOUN deadly claws!",
					],
				],
			},
		},
	}
}
