namespace App.Data {
	const location = 'Port Royale';
	npcs["Georgina"] = {
		name: "Georgina",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME Littlehales, Bar Wench",
		longDesc: "NPC_NAME is an older, but still attractive woman, with long flowing black hair and a truly stupendous bust that is only enhanced \
		by her corseted dress. She can usually be found busily tending to orders, or just casually chatting up a regular.",
		store: "peacock",
	};

	npcs["Gulliver"] = {
		name: "Gulliver",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Master Shipwright",
		longDesc: "A hale and hearty man in his late 30s, NPC_NAME has a broad smile and even broader shoulders. The man is infact so massively \
		built that if you don't know his profession, you'd assume he was a soldier or pirate of some sort.",
	};

	npcs["Blanche"] = {
		name: "Madame Blanche",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Fortune Teller Extraordinaire",
		longDesc: "An older woman dressed like a gypsy, with long curly black hair a pair of gigantic gold hoop earrings. You have no idea if she \
		can really tell the future or not, but she's clearly doing her best to live up to the stereotypes.",
	};

	npcs["Simone"] = {
		name: "Simone",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "'Sultry' NPC_NAME, Madame",
		longDesc: "For a woman in her 40s, NPC_NAME clearly lives up to her nickname of 'Sultry'. She has a fine, voluptuous figure with heaving \
		breasts and a pronounced arse, both of which are barely contained by her flimsy evening gown. Her bright red hair is done up in a bun, \
		which shows off the curve of her slender neck.",
		store: "lustyLass",
	};

	npcs["LustyLassCustomers"] = {
		name: "Lusty Lass Customer", // This is a place holder and not meant to be used as an actual NPC.
		location: location,
		mood: 50,
		dailyMood: -1,
		lust: 100,
		dailyLust: 5,
		gender: Gender.Male,
		title: "Customers of the Lusty Lass",
		longDesc: "A step above the common scum you're used to spreading your arse-cheeks for.",
	};

	npcs["MARKET_STORE"] = {
		name: "Shop Keeper",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 0,
		gender: Gender.Male,
		title: "NPC_NAME of the Great Market",
		longDesc: "Just a generic shop keeper, these guys breed like rabbits.",
		store: "marketStore",
	};

	// CAPTAIN JOHN SWALES, Gentleman Buccaneer - Note:Mood/Lust not adjusted//
	npcs["Swales"] = {
		name: "Captain John Swales",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Gentleman Buccaneer",
		longDesc: "At first glance, this \"Gentleman Buccaneer\" might be mistaken for a highly-ranked noble, though in fact he was a successful privateer, \
		and is now a man of leisure. His attire is meant to display his conspicuous wealth: On his shoulders is a fine, black frock coat with braided \
		lace facings, ornate gold buttons, and generous dog-ear sleeve cuffs fit for a commodore. Beneath his coat are an intricate brocade set of \
		waistcoat and breeches, white stockings, and polished, heeled court shoes. On his head is worn a magisterial, powdered wig that flows below \
		his shoulders, complemented by a dashing, lace-edged cravat under his collar. Lest anyone forget how his wealth was earned, he wears a \
		keen-edged rapier, whose engraved brass basket hilt incorporates scenes from his storied adventures at sea, and a black dragoon pistol, kept \
		close at hand by a belt hook.",
	};

	// CUTCHBART SMITH, Tavern Master - Note:Mood/Lust not adjusted//
	npcs["Cutchbart"] = {
		name: "Cutchbart Smith",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Tavern Master",
		longDesc: "A lean but sturdily framed man, he has the look of one who has seen it all and lived to tell the tale. His powdered, salt-and-pepper \
	natural hair grows to his shoulders, and he sweeps it back by means of a leather cord. His no-nonsense attire is modestly fashionable, but he has \
	the weather-beaten hands and rolling gait of a veteran sea-dog. His lively, colorful manner is well-suited to publican life, and he runs his \
	ordinary with an accomplished hand. Few can best him at filling - or draining - a tankard.",
	};

	// SINDEE RELLA, Mistress of the House - Note:Mood/Lust not adjusted//
	npcs["Sindee"] = {
		name: "Sindee Rella",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Mistress of the House",
		longDesc: "Her lightly rouged, creamy complected face is framed by ringleted onyx locks. Their long curls are stylishly trussed above her head by a ribbon the color of wine splashed with ruby adornments. Her hair spills down to her back and shoulders with precisely careless, spiraling drops. She wears a sangria-hued, mantua gown in shimmering silk with black floral brocade. Its skirts brush the floor and conceal her lower half. Though her dress omits the large hoops so fashionable among high-born ladies, her narrow, corseted waist cuts a mesmerizing, wasp-like figure. Her invisible steps have little rise or fall, creating the illusion that she simply floats across the room as she sees to patrons of the house.",
	};

	// MLLE. JEZEBELLE DE ROUVROY, Hostess of the Ball - Note: Mood/Lust values not adjusted//
	npcs["Jezebelle"] = {
		name: "Mlle. Jezebelle de Rouvroy",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Hostess of the Ball",
		longDesc: "Dressed in too-opulent finery like a self-created duchess, she flits from point to point wearing a pretentious smile. Her tall, golden, ornately-adorned hair is pillared high above her and wrapped in lace and costume jewelery, while her gaudy, ornamental necklace emits far more sparkle than taste. Her rigidly-corseted ball gown ensemble has dramatic, cuffed sleeves, a lacy chemise drawn up in poofs that swallow her upper arms, and elbow-length, floral silk mitts. Her large farthingale petticoat flares to the point of caricature, forcing her to lift her elbows like a fluttering bird wherever she goes. Her face is powdered almost chalk white, except for a stripe of cherry color on her lower lip and a painted beauty mark offset below her nose. Her high-born affectations are obvious at the distance of pistol shot.",
	};

	// BRAM DE VRIES, Curio Dealer - Note: Mood/Lust values not adjusted//
	npcs["Bram"] = {
		name: "Bram De Vries",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Curio Dealer",
		longDesc: "The grizzled shopmaster wears a long, white beard, revealing his advanced age. His snowy hair is still long, though, and tied back at his nape into a ponytail. His stooped torso bears a patchy, broadcloth long-coat embellished in tattered silver piping and patina-soiled buttons. Underneath, he wears an old, loose-fitting shirt, some threadbare venetian breeches, and a pair of dingy, red stockings. A faded, orange sash is proudly tied around his waist, carefully knotted even though its weave has irreparably frayed. His yellowing-but-tidy sailor's kerchief and salt-stained buccaneer's jackboots are further hallmarks of a life spent at sea.",
	};

	// CONSTABLE JONAH BLYTHE, Captain of Militia - Note: Mood/Lust values not adjusted//
	npcs["Blythe"] = {
		name: "Constable Jonah Blythe",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Captain of Militia",
		longDesc: "A brutish, disagreeable man with a coarse, sun-weathered face, his blunted features bear a pugnacious glare even at rest. He is dressed in a battle-scarred leather jerkin with a long doublet underneath, dusty brown breeches and high boots, a loose-knotted cravat and a black tricorne. He forgoes the fancy rapier common to personages of his rank, opting instead to equip the menacing, heavy cutlass hanging at his side. A blunderbuss and a brace of pistols rounds out his equipment, and he wears their bandoleer with a bellicose swagger.",
	};

	// LADY SELENE, Headmistress - Note: Mood/Lust values not adjusted//
	npcs["Selene"] = {
		name: "Lady Selene",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Headmistress",
		longDesc: "A regal lady who strikes an adroit balance of social grace, elegance and raw iron, she takes over the \
    room with her sharp wit and disarming charms. Her beautiful arsenal of body and words has greater potency than the \
    fiercest martial weapon, and many a noble lord or lady has been brought to heel who underestimated her. Her attire \
    is strictly haute couture from the best makers in the world - her every piece of finery selected with matchless \
    refinement and taste. It's rumored her private treasury may rival the governor's, though her real influence and \
    reach are as closely guarded secrets as her feelings. While she (and everyone who knows her) styles herself 'Lady,' \
    even her true name is a mystery, and she knows the value of information.",
		store: "selene",
	};

	// VERONICA ÍÑIGUEZ DE LA VEGA, Finery Dealer by Appointment of Her Royal Majesty - Note: Mood/Lust values not adjusted//
	npcs["Veronica"] = {
		name: "Veronica Íñiguez De La Vega",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Finery Dealer by Appointment of Her Royal Majesty",
		longDesc: "Her slight features and a fragile-seeming build might lead one to dismiss this clothier as just another daughter born into wealth. That assumption would be a mistake. Though she dresses the part of her position in a formal gown - a slim-fitting black mantua detailed with intricate ebony lace, golden florals and complex needlework - she slings a jeweled scimitar and a masterwork flintlock pistol from a belt about her waist. Her petticoats and skirts, though compliant with the social mores of her station, have almost no restrictive hooping, and allow both freer movement and the rare glimpse of the slouch boots she wears beneath. Her holdings are not limited to this famous shop, either - three of her merchantmen anchor in Port Royale's harbor.",
	};

	// MASTER FRANCIS JAMESON, Bespoke Merchant - Note: Mood/Lust values not adjusted//
	npcs["Jameson"] = {
		name: "Master Francis Jameson",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Bespoke Merchant",
		longDesc: "A stout but finely-dressed nobleman, this eminent ship master's attire is suited to his station as a landed merchant. He wears a generous, wine-colored velvet gown, draped over his garnet waistcoat in the 'Persian' fashion with matching petticoat breeches reaching just past his knees. A white falling band in extravagant needlepoint graces his neck, a formal and conservative touch, matched by his fitted court shoes. A long wig cloaks his shoulders with rich chestnut curls like a lion's mane.",
	};

	// BARBARY BILL PADGETT, Jailer - Note: Mood/Lust values not adjusted//
	npcs["Barbary"] = {
		name: "Barbary Bill Padgett",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Jailer",
		longDesc: "A half-toothless ogre of a man with a temper to rival the devil's own, his well-known nickname and fearsome reputation come from his pirating days. It's said that he joined up with a buccaneer crew off the Barbary Coast, collecting tribute for the great pashas of the northern deserts. In action against a royal frigate, he leaped aboard the enemy ship, cut his way through twenty able seamen, threw their captain straight off the forecastle and into the ocean, and took the ship for a prize himself. When his own ship's powder keg exploded moments later, most of his crew was lost, but he rounded up both sides' survivors and made the lot sail him home. How he wound up a jailer is a mystery to everyone except him, but it's rumored he was chosen for being the most contemptible bastard in the whole Caribbean - they either had to put him in charge of the jail, or be damned to try and keep him in it.",
	};

	// Bottlecork Lane NPC's
	npcs["Emily"] = {
		name: "Emily Ligget",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Food Vendor",
		longDesc: "Thickset and quick-tempered, she channels her obvious disdain for the world into thrusting the sausages, loaves and other hand foods she sells into the fires of her portable clay oven. A large, waxed canvas apron protects her plain dress and petticoats from stray embers and spills, and a leather tricorne shades her head. Meanwhile, her barbed-but-quick-witted tongue capably defends the rest of her.",
		store: "emily",
	};

	npcs["Bradshaw"] = {
		name: "One-Eye Bradshaw",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Booze Trader",
		longDesc: "A hirsute, barrel-chested man in sailors' slops with scars of battle on every extremity sits upon a stool before you. In a raucous voice, he hawks the bottled contents of several elicit crates. Contrary to his puzzling nickname, both of his eyes are fully intact. However, he has a wooden peg leg in place of one calf, a makeshift brace on the opposite knee, a hook instead of one hand, and two missing fingers on the other. He wears a Monmouth cap tilted low to one side, helping him cast a salty look at anyone who stares too long.",
		store: "bradshaw",
	};

	npcs["Amos"] = {
		name: "Amos Price",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Pet Merchant",
		longDesc: "This tall, thin man kindly tends to the menagerie of exotic animals among the cages and perches in his stall, all while keeping a weather eye on the lane's potential customers. He wears brightly colored clothes more common in the orient, and his flowing beard gives him a wild appearance of his own.",
	};

	// Long's Public House

	npcs["Barnaby Long"] = {
		name: "Barnaby Long",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Male,
		title: "NPC_NAME, Public House Owner",
		longDesc: "Paunchy but supremely good-natured, he moves around his house in stride, happily catering to the perpetual influx of guests. Here he mixes and serves a sought-after drink, there he turns the roast and stirs the kettle. While his home and attire may be humble, his obliging manners and cheerful bearing are fit for royalty.",
		store: "barnabyLong",
	};

	npcs["Meghan Long"] = {
		name: "Meghan Long",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Public House Matron",
		longDesc: "A statuesque woman with an endearing presence, she chops, cooks, cleans and entertains for the public house to its great success. Her homespun clothing needs no fancy additions to improve her simple but abundant charm - a few moments' study makes plain she is the beating heart of this venture.",
		store: "meghanLong",
	};

	// Levant
	npcs["Solenn"] = {
		name: "Solenn Leclère",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "Mme. NPC_NAME, House Madam",
		longDesc: "The madam of the house is a mature woman who looks far younger than her years, with china-white skin, olive black hair, piercing silvery-blue eyes and lips painted in deep garnet. Her gown is elegant and made from splendid materials, but unbustled and cut to reveal her body far beyond the toleration of polite society. Having laid eyes on her, it becomes strangely difficult to look away or think of other things. A slight smile from her suggests she is aware of having the effect.",
	};

	npcs["Isabella Ioveanu"] = {
		name: "Isabella Ioveanu",
		location: location,
		mood: 40,
		dailyMood: 0,
		lust: 40,
		dailyLust: 10,
		gender: Gender.Female,
		title: "NPC_NAME, Diviner",
		longDesc: "Though she seems much junior in age to her mistress, this young woman carries herself with equal poise. Beyond sharing a scandalous taste in dress, other similarities exist: the same midnight black hair, snowy pale complexion and identical, striking blue irises. Though her wide-set eyes and high cheekbones should place her heritage far more east in the continent than her madam's, there remains an uncanny resemblance between the two.",
		store: "isabella",
	};
}
