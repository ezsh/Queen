// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT

		// NECK SLOT
		"bimbo collar": { // +12
			name: "bimbo collar", shortDesc: "{COLOR} collar with the word 'BIMBO' in rhinestones",
			longDesc: "\
    If you're not trying to hide it, you might as well flaunt it. This pink leather collar is studded with \
    shiny rhinestones spelling out the word 'B-I-M-B-O' just incase anyone looking at you couldn't \
    figure it out on their own.\
    ",
			slot: ClothingSlot.Neck,
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.Bimbo, Fashion.Style.Bdsm],
			inMarket: false,
		},

		// NIPPLES SLOT

		// BRA SLOT
		"bimbo bra": { // +15
			name: "bimbo bra", shortDesc: "a {COLOR} push-up bra trimmed with lace and studded with rhinestones",
			longDesc: "This sexy bra is designed to push up and display your assets in the most provocative way possible. The lace and shiny rhinestones there are for maximum eye-catching appeal.",
			slot: ClothingSlot.Bra, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], activeEffect: ['PUSH_UP_BRA'], style: [Fashion.Style.Bimbo],
		},

		// CORSET SLOT
		"cupless corset": { // +9
			name: "cupless corset", shortDesc: "a lacey {COLOR} cupless corset",
			longDesc: "This highly impractical garment doesn't cinch your waist at all and leaves your breasts totally exposed. It's obviously meant just for show.",
			slot: ClothingSlot.Corset, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"],
			style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer, Fashion.Style.SluttyLady, Fashion.Style.HighClassWhore],
		},

		// PANTY SLOT
		"bimbo thong": { // 15
			name: "bimbo thong", shortDesc: "a {COLOR} thong with 'I @@.state-girlinness;&#9825;@@ Cum! written on the crotch' ",
			longDesc: "This tiny thong is practically a g-string. The advertisement printed on it leaves no doubt to any onlookers as to what you're all about.",
			slot: ClothingSlot.Panty, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.Bimbo],
		},

		// STOCKINGS SLOT

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"sexy micro dress": { // +40
			name: "sexy micro dress", shortDesc: "a figure hugging {COLOR} micro dress",
			longDesc: "This dress is made out of some exotic material that hugs every curve of your body. It's so humiliatingly short that your genitals are half open for everyone and the top has several strategic holes to present multiple views of your chest.",
			slot: ClothingSlot.Dress,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING", "KINKY_CLOTHING"], activeEffect: ["SUCCUBUS_ALLURE"], style: [Fashion.Style.Bimbo],
		},

		"bimbo cocktail dress": { // +40
			name: "bimbo cocktail dress", shortDesc: "extremely revealing {COLOR} cocktail dress",
			longDesc: "\
    This tiny dress consists of a strapless top with a plunging neckline so low it threatens to show off \
    your nipples to anyone passing by. The bottom is made out of a stiff crinkly material that sticks \
    out the sides and is so short that it will show off your panties if you do anything but stand ramrod straight.\
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.Bimbo],
			inMarket: false,
		},

		// COSTUME SLOT
		"bimbo cheerleader costume": { // +40
			name: "bimbo cheerleader costume", shortDesc: "bright {COLOR} cheerleader outfit",
			longDesc: "\
    Legends tell that in ages long ago great wars were fought with the assistance of beautiful women \
    known as 'cheerleaders'. No one is quite sure what that means, and all recent attempts to resurrect the \
    practice have met with limited success. Despite that, many men are still attracted to the ceremonial costume \
    which consists in this case of a tight midrift halter top emblazoned with the words 'BJ U' on it and a tiny \
    pleated pink skirt.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// SHOES SLOT
		"tall heels": { // +10
			name: "tall heels", shortDesc: "very tall {COLOR} high heels",
			longDesc: "\
    These tall high heels make your arse stick out and your hips wiggle when you walk.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer, Fashion.Style.HighClassWhore],
			inMarket: true,
		},

		"platform heels": { // +15
			name: "platform heels", shortDesc: "very tall {COLOR} platform high heels",
			longDesc: "\
    The thick platform on these heels gives you several inches of height and forces you walk with \
    a slight mincing or prancing motion. The effect is most noticeable in how attention is drawn to your arse \
    and how it wiggles and shakes as you walk. \
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer, Fashion.Style.HighClassWhore],
			inMarket: true,
		},

		"bimbo heels": { // +20
			name: "bimbo heels", shortDesc: "{COLOR} rhinestone encrusted platform high heels",
			longDesc: "\
    The thick platform on these heels gives you several inches of height and forces you walk with \
    a slight mincing or prancing motion. The effect is most noticeable in how attention is drawn to your arse \
    and how it wiggles and shakes as you walk. The shiny rhinestones encrusted around the front and back \
    of the heel make it just that much more 'extra'.\
    ",
			slot: ClothingSlot.Shoes,
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// BUTT SLOT
		"small plug": { // +9
			name: "small plug", shortDesc: "a small anal plug with a heart-shaped {COLOR} faux gem",
			longDesc: "This tiny little plug is designed be worn in your ass. The fake gem is there for decoration because everyone is going to be looking at it…",
			slot: ClothingSlot.Butt, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING"], style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer, Fashion.Style.HighClassWhore],
		},

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
