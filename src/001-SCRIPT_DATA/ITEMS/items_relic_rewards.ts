
namespace App.Data {
	const relicEnhancerLongDesc = "This fortifying draught has had the remnants of ancient magic imbued in it. Who knows what it does?";
	const relicEnhancerConsumeMessage = "You down the tart liquid, your lips pursing at the sour and acrid taste. A warm sensation settles in your stomach";
	Object.append(App.Data.drugs, {
		// Energy, Face, Hair, Lips
		"relic enhancer a": {
			name: "relic enhancer a",
			shortDesc: "Relic Enhancer Potion (A)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: ["ENERGY_RARE", "FACE_XP_COMMON", "HAIR_XP_COMMON", "LIPS_XP_COMMON", "TOXICITY_COMMON"],
		},

		// Bust, Femininity, Perversion, Seduction
		"relic enhancer b": {
			name: "relic enhancer b",
			shortDesc: "Relic Enhancer Potion (B)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: [
				"BUST_XP_UNCOMMON", "FEMININITY_XP_RARE", "PERVERSION_XP_RARE",
				"SEDUCTION_XP_UNCOMMON", "TOXICITY_COMMON",
			],
		},

		// Ass, Hips, Perversion, Seduction
		"relic enhancer c": {
			name: "relic enhancer c",
			shortDesc: "Relic Enhancer Potion (C)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: [
				"ASS_XP_UNCOMMON", "HIPS_XP_UNCOMMON", "PERVERSION_XP_RARE",
				"SEDUCTION_XP_UNCOMMON", "TOXICITY_COMMON",
			],
		},

		// Fitness, Waist, Dancing
		"relic enhancer d": {
			name: "relic enhancer d",
			shortDesc: "Relic Enhancer Potion (D)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: ["FITNESS_XP_RARE", "WAIST_XP_UNCOMMON", "DANCING_XP_UNCOMMON", "TOXICITY_COMMON"],
		},

		// Swashbuckling, Navigating, Sailing
		"relic enhancer e": {
			name: "relic enhancer e",
			shortDesc: "Relic Enhancer Potion (E)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: ["SWASHBUCKLING_XP_UNCOMMON", "SAILING_XP_UNCOMMON", "NAVIGATING_XP_UNCOMMON", "TOXICITY_UNCOMMON"],
		},

		// Seduction, Singing, Dancing, Styling
		"relic enhancer f": {
			name: "relic enhancer f",
			shortDesc: "Relic Enhancer Potion (F)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: [
				"SEDUCTION_XP_UNCOMMON", "SINGING_XP_UNCOMMON", "DANCING_XP_UNCOMMON",
				"STYLING_XP_UNCOMMON", "TOXICITY_UNCOMMON",
			],
		},

		// Cleaning, Cooking, Serving
		"relic enhancer g": {
			name: "relic enhancer g",
			shortDesc: "Relic Enhancer Potion (G)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: [
				"CLEANING_XP_UNCOMMON", "COOKING_XP_UNCOMMON", "SERVING_XP_UNCOMMON",
				"TOXICITY_UNCOMMON",
			],
		},

		// Penis, Balls, Perversion
		"relic enhancer h": {
			name: "relic enhancer h",
			shortDesc: "Relic Enhancer Potion (H)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: ["PENIS_XP_RARE", "BALLS_XP_RARE", "PERVERSION_XP_LEGENDARY", "TOXICITY_RARE"],
		},

		// Fitness, Waist Up
		"relic enhancer i": {
			name: "relic enhancer i",
			shortDesc: "Relic Enhancer Potion (I)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: ["FITNESS_XP_LEGENDARY", "WAIST_REVERT_RARE", "TOXICITY_RARE"],
		},

		// TitFucking, AssFucking, BlowJobs, HandJobs
		"relic enhancer j": {
			name: "relic enhancer j",
			shortDesc: "Relic Enhancer Potion (J)",
			longDesc: relicEnhancerLongDesc,
			message: relicEnhancerConsumeMessage,
			type: ItemTypeConsumable.Potion,
			inMarket: false,
			charges: 1,
			effects: [
				"HANDJOBS_XP_RARE", "TITFUCKING_XP_RARE", "BLOWJOBS_XP_RARE",
				"ASSFUCKING_XP_RARE", "TOXICITY_UNCOMMON",
			],
		},
	});
}
