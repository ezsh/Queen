namespace App.Data {
	stores.didi = {
		name: "Glutezon Village Trader",
		open: [0, 1, 2],
		restock: 7,
		maxRares: 4,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, tag: "butter gourd", qty: 5, max: 5, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apRose", qty: 1, max: 2, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apGlutezon1", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apPlasterglutezon", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, tag: "apOrchid", qty: 1, max: 1, price: 1.0, mood: 0, lock: 0},
		],
	};
}
