namespace App.Data {
	stores.yvonneStore = {
		name: "Madame Yvonne's Salon", open: [0, 1, 2], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "basic makeup"},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "hair products"},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "expensive makeup"},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "hair accessories"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "red hair dye"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "black hair dye"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "brown hair dye"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "blond hair dye"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 5, max: 5, price: 1.0, mood: 0, lock: 0, tag: "face cream"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 5, max: 5, price: 1.0, mood: 0, lock: 0, tag: "hair tonic"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 5, max: 5, price: 1.0, mood: 0, lock: 0, tag: "lip balm"},
		],
	};
}
