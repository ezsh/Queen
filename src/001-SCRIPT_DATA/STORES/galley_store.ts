namespace App.Data {
	stores.galley = {
		name: "Salty Mermaid Galley",
		open: [0, 1, 2, 3],
		restock: 7,
		maxRares: 2,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "slave gruel"}, // special food, -will
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 8, max: 8, price: 1.0, mood: 0, lock: 0, tag: "grog"}, // alcohol
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 20, lock: 0, tag: "mystery stew"}, // normal food
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 40, lock: 0, tag: "starfruit"}, // Energy
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 65, lock: 0, tag: "pink peach"}, // purge
			// RARE TF FOOD
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 60, lock: 0, tag: "milkdew melon"}, // boobs
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 70, lock: 0, tag: "mighty banana"}, // dick
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "honey mead"}, // lips
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 3, max: 3, price: 1.0, mood: 80, lock: 0, tag: "meat and beans"}, // normal food
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "butter gourd"}, // ass
			// RARE ALCOHOL
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "ol musky"}, // seduction xp
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "expensive wine"}, // perv, fem, -will
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 1, max: 1, price: 1.0, mood: 80, lock: 0, tag: "rum"}, // willpower
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 80, lock: 0, tag: "smugglers ale"},
		],
	};
}
