namespace App.Data {
	stores.bella = {
		name: "Bella's Boudoir",
		open: [0, 1, 2, 3],
		restock: 7,
		maxRares: 4,
		inventory: [
			// Common items - Cosmetics
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, tag: "basic makeup", qty: 10, max: 10, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, tag: "expensive makeup", qty: 10, max: 10, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, tag: "hair products", qty: 10, max: 10, price: 1.0, mood: 0, lock: 0},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, tag: "hair accessories", qty: 10, max: 10, price: 1.0, mood: 0, lock: 0},
			// Rare Items - Pirate Slut
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "pirate queen regalia", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "pirate stripper costume", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "skull piercings", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "pirate ankle boots", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Bimbo
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "bimbo cheerleader costume", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "bimbo cocktail dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "bimbo collar", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "bimbo heels", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Sissy Lolita
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sissy maid outfit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sweet cherry dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sweet cherry socks", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sweet cherry shoes", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Gothic Lolita
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "gothic maid outfit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "dark dreams dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "dark dreams bonnet", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "dark dreams shoes", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// BDSM
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "rubber nurse outfit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "bondage dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "gimp mask", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "spiked boots", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Daddy's Girl
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "naughty schoolgirl outfit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "babydoll dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "pink cock ribbon", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "low heel maryjanes", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Naughty Nun
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sexy nun habit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "naughty nun dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sexy nun cowl", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sexy nun boots", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// High Class Whore
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sexy librarian outfit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "slutty strumpet dress", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "whore belt", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "whore sandals", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Slutty Lady
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sexy showgirl outfit", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "luxurious black evening gown", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "luxurious butt plug", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "luxurious black high heels", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			// Pet Girl
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "sexy kitten costume", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "fuzzy black bikini", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "fuzzy anal tail", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, tag: "fuzzy paws", qty: 1, max: 1, price: 1.0, mood: 80, lock: 0},
		],
	};
}
