namespace App.Data {
	Object.append(effectLibrary, {
		NEREID_PHILTRE: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, 5);
				p.adjustCoreStat(CoreStat.Femininity, 5);
				p.adjustCoreStat(CoreStat.Toxicity, 50);
				p.adjustCoreStatXP(CoreStat.Femininity, 100);
				p.adjustCoreStatXP(CoreStat.Willpower, 200);
				p.adjustCoreStatXP(CoreStat.Hormones, 300);
				p.adjustBodyXP(BodyPart.Face, 500);
				p.adjustBodyXP(BodyPart.Hair, 500);
				p.adjustBodyXP(BodyPart.Lips, 500);
				p.adjustBodyXP(BodyPart.Waist, -500);
				p.adjustSkillXP(Skills.Charisma.Dancing, 500);
				p.adjustSkillXP(Skills.Charisma.Singing, 500);
				p.adjustSkillXP(Skills.Charisma.Seduction, 500);
			},
			value: 2000,
			knowledge: [
				"Female Hormones++++", "Femininity Up+++", "Femininity XP+++", "Toxicity Up---",
				"Face Prettier++++", "Hair Growth++++", "Lips Fuller++++", "WillPower Up+++",
				"Waist Narrower++++", "Dancing XP++++", "Singing XP++++", "Seduction XP++++",
			],
		},

		SUCCUBUS_PHILTRE: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, 5);
				p.adjustCoreStat(CoreStat.Femininity, 5);
				p.adjustCoreStat(CoreStat.Toxicity, 50);
				p.adjustCoreStatXP(CoreStat.Femininity, 300);
				p.adjustCoreStatXP(CoreStat.Willpower, 200);
				p.adjustCoreStatXP(CoreStat.Hormones, 300);
				p.adjustCoreStatXP(CoreStat.Perversion, 500);
				p.adjustBodyXP(BodyPart.Bust, 500);
				p.adjustBodyXP(BodyProperty.BustFirmness, 500, 80);
				p.adjustBodyXP(BodyPart.Ass, 500);
				p.adjustBodyXP(BodyPart.Hips, 500);
				p.adjustBodyXP(BodyPart.Waist, -500);
				p.adjustSkillXP(Skills.Charisma.Seduction, 500);
			},
			value: 2000,
			knowledge: [
				"Female Hormones++++", "Femininity Up+++", "Femininity XP+++", "Toxicity Up---",
				"Bust Growth++++", "Bust Firmness++++", "Ass Growth++++", "Hips Wider++++", "WillPower Up+++",
				"Waist Narrower++++", "Perversion XP++++", "Seduction XP++++",
			],
		},

		FEMALE_MANDRAKE: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, 10);
				p.adjustCoreStat(CoreStat.Femininity, 15);
				p.adjustCoreStat(CoreStat.Toxicity, 80);
				p.adjustCoreStatXP(CoreStat.Femininity, 200);
				p.adjustCoreStatXP(CoreStat.Willpower, 400);
				p.adjustCoreStatXP(CoreStat.Hormones, 300);
				p.adjustCoreStatXP(CoreStat.Perversion, 300);
				p.adjustBodyXP(BodyPart.Lips, 300);
				p.adjustBodyXP(BodyPart.Bust, 300);
				p.adjustBodyXP(BodyProperty.BustFirmness, 300, 50);
				p.adjustBodyXP(BodyPart.Ass, 300);
				p.adjustBodyXP(BodyPart.Hips, 300);
				p.adjustBodyXP(BodyPart.Waist, -300);
				p.adjustSkillXP(Skills.Charisma.Seduction, 500);
			},
			value: 2000,
			knowledge: [
				"Female Hormones++++", "Femininity Up+++", "Femininity XP+++", "Toxicity Up----",
				"Bust Growth+++", "Bust Firmness+++", "Ass Growth+++", "Hips Wider+++", "WillPower Up+++",
				"Waist Narrower+++", "Lips Fuller+++", "Perversion XP+++", "Seduction XP++++",
			],
		},

		MALE_MANDRAKE: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, -10);
				p.adjustCoreStat(CoreStat.Femininity, -15);
				p.adjustCoreStat(CoreStat.Toxicity, 80);
				p.adjustCoreStat(CoreStat.Willpower, 5);
				p.adjustCoreStatXP(CoreStat.Femininity, -200);
				p.adjustCoreStatXP(CoreStat.Willpower, 400);
				p.adjustCoreStatXP(CoreStat.Hormones, -300);
				p.adjustCoreStatXP(CoreStat.Perversion, 500);
				p.adjustBodyXP(BodyPart.Lips, -300);
				p.adjustBodyXP(BodyPart.Bust, -300);
				p.adjustBodyXP(BodyPart.Penis, 500);
				p.adjustBodyXP(BodyPart.Balls, 500);
				p.adjustBodyXP(BodyPart.Ass, -300);
				p.adjustBodyXP(BodyPart.Hips, -300);
				p.adjustBodyXP(BodyPart.Waist, 300);
				p.adjustSkillXP(Skills.Charisma.Seduction, -500);
			},
			value: 2000,
			knowledge: [
				"Female Hormones----", "Femininity Down+++", "Femininity XP---", "Toxicity Up----",
				"Bust Growth---", "Ass Growth---", "Hips Wider---", "WillPower Up++++", "Balls XP++++",
				"Penis XP++++", "Waist Narrower---", "Lips Fuller---", "Perversion XP+++", "Seduction XP----",
			],
		},

		SIREN_ELIXIR: {
			fun: (p) => {
				p.adjustCoreStatXP(CoreStat.Willpower, 200);
				p.adjustCoreStatXP(CoreStat.Hormones, 200);
				p.adjustCoreStatXP(CoreStat.Perversion, -200);
				p.adjustBodyXP(BodyPart.Face, 100);
				p.adjustBodyXP(BodyPart.Bust, 100);
				p.adjustBodyXP(BodyProperty.BustFirmness, 100, 70);
				p.adjustBodyXP(BodyPart.Hips, 100);
				p.adjustBodyXP(BodyPart.Ass, 100);
				p.adjustBodyXP(BodyPart.Waist, -100);
				p.adjustBodyXP(BodyPart.Hair, 100);
				p.adjustCoreStat(CoreStat.Toxicity, 50);
			},
			value: 500,
			knowledge: [
				"Female Hormones+++", "Face Prettier++",
				"Bust Growth++", "Bust Firmness++", "Ass Growth++", "Hips Wider++", "WillPower Up+++",
				"Waist Narrower++", "Perversion Down++++", "Toxicity Up---",
			],
		},

		ELECTUARY_MARS: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, -200);
				p.adjustBody(BodyPart.Face, -1000);
				p.adjustBody(BodyPart.Bust, -15);
				p.adjustBody(BodyPart.Hips, -20);
				p.adjustBody(BodyPart.Ass, -16);
				p.adjustBody(BodyPart.Waist, 18);
				p.adjustBody(BodyPart.Lips, -20);
				p.adjustBody(BodyProperty.Height, +20);
				p.adjustBody(BodyPart.Penis, 100);
				p.adjustBody(BodyPart.Balls, 100);
			},
			value: 6000,
			knowledge: [
				"Male Hormones++++", "Face Harder++++", "Bust Shrink----", "Hips Narrower----", "Ass Shrink----",
				"Waist Wider----", "Lips Thinner----", "Height Up++", "Penis Grow++++", "Balls Grow++++",
			],
		},
		/** POSSET VENUS */
		POSSET_VENUS: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, 200);
				p.adjustBody(BodyPart.Face, 1000);
				p.adjustBody(BodyPart.Bust, 15);
				p.adjustBody(BodyProperty.BustFirmness, 15);
				p.adjustBody(BodyPart.Hips, 20);
				p.adjustBody(BodyPart.Ass, 16);
				p.adjustBody(BodyPart.Waist, -18);
				p.adjustBody(BodyPart.Hair, 40);
				p.adjustBody(BodyPart.Lips, 20);
				p.adjustBody(BodyProperty.Height, -20);
				p.adjustBody(BodyPart.Penis, -100);
				p.adjustBody(BodyPart.Balls, -100);
			},
			value: 6000,
			knowledge: [
				"Female Hormones++++", "Face Prettier++++", "Bust Growth++++", "Bust Firmness++++", "Hips Wider++++", "Ass Growth++++",
				"Waist Narrower++++", "Hair Growth++++", "Lips Fuller++++", "Height Down--", "Penis Shrink----",
				"Balls Shrink----",
			],
		},
		/** BALSAM PULCHRITUDE */
		BALSAM_PULCHRITUDE: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, 50);
				p.adjustBody(BodyPart.Face, 20);
				p.adjustBody(BodyPart.Hair, 40);
				p.adjustBody(BodyPart.Lips, 20);
			},
			value: 2000,
			knowledge: ["Face Prettier++++", "Lips Fuller++++", "Hair Growth++++", "Female Hormones++"],
		},
		/** OIL ENDOWMENT */
		OIL_ENDOWMENT: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Hormones, -200);
				p.adjustBody(BodyPart.Penis, 40);
				p.adjustBody(BodyPart.Balls, 40);
			},
			value: 2000,
			knowledge: ["Penis Grow++++", "Balls Grow++++", "Male Hormones++++"],
		},
		/** RESOLUTE_DROUGHT */
		RESOLUTE_DROUGHT: {
			fun: (p) => {
				p.adjustCoreStat(CoreStat.Willpower, 10);
				p.adjustCoreStat(CoreStat.Energy, 100);
				p.adjustCoreStat(CoreStat.Health, 100);
				p.adjustCoreStat(CoreStat.Toxicity, -100);
			},
			value: 2000,
			knowledge: ["WillPower Up++++", "Energy Up++++", "Health Up++++", "Toxicity Down----"],
		},
	});

	Object.append(App.Data.effectLibraryEquipment.wear, {
		MAGIC_COCK_RING: {
			fun: (p) => {
				p.setXP(Stat.Body, BodyPart.Penis, 0);
				p.setXP(Stat.Body, BodyPart.Balls, 0);
			},
			value: 0,
			knowledge: ["Genital Protection++++"],
		},

		VOODOO_ANAL_PLUG: {
			fun: (p) => {
				p.adjustBodyXP(BodyPart.Ass, 50, 70);
				p.adjustBodyXP(BodyPart.Hips, 50, 70);
				p.adjustCoreStatXP(CoreStat.Perversion, 50, 70);
				p.adjustSkillXP(Skills.Sexual.AssFucking, 50, 70);
			},
			value: 0,
			knowledge: ["Ass Growth++++", "Hips Growth++++", "Kinky+++", "Gape Training+++"],
		},
		FUTA_TRANSFORM: {
			fun: (p) => {
				p.adjustCoreStatXP(CoreStat.Futa, 50);
			},
			value: 50,
			knowledge: ["Futanari Identity+"],
		},
	});

	Object.append(App.Data.effectLibraryEquipment.active, {
		VOODOO_ANAL_PLUG2: {
			fun: (s) => {return (s === "assFucking") ? 20 : 0;},
			value: 0,
			knowledge: ["Butt Slut++++"],
		},
	});

	Object.append(App.Data.effectLibrary, {
		// DAD Face modification effects
		EYES_BIGGER: {
			fun: (p) => {
				const c = Math.clamp((p.faceData.basedim.eyeSize ?? 0) + 2, 0, 40);
				p.faceData.basedim.eyeSize = c;
			},
			value: 400,
			knowledge: ["Eyes Bigger++"],
		},

		EYES_SMALLER: {
			fun: (p) => {
				const c = Math.clamp((p.faceData.basedim.eyeSize ?? 0) - 2, 0, 40);
				p.faceData.basedim.eyeSize = c;
			},
			value: 400,
			knowledge: ["Eyes Smaller++"],
		},

		BROW_THICKER: {
			fun: (p) => {
				const c = Math.clamp((p.faceData.Mods.browThickness ?? 0) + 1, -10, 10);
				p.faceData.Mods.browThickness = c;
			},
			value: 200,
			knowledge: ["Eyebrows Thicker++"],
		},

		BROW_THINNER: {
			fun: (p) => {
				const c = Math.clamp((p.faceData.Mods.browThickness ?? 0) - 1, -10, 10);
				p.faceData.Mods.browThickness = c;
			},
			value: 250,
			knowledge: ["Eyebrows Thinner++"],
		},
	});
}
