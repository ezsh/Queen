declare namespace App.Data {
	export namespace Whoring {
		type SexActStr = "hand" | "ass" | "bj" | "tits";
		type SexualBodyPart = BodyPart.Ass | BodyPart.Bust | BodyPart.Face | BodyPart.Lips | BodyPart.Penis;
		export type BonusSource = Fashion.StyleId | SexualBodyPart;
		export type BonusCategory = CoreStat.Perversion | CoreStat.Femininity | "beauty";

		interface Bonus {
			body?: Partial<Record<SexualBodyPart, number>>,
			style?: Partial<Record<Fashion.StyleId, number>>,
			stat: Partial<Record<BonusCategory, number>>
		}

		export interface WhoringLocationDesc {
			/** Shows in the UI as location. */
			desc: string;
			/** Sex act weighted table. */
			wants: Partial<Record<SexActStr, number>>;
			/** Minimum payout rank. */
			minPay: number;
			/** Maximum payout rank. */
			maxPay: number;
			/** Bonus payout, weighted table */
			bonus: Bonus;
			names: string[];
			title: string | null;
			rares: string[];
			npcTag: string;
			marquee?: string;
			/**
			 * @default [DayPhase.Morning, DayPhase.Afternoon, DayPhase.Evening, DayPhase.Night]
			 */
			phases?: DayPhase[];
			available?: Conditions.Expression;
		}
	}
}
