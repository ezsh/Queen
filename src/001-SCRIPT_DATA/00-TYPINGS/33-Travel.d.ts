declare namespace App.Data {
	export namespace Travel {
		type BaseChoice = Choices.Choice<Entity.Player>;
		interface PassageDestination extends BaseChoice{
			destination: Choices.DynamicValue<string, [Entity.Player]> | null;
		}

		interface SceneDestination extends RequiredPick<BaseChoice, 'caption'>{
			scene: string;
		}

		type DestinationChoice = PassageDestination | SceneDestination;

		export type Destinations = NonEmptyArray<string | DestinationChoice>;
	}
}
