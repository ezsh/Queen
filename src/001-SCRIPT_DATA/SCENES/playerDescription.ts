namespace App.Data {
	Object.append(scenes, {
		playerDescription: {
			actors: {},
			entry: "general",
			fragments: {
				general: {
					type: "text",
					text: "<p>@{pc} are @{pc.body.height} and @{pc.core.fitness}</p>",
					next: 'head',
				},
				head: {
					type: "text",
					text: "<p>@{pc|(have)} a @{pc.body.face.desc} @{pc.body.hair.desc} @{pc.body.eyes.desc} @{pc|(have)} @{pc.body.lips.desc}</p>",
					next: "chest",
				},
				chest: {
					type: "text",
					text: "@{pc.body.bust.desc}",
					next: "bustFirmness",
				},
				bustFirmness: {
					enabled: ctx => ctx.pc.bodyStats.bust > 7,
					type: "text",
					text: "@{pc.body.bustFirmness.desc}",
					next: ctx => ctx.pc.bodyStats.lactation > 0 ? "lactation" : "ass",
				},
				lactation: {
					type: "text",
					text: "@{pc.body.lactation.desc}",
					next: "ass",
				},
				ass: {
					type: "text",
					text: "<p>@{pc.body.ass.desc|.body.hips.desc|.body.waist.desc} and @{pc.look.figure.desc}.</p>",
					next: "crotch",
				},
				crotch: {
					type: "text",
					text: "<p>@{pc|(have)} @{pc.body.penis.desc} and @{pc.body.balls.desc} between @{pc.pr.his} legs.",
					next: ctx => ctx.pc.coreStats.futa > 0 ? "futa" : "look",
				},
				futa: {
					type: "text",
					text: "@{pc.core.futa.desc}",
					next: "look",
				},
				look: {
					type: "text",
					text: "</p><p>@{pc|(consider)|.pr.his} natural beauty to be @{pc.look.beauty.desc}.<br/> The fetish appeal of @{pc.pr.his} body is @{pc.look.fetish.desc}.</p>",
				},
			},
		},
	});
}
