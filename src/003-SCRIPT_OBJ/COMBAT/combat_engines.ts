namespace App.Combat.Engines {
	export interface StyleEngine  {
		attackTarget(target: Combatant, command: string): boolean;
		checkCommand(command: Data.Combat.Move): boolean;
		defend(): void;
		recover(): void;
		doAI(target: Combatant): void;
		moves: Record<string, Data.Combat.Move>;
		combatClass: Style;
		lastMove: string | null;
	}

	export function createEngine(style: Style, owner: Combatant,
		myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback
	): StyleEngine {
		switch (style) {
			case Style.AssFu: return new Assfu(owner, myStatusCB, theirStatusCB, chatLogCB);
			case Style.BoobJitsu: return new Boobjitsu(owner, myStatusCB, theirStatusCB, chatLogCB);
			case Style.Boobpire: return new Boobpire(owner, myStatusCB, theirStatusCB, chatLogCB);
			case Style.Kraken: return new Kraken(owner, myStatusCB, theirStatusCB, chatLogCB);
			case Style.Siren: return new Siren(owner, myStatusCB, theirStatusCB, chatLogCB);
			case Style.Swashbuckling: return new Swashbuckling(owner, myStatusCB, theirStatusCB, chatLogCB);
			case Style.Unarmed: return new Unarmed(owner, myStatusCB, theirStatusCB, chatLogCB);
		}
	}

	type MoveOrMiss<TMove extends string> = TMove | 'Miss';

	// Generic engine with default behavior
	abstract class Generic<Moves extends string> implements StyleEngine {
		#combatClass: Style;
		#owner: Combatant;
		#myStatusCB: MyStatusCallback;
		#theirStatusCB: TheirStatusCallback;
		#chatLogCB: ChatLogCallback;
		#attackHistory: MoveOrMiss<Moves>[] = []; // TODO refactor the class to generic and take this as a parameter?

		protected constructor(combatClass: Style, owner: Combatant,
			myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback
		) {
			this.#combatClass = combatClass;
			this.#owner = owner; // The object that owns this engine.
			this.#myStatusCB = myStatusCB; // Callback for updating a html component
			this.#theirStatusCB = theirStatusCB; // Callback for updating a html component
			this.#chatLogCB = chatLogCB; // Callback for updating an html component
		}

		get combatClass(): Style {return this.#combatClass;}

		get owner(): Combatant {return this.#owner}

		get moves(): Record<Moves, Data.Combat.Move> {
			return Data.Combat.moves[this.combatClass]!.moves as Record<Moves, Data.Combat.Move>;
		}

		get lastMove(): MoveOrMiss<Moves> | null {
			return this.#attackHistory.length > 0 ? this.#attackHistory[this.#attackHistory.length - 1] : null;
		}

		/**
		 * Attack the enemy
		 */
		attackTarget(target: Combatant, command: Moves): boolean {
			const roll = this.calculateHit(target);

			const mv = this.move(command);
			this.consumeResources(mv);
			// Try to hit target
			if (roll > 0) {
				this.owner.recoverCombo(this.generateCombo(target, command, roll));
				this.#attackHistory.push(command);
				this.applyEffects(target, command, roll);
				this.doDamage(target, command, roll);
				return true;
			} else {
				this.#attackHistory.push("Miss");
				const message = this.getMissMessage(mv.miss);
				this.printMessage(message, target);
				return false;
			}
		}

		recover(): void {
			// use energy for stamina
			this.owner.useEnergy(1);
			this.owner.recoverStamina(100);
			this.owner.addWeaponDelay(10);
			this.printMessage(this.owner.selectMessage(recoverMessage), this.owner);
		}

		defend(): void {
			this.owner.recoverStamina(10); // Regain some stamina
			this.owner.addEffect(Effect.Guarded, 2);
			this.owner.addWeaponDelay(10);
			this.printMessage(this.owner.selectMessage(defendMessage), this.owner);
		}

		abstract doAI(target: Combatant): void;

		consumeResources(command: Data.Combat.Move): void {
			this.owner.useStamina(command.stamina);
			this.owner.useCombo(command.combo);
			this.owner.addWeaponDelay(command.speed);
		}

		checkCommand(command: Data.Combat.Move): boolean {
			return command.stamina <= this.owner.stamina
				&& command.combo <= this.owner.combo
				&& command.unlock(this.owner);
		}

		/**
		 * Calculate if an attack hits.
		 * @param target  Entity that you are attacking
		 */
		calculateHit(target: Combatant): number {
			const myRoll = this.owner.attackRoll(); // Includes getting attack buffs
			const theirRoll = target.defenseRoll(); // Includes getting defense buffs
			return (myRoll - theirRoll);
		}

		/**
		* @param message Miss array from attack definition.
		* @param target  Object we are attacking.
		*/
		printMessage(message: string, target: Combatant): void {
			if (typeof this.#chatLogCB === 'function') {
				if (this.owner.isNPC) {
					this.#chatLogCB(message, this.owner);
				} else {
					this.#chatLogCB(message, target);
				}
			}
		}

		doDamage(target: Combatant, command: Moves, roll: number): void {
			let dmg = this.calculateDamage(target, command, roll);
			// Apply effect bonuses
			if (this.owner.hasEffect(Effect.Bloodthirst)) dmg = Math.ceil(dmg * 1.5);
			if (target.hasEffect(Effect.Guarded)) dmg = Math.floor(dmg * 0.7);

			if (target.hasEffect(Effect.Parry)) {
				dmg = 0; // block all damage.
				target.reduceEffect(Effect.Parry, 1); // Reduce parry counter.
				this.#attackHistory.push("Miss"); // We missed. Sadface.
				this.printMessage(this.owner.selectMessage(parryMessage), target);
			} else {
				const mv = this.move(command);
				if (target instanceof Combat.Player) {
					const dmgMod = Math.min(target.player.getWornSkillBonus("damageResistance"), 90) / 100; // capped
					dmg = (dmg - Math.ceil(dmg * dmgMod));
				}
				this.printHit(mv.hit, target, roll, dmg);
				target.takeDamage(dmg);
			}

			if (this.owner.hasEffect(Effect.LifeLeech)) {
				const heal = Math.ceil(dmg * 0.5);
				this.owner.recoverHealth(heal);
				this.printMessage(String.format(this.owner.selectMessage(healMessage), heal), target);
			}
		}

		calculateDamage(_target: Combatant, _command: Moves, _roll: number): number {
			return 1;
		}

		applyEffects(_target: Combatant, _command: Moves, _roll: number): void {
			// no-op
		}

		generateCombo(_target: Combatant, _command: Moves, _roll: number): number {
			return 0;
		}

		printHit(attacks: [string, string][], target: Combatant, roll: number, damage: number): void {
			const len = Math.floor(Math.max(0, Math.min((attacks.length * roll), (attacks.length - 1))));
			let msg = this.owner.selectMessage(attacks[len]);
			msg += ` <span style='color:red'>(${damage})</span>`;
			this.printMessage(msg, target);
		}

		/**
		 *
		 * @param arr Message to show to chat log for misses.
		 */
		getMissMessage(arr: [string, string][]): string {
			return this.owner.selectMessage(arr.randomElement());
		}

		protected stun(target: Combatant, duration: number) {
			target.addEffect(Effect.Stunned, duration);
			this.printMessage(this.owner.selectMessage(stunMessage), target);
		}

		protected move(move: Moves): Data.Combat.Move {
			return this.moves[move];
		}
	}

	/**
	 * Unarmed combat class
	 */
	export class Unarmed extends Generic<Moves.Unarmed> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.Unarmed, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		/**
		 * Calculate the damage of an unarmed attack
		 * @param target
		 * @param command
		 * @param roll
		 * @returns Damage
		 */
		override calculateDamage(target: Combatant, command: Moves.Unarmed, _roll: number): number {
			let base = 1;

			const owner = this.owner;
			if (owner instanceof Combat.Player) {
				base = 1 + (State.random() * 3) + Math.max(1, Math.min((owner.player.stat(Stat.Core, CoreStat.Fitness) / 25), 4));
				base = Math.floor(base);
			} else {
				base = base + Math.floor(owner.attack / 20);
			}

			if (command !== Moves.Unarmed.Knee) {
				base = Math.floor(base * this.move(command).damage); // Add damage mod
			} else {
				const mod = target.gender === Gender.Male ? 4.0 : 2.0; // Knee attack does more damage on male enemies
				base = Math.floor(base * mod);
			}

			return base;
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		override generateCombo(_target: Combatant, command: Moves.Unarmed, _roll: number): number {
			if ((command === Moves.Unarmed.Punch && this.lastMove === Moves.Unarmed.Kick) ||
				(command === Moves.Unarmed.Kick && this.lastMove === Moves.Unarmed.Punch)) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 * @param target
		 * @param command
		 * @param Roll
		 */
		override applyEffects(target: Combatant, command: Moves.Unarmed, roll: number): void {
			if (command === Moves.Unarmed.Haymaker) {
				const chance = Math.max(10, Math.min((100 * roll), 100));
				if (chance >= Math.floor(State.random() * 100)) {
					this.stun(target, 2);
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.moves.Knee.combo) {
				this.attackTarget(target, Moves.Unarmed.Knee);
			} else if (this.owner.combo >= this.moves.Haymaker.combo && Math.floor(State.random() * 100) >= 60) {
				this.attackTarget(target, Moves.Unarmed.Haymaker);
			} else if (this.lastMove === Moves.Unarmed.Kick) {
				this.attackTarget(target, Moves.Unarmed.Punch);
			} else {
				this.attackTarget(target, Moves.Unarmed.Kick);
			}
		}
	}

	/**
	 * Swashbuckling Class
	 */
	export class Swashbuckling extends Generic<Moves.Swashbuckling> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.Swashbuckling, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		/**
		 * Calculate the damage of an swashbuckling attack
		 * @returns Damage
		 */
		override calculateDamage(target: Combatant, command: Moves.Swashbuckling, _roll: number): number {
			let base = 1;
			const owner = this.owner;
			if (owner instanceof Combat.Player) { // TODO replace with visitors?
				const weaponQuality = owner.getWeaponQuality();
				const bonus = owner.getWeaponBonus();
				const skill = owner.attack;
				const fitness = owner.player.stat(Stat.Core, CoreStat.Fitness);
				const mod = 1 + (skill / 100) + (fitness / 100);
				base = Math.ceil(weaponQuality * mod) + bonus;
			} else {
				base += Math.floor(owner.attack / 10);
			}

			if (command === Moves.Swashbuckling.Riposte) { // Converts combo points into extra damage.
				// Drain all combo points.
				const combo = this.owner.combo;
				this.owner.useCombo(combo);

				base = base + (combo * 2); // bonus base damage from combo points.
			}

			if (command === Moves.Swashbuckling.Behead) { // Chance to do massive damage against enemies at low health
				if (target.health / target.mMaxHealth < 0.5) {
					const chance = (65 - Math.floor((100 * (target.health / target.mMaxHealth))));
					if (chance >= Math.floor(State.random() * 100)) {
						base = target.health;
					}
				}
			}

			base = Math.floor(base * this.move(command).damage); // Add damage mod

			return base;
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		override generateCombo(_target: Combatant, command: Moves.Swashbuckling, _roll: number): number {
			if (
				(command === Moves.Swashbuckling.Slash && this.lastMove === Moves.Swashbuckling.Stab) ||
				(command === Moves.Swashbuckling.Stab && this.lastMove === Moves.Swashbuckling.Slash) ||
				(command === Moves.Swashbuckling.Slash && this.lastMove === Moves.Swashbuckling.Riposte) ||
				(command === Moves.Swashbuckling.Parry)
			) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		override applyEffects(_target: Combatant, command: Moves.Swashbuckling, _roll: number): void {
			if (command === Moves.Swashbuckling.Parry) {
				this.owner.addEffect(Effect.Guarded, 2);
				this.owner.addEffect(Effect.Parry, 2);
			} else if (command === Moves.Swashbuckling.Stab && this.lastMove === Moves.Swashbuckling.Riposte) {
				this.owner.addEffect(Effect.Seeking, 3);
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.moves.Behead.combo) {
				this.attackTarget(target, Moves.Swashbuckling.Behead);
			} else if (this.owner.combo >= this.moves.Cleave.combo && Math.floor(State.random() * 100) >= 60) {
				this.attackTarget(target, Moves.Swashbuckling.Cleave);
			} else if (this.owner.combo >= this.moves.Riposte.combo && this.lastMove === Moves.Swashbuckling.Parry) {
				this.attackTarget(target, Moves.Swashbuckling.Riposte);
			} else if ((this.lastMove !== Moves.Swashbuckling.Riposte && this.lastMove !== Moves.Swashbuckling.Parry)
				&& Math.floor(State.random() * 100) >= 70) {
				this.attackTarget(target, Moves.Swashbuckling.Parry);
			} else if (this.lastMove === Moves.Swashbuckling.Stab) {
				this.attackTarget(target, Moves.Swashbuckling.Slash);
			} else {
				this.attackTarget(target, Moves.Swashbuckling.Stab);
			}
		}
	}

	/**
	 * Boob-jitsu Class
	 * */
	export class Boobjitsu extends Generic<Moves.BoobJitsu> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.BoobJitsu, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		override calculateDamage(_target: Combatant, command: Moves.BoobJitsu, _roll: number): number {
			const base = Math.floor(this.owner.bust / 10) + Math.floor(this.owner.attack / 10);
			return Math.floor(base * this.move(command).damage); // Add damage mod
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Moves.BoobJitsu, _roll: number): number {
			if ((command === Moves.BoobJitsu.Jiggle && this.lastMove === Moves.BoobJitsu.Wobble) ||
				(command === Moves.BoobJitsu.Wobble && this.lastMove === Moves.BoobJitsu.Jiggle)) {
				return 1;
			}

			if (command === Moves.BoobJitsu.BustOut &&
				(this.lastMove === Moves.BoobJitsu.Twirl || this.lastMove === Moves.BoobJitsu.BoobQuake
					|| this.lastMove === Moves.BoobJitsu.TittyTwister)) {
				return 2;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		override applyEffects(target: Combatant, command: Moves.BoobJitsu, _roll: number): void {
			switch (command) {
				case Moves.BoobJitsu.Jiggle: target.addEffect(Effect.Blinded, 3); break;
				case Moves.BoobJitsu.Wobble: this.owner.addEffect(Effect.Bloodthirst, 3); break;
				case Moves.BoobJitsu.BustOut: this.owner.addEffect(Effect.Seeking, 1); break;
				case Moves.BoobJitsu.Twirl: this.owner.addEffect(Effect.LifeLeech, 1); break;
				case Moves.BoobJitsu.BoobQuake:
					if (Math.floor(100 * State.random()) <= (this.owner.attack / 2)) {
						this.stun(target, 3);
					}
					break;
				case Moves.BoobJitsu.TittyTwister:
					if (Math.floor(100 * State.random()) <= this.owner.attack) {
						this.stun(target, 4);
					}
					break;
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.moves[Moves.BoobJitsu.TittyTwister].combo) {
				this.attackTarget(target, Moves.BoobJitsu.TittyTwister);
			} else if (this.owner.combo >= this.moves[Moves.BoobJitsu.BoobQuake].combo && Math.floor(State.random() * 100) >= 60) {
				this.attackTarget(target, Moves.BoobJitsu.BoobQuake);
			} else if (this.owner.combo >= this.moves[Moves.BoobJitsu.Twirl].combo && Math.floor(State.random() * 100) >= 80) {
				this.attackTarget(target, Moves.BoobJitsu.Twirl);
			} else if (this.lastMove && this.lastMove !== 'Miss' &&
				[Moves.BoobJitsu.TittyTwister, Moves.BoobJitsu.BoobQuake, Moves.BoobJitsu.Twirl].includes(this.lastMove)) {
				this.attackTarget(target, Moves.BoobJitsu.BustOut);
			} else if (this.lastMove === Moves.BoobJitsu.Wobble) {
				this.attackTarget(target, Moves.BoobJitsu.Jiggle);
			} else {
				this.attackTarget(target, Moves.BoobJitsu.Wobble);
			}
		}
	}

	/**
	 * Ass-fu Class
	 */
	export class Assfu extends Generic<Moves.AssFu> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.AssFu, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		override calculateDamage(_target: Combatant, command: Moves.AssFu, _roll: number): number {
			const base = Math.floor(this.owner.ass / 10) + Math.floor(this.owner.attack / 10);
			return Math.floor(base * this.move(command).damage); // Add damage mod
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		override generateCombo(_target: Combatant, command: Moves.AssFu, _roll: number): number {
			if ((command === Moves.AssFu.ShakeIt && this.lastMove === Moves.AssFu.BootySlam) ||
				(command === Moves.AssFu.BootySlam && this.lastMove === Moves.AssFu.ShakeIt)) {
				return 1;
			}

			if (command === Moves.AssFu.Twerk &&
				(this.lastMove === Moves.AssFu.AssQuake || this.lastMove === Moves.AssFu.ThunderBuns || this.lastMove === Moves.AssFu.BunsOfSteel)) {
				return 2;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		override applyEffects(target: Combatant, command: Moves.AssFu, _roll: number): void {
			switch (command) {
				case Moves.AssFu.ShakeIt: target.addEffect(Effect.Blinded, 3); break;
				case Moves.AssFu.Twerk: this.owner.addEffect(Effect.Dodging, 3); break;
				case Moves.AssFu.AssQuake:
					if (Math.floor(100 * State.random()) <= (this.owner.attack / 3)) {
						this.stun(target, 3);
					}
					break;
				case Moves.AssFu.ThunderBuns:
					if (Math.floor(100 * State.random()) <= this.owner.attack / 2) {
						this.stun(target, 4);
					}
					break;
				case Moves.AssFu.BunsOfSteel:
					if (Math.floor(100 * State.random()) <= this.owner.attack) {
						this.stun(target, 4);
					}
					this.owner.addEffect(Effect.Guarded, 4);
					break;
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.moves[Moves.AssFu.BunsOfSteel].combo) {
				this.attackTarget(target, Moves.AssFu.BunsOfSteel);
			} else if (this.owner.combo >= this.moves[Moves.AssFu.ThunderBuns].combo && Math.floor(State.random() * 100) >= 60) {
				this.attackTarget(target, Moves.AssFu.ThunderBuns);
			} else if (this.owner.combo >= this.moves[Moves.AssFu.AssQuake].combo && Math.floor(State.random() * 100) >= 80) {
				this.attackTarget(target, Moves.AssFu.AssQuake);
			} else if (this.lastMove === Moves.AssFu.BunsOfSteel || this.lastMove === Moves.AssFu.ThunderBuns
				|| this.lastMove === Moves.AssFu.AssQuake) {
				this.attackTarget(target, Moves.AssFu.Twerk);
			} else if (this.lastMove === Moves.AssFu.BootySlam) {
				this.attackTarget(target, Moves.AssFu.ShakeIt);
			} else {
				this.attackTarget(target, Moves.AssFu.BootySlam);
			}
		}
	}

	// MONSTER CLASSES
	// ====================================================

	// Kraken
	export class Kraken extends Generic<Moves.Kraken> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.Kraken, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		override calculateDamage(_target: Combatant, command: Moves.Kraken, _roll: number): number {
			if (command === Moves.Kraken.Ejaculate1 || command === Moves.Kraken.Ejaculate2) return 0;

			return Math.ceil(Math.max(2, (5 * State.random())) * this.move(command).damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, _command: Moves.Kraken, _roll: number): number {
			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Moves.Kraken, _roll: number): void {
			if (target instanceof Combat.Player) { // No effects on non-player characters.
				const player = target.player;
				switch (command) {
					case Moves.Kraken.Grab:
						this.owner.addEffect(Effect.Seeking, 4);
						break;
					case Moves.Kraken.Ejaculate1:
						player.adjustCoreStatXP(CoreStat.Perversion, 20);
						player.adjustCoreStatXP(CoreStat.Willpower, -20);
						player.adjustBodyXP(BodyPart.Lips, 100);
						player.adjustCoreStatXP(CoreStat.Hormones, 100);
						player.adjustBodyXP(BodyPart.Bust, 100);
						break;
					case Moves.Kraken.Ejaculate2:
						player.adjustCoreStatXP(CoreStat.Perversion, 20);
						player.adjustCoreStatXP(CoreStat.Willpower, -20);
						player.adjustBodyXP(BodyPart.Hips, 100);
						player.adjustCoreStatXP(CoreStat.Hormones, 100);
						player.adjustBodyXP(BodyPart.Ass, 100);
						break;
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.stamina < 30 && (50 >= (100 * State.random()))) {
				this.defend();
				return;
			}

			switch (this.lastMove) {
				case Moves.Kraken.Ejaculate2:
				case Moves.Kraken.Ejaculate1:
					this.attackTarget(target, Moves.Kraken.Strangle);
					break;
				case Moves.Kraken.Ass:
					this.attackTarget(target, Moves.Kraken.Ejaculate2);
					break;
				case Moves.Kraken.Mouth:
					this.attackTarget(target, Moves.Kraken.Ejaculate1);
					break;
				case Moves.Kraken.Strangle:
					if (100 * State.random() >= 50) {
						this.attackTarget(target, Moves.Kraken.Ass);
					} else {
						this.attackTarget(target, Moves.Kraken.Mouth);
					}
					break;
				case Moves.Kraken.Grab:
					this.attackTarget(target, Moves.Kraken.Strangle);
					break;
				default:
					this.attackTarget(target, Moves.Kraken.Grab);
			}
		}
	}

	/**
	 * Siren
	 */
	export class Siren extends Generic<Moves.Siren> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.Siren, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		 override calculateDamage(_target: Combatant, command: Moves.Siren, _roll: number): number {
			return Math.ceil(Math.max(1, ((this.owner.attack / 10) * State.random())) * this.move(command).damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Moves.Siren, _roll: number): number {
			if ((command === Moves.Siren.Touch && this.lastMove === Moves.Siren.Toss) ||
				(command === Moves.Siren.Toss && this.lastMove === Moves.Siren.Touch) ||
				(this.lastMove === 'Miss')) {
				return 1;
			}
			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Moves.Siren, _roll: number): void {
			if (target.isNPC) return; // No effects on non-player characters.

			if (command === Moves.Siren.Scream) {
				this.stun(target, 2);
				this.owner.addEffect(Effect.Bloodthirst, 2);
			} else if (command === Moves.Siren.Drown) {
				target.addEffect(Effect.Blinded, 3);
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.moves[Moves.Siren.Drown].combo) {
				this.attackTarget(target, Moves.Siren.Drown);
			} else if (this.owner.combo >= this.moves[Moves.Siren.Scream].combo && Math.floor(State.random() * 100) >= 75) {
				this.attackTarget(target, Moves.Siren.Scream);
			} else if (this.lastMove === Moves.Siren.Toss) {
				this.attackTarget(target, Moves.Siren.Touch);
			} else {
				this.attackTarget(target, Moves.Siren.Toss);
			}
		}
	}

	/**
	 * Boobpire
	 */
	export class Boobpire extends Generic<Moves.Boobpire> {
		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			super(Style.Boobpire, owner, myStatusCB, theirStatusCB, chatLogCB);
		}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		 override calculateDamage(_target: Combatant, command: Moves.Boobpire, _roll: number): number {
			return Math.ceil(Math.max(1, ((this.owner.attack / 10) * State.random())) * this.move(command).damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		 override generateCombo(_target: Combatant, command: Moves.Boobpire, _roll: number): number {
			if ((command === Moves.Boobpire.Touch && this.lastMove === Moves.Boobpire.Toss) ||
				(command === Moves.Boobpire.Toss && this.lastMove === Moves.Boobpire.Touch)) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		 override applyEffects(target: Combatant, command: Moves.Boobpire, _roll: number): void {
			if (target instanceof Combat.Player) { // No effects on non-player characters.
				if (command === Moves.Boobpire.Bite) {
					this.stun(target, 2);
					this.owner.addEffect(Effect.Bloodthirst, 2);
					if (!this.owner.isNPC) {
						this.printMessage("<span style='color:red'>Your chest feels hot!</span>", target);
						// Drain breast xp
						target.player.adjustBodyXP(BodyPart.Bust, Math.ceil((50 * State.random()) * -1.0));
					}
				}
			}
		}

		override doAI(target: Combatant): void {
			if (this.owner.combo >= this.moves[Moves.Boobpire.Claw].combo) {
				this.attackTarget(target, Moves.Boobpire.Claw);
			} else if (this.owner.combo >= this.owner.moves[Moves.Boobpire.Bite].combo && Math.floor(State.random() * 100) >= 50) {
				this.attackTarget(target, Moves.Boobpire.Bite);
			} else if (this.lastMove === Moves.Boobpire.Toss) {
				this.attackTarget(target, Moves.Boobpire.Touch);
			} else {
				this.attackTarget(target, Moves.Boobpire.Toss);
			}
		}
	}

	const stunMessage: [string, string] = [
		"<span style='color:yellow'>You stun NPC_NAME!</span>",
		"<span style='color:yellow'>NPC_NAME stuns you!</span>",
	];

	const recoverMessage: [string, string] = [
		"<span style='color:lime'>You pull deep from your reserves and catch a second wind!</span>",
		"<span style='color:lime'>NPC_NAME catches a second wind!</span>",
	];

	const defendMessage: [string, string] = [
		"<span style='color:lime'>You assume a defensive position and catch your breath.</span>",
		"<span style='color:lime'>NPC_NAME assumes a defensive position.</span>",
	];

	const parryMessage: [string, string] = [
		"You parry NPC_NAME's attack!",
		"NPC_NAME parries your attack!",
	];

	const healMessage: [string, string] = [
		"You heal <span style='color:lime'>{0}</span> damage.",
		"NPC_NAME heals <span style='color:lime'>{0}</span> damage.",
	];
}
