namespace App.PR {
	/**
	 * Get a list of all effects, but print ????? for ones unlearned.
	 */
	export function getAllEffects(type: Exclude<Items.Category, Items.Category.Quest>, tag: string): string {
		const o = Items.factory(type, tag);
		if (o instanceof Items.Reel) {
			return "";
		}
		const effects = o.getKnowledge();
		const uses = (type === Items.Category.Clothes) ? setup.world.pc.getHistory("clothingEffectsKnown", tag) :
			setup.world.pc.getHistory('items', tag);

		const output: string[] = [];

		for (let i = 0; i < effects.length; i++) {
			if (i < uses) {
				output.push(pEffectMeter(effects[i], o instanceof Items.Equipment ? o.rankNumber : 1));
			} else {
				output.push("?????");
			}
		}

		return output.join(" ");
	}

	/**
	 * Shortcut
	 * @see unitSystem.lengthString
	 */
	export function lengthString(x: number, compact = false): string {
		return unitSystem.lengthString(x, compact);
	}

	/**
	 * Shortcut
	 * @see unitSystem.lengthValue
	 */
	export function lengthValue(x: number): number {
		return unitSystem.lengthValue(x);
	}

	// function isRatedFinalValue(r: Data.RatingFinalValue | Data.SimpleRated | Data.FullRated): r is Data.FullRated {
	// 	return Array.isArray(r) || typeof r === "string";
	// }

	type NontabulatedBodyStats = Extract<BodyStatStr, "hair" | "height">;
	type DirectlyGeneratedRatings = "cup" | "eyes" | "figure" | "makeup" | "hormones";
	type NontabulatedCoreStats = Extract<CoreStatStr, "energy" | "femininity" | "futa" | "health" | "hormones" | "nutrition"
		| "perversion" | "toxicity" | "willpower">;
	type CharacterRatingKey = Exclude<BodyStatStr, NontabulatedBodyStats> |
		Exclude<CoreStatStr, NontabulatedCoreStats> |
		"beauty" | "clothing" | "fetish" | "style";
	/**
	 * Helper function for getting stat configurations.
	 */
	export function statConfig<T extends keyof Data.StatConfigMap>(type: T): Record<StatTypeMap[T], Data.StatConfigMap[T]>;
	export function statConfig<T extends keyof Data.StatConfigStrMap>(type: T): Record<StatTypeStrMap[T], Data.StatConfigStrMap[T]>;
	export function statConfig(type: Stat) {
		switch (type) {
			case Stat.Core: return Data.Lists.coreConfig;
			case Stat.Skill: return Data.Lists.skillConfig;
			case Stat.Body: return Data.Lists.bodyConfig;
		}
	}

	/**
	 * Colorizes and returns a string primitive
	 */
	export function colorizeString(value: number, str: string, maxValue?: number): string {
		return `<span style='color:${UI.colorScale(value, maxValue)}'>${str}</span>`;
	}

	/**
	 * Lookup a body part's configuration entry and figure out the current CM size of it for the player.
	 * @param value
	 * @param statName - body stat with numerical value, like hair, penis, waist, etc.
	 * @param adjust - Optional arg: adjust stat by this.
	 */
	export function statValueToCM(value: number, statName: BodyStat, adjust?: number): number;
	export function statValueToCM(value: number, statName: BodyStatStr, adjust?: number): number;
	export function statValueToCM(value: number, statName: BodyStatStr, adjust = 0): number {
		const statConfigObject = statConfig(Stat.Body)[statName];
		const cmScale = statConfigObject.cmMax - statConfigObject.cmMin;
		const statPercent = Math.floor(((value - statConfigObject.min) / (statConfigObject.max - statConfigObject.min)) * 100);
		return (cmScale * ((statPercent + adjust) / 100)) + statConfigObject.cmMin;
	}

	export function bodyStatValueFromCM(stat: BodyStatStr, cm: number): number {
		const statConfigObject = statConfig(Stat.Body)[stat];
		console.assert(cm >= statConfigObject.cmMin);
		console.assert(cm <= statConfigObject.cmMax);

		const statFraction = (cm - statConfigObject.cmMin) / (statConfigObject.cmMax - statConfigObject.cmMin);
		return Math.round(statConfigObject.min + statFraction * (statConfigObject.max - statConfigObject.min));
	}

	/**
	 * Lookup a body part's configuration entry and figure out the current CM size of it for the player.
	 * @param human
	 * @param statName - currently supported: Bust, Ass, Hips, Waist, Penis
	 * @param adjust - Optional arg: adjust stat by this and report figure.
	 */
	export function statToCm(human: Entity.Human, statName: BodyStat, adjust?: number): number;
	export function statToCm(human: Entity.Human, statName: BodyStatStr, adjust?: number): number;
	export function statToCm(human: Entity.Human, statName: BodyStatStr, adjust = 0): number {
		const statCfg = statConfig(Stat.Body)[statName];
		if (!statCfg) return 0;

		const cmScale = statCfg.cmMax - statCfg.cmMin;
		return (cmScale * ((human.statPercent(Stat.Body, statName) + adjust) / 100))
			+ statCfg.cmMin;
	}

	/**
	 * Convert Ass stat into CM
	 * @todo maybe unused
	 */
	export function assInCM(human: Entity.Human): number {return statToCm(human, BodyPart.Ass);}

	/**
	 * Convert Waist Stat into CM.
	 */
	export function waistInCM(human: Entity.Human): number {return statToCm(human, BodyPart.Waist);}

	/**
	 * Convert Hips Stat into CM.
	 */
	export function hipsInCM(human: Entity.Human): number {return statToCm(human, BodyPart.Hips);}

	/**
	 * Convert Bust Stat into CM.
	 */
	export function busInCM(human: Entity.Human): number {return statToCm(human, BodyPart.Bust);}

	/**
	 * Return a string describing and coloring the effect
	 */
	export function pEffectMeter(effect: string, rank = 1): string {
		let output = "";
		const effectString = effect.replaceAll(' ', '&nbsp;');

		// Build color and arrow
		if (effect.includes('-')) {
			output = "<span class='state-negative'>" + effectString.replaceAll('-', '&dArr;') + "</span>";
		} else {
			if (effect.includes('+')) {
				output = "<span class='state-positive'>" + effectString.replaceAll('+', '&uArr;') + "</span>";
			} else {
				output = effect.includes('?') ? "<span class='state-positive'>&uArr;" + effectString + "</span>" : `<span class='state-neutral'>${effectString}</span>`;
				output = output.replaceAll('RANK', "&uArr;".repeat(rank));
			}
		}
		return output;
	}

	/**
	 * Finds and prints out the NPC quest dialog as a string.
	 * @param questId - ID of the Quest.
	 * @param stage - INTRO, MIDDLE, FINISH
	 * @param player
	 * @param npc - String, ID of the NPC in player.NPCs array.
	 */
	export function pQuestDialog(questId: string, stage: QuestStage, player: Entity.Player, npc: Entity.NPC): string {
		return tokenizeString(player, npc, Data.quests[questId][stage]);
	}

	/**
	 * Print the description of an item.
	 */
	/*
	function pItemDesc(itemType: Items.Category, tag: string, amount: number, opt?: boolean): string {
		const oItem = Items.factory(itemType, tag);
		if (opt && amount > 1) return oItem.description + " x " + amount;
		return oItem.description;
	}
	*/

	export type HumanAspectDescriptionGenerator = (actor: Text.ActorHuman,
		options?: Text.Human.AspectDescriptionOptions, viewer?: Text.ActorHuman) => string;

	type CharacterAspect = CharacterRatingKey | NontabulatedBodyStats | DirectlyGeneratedRatings;
	/**
	 * Print description for rated human properties
	 */
	export const pCharacterRating: Record<CharacterAspect, HumanAspectDescriptionGenerator> =
	{
		// body stats
		ass: (actor, options = {}, viewer) => {
			return actor.body.ass.describe(options, viewer);
		},
		balls: (actor, options = {}, viewer) => {
			return actor.body.balls.describe(options, viewer);
		},
		bust: (actor, options = {}, viewer) => {
			return actor.body.bust.describe(options, viewer);
		},
		bustFirmness: (actor, options = {}, viewer) => {
			return actor.body.bustFirmness.describe(options, viewer);
		},
		cup: (actor, options = {}, _viewer) => {
			return actor.body.bust.sizeString(options);
		},
		eyes: (actor, options = {}, viewer) => {
			return actor.body.eyes.describe(options, viewer);
		},
		face: (actor, options = {}, viewer) => {
			return actor.body.face.describe(options, viewer);
		},
		figure: (actor, options = {}, viewer) => {
			return actor.look.figure.describe(options, viewer);
		},
		fitness: (actor, options = {}, viewer) => {
			return actor.core.fitness.describe(options, viewer);
		},
		hips: (actor, options = {}, viewer) => {
			return actor.body.hips.describe(options, viewer);
		},
		hormones: (actor, options = {}, viewer) => {
			return actor.core.hormones.describe(options, viewer);
		},
		lactation: (actor, options = {}, viewer) => {
			return actor.body.lactation.describe(options, viewer);
		},
		lips: (actor, options = {}, viewer) => {
			return actor.body.lips.describe(options, viewer);
		},
		makeup: (actor, options = {}) => {
			return actor.look.makeup.describe(options);
		},
		penis: (actor, options = {}, viewer) => {
			return actor.body.penis.describe(options, viewer);
		},
		waist: (actor, options = {}, viewer) => {
			return actor.body.waist.describe(options, viewer);
		},
		// unrated body stats
		hair: (actor, options = {}, viewer) => {
			return actor.body.hair.describe(options, viewer);
		},
		height: (actor, options = {}, _viewer) => {
			return actor.body.height.sizeString(options);
		},
		// meta stats
		beauty: (actor, options = {}, viewer) => {
			return actor.look.beauty.describe(options, viewer);
		},
		fetish: (actor, options = {}, viewer) => {
			return actor.look.fetish.describe(options, viewer);
		},
		style: (actor, options = {}, viewer) => {
			return actor.look.style.describe(options, viewer);
		},
		clothing: (actor, options = {}, viewer) => {
			return actor.look.clothing.describe(options, viewer);
		},
	};

	export function humanSelfRating(human: Entity.Human, aspect: CharacterAspect, options: Text.Human.AspectDescriptionOptions = {}): string {
		return pCharacterRating[aspect](human.actor, options);
	}

	/**
     * Prints how does the futa state matches the current body state
     */
	export function pFutaStatus(player: Entity.Player): string {
		const pFuta = player.statPercent(Stat.Core, CoreStat.Futa);
		const hormones = player.stat(Stat.Core, CoreStat.Hormones);
		if ((hormones > 100)) {
			const pPenis = player.statPercent(Stat.Body, BodyPart.Penis);
			const deltaPenis = pFuta - pPenis;
			if (deltaPenis > 90) return "You crave for a bigger penis."
			if (deltaPenis > 60) return "You feel an urge to grow a bigger penis."
			if (deltaPenis > 30) return "You are pretty sure a bigger penis would be a good thing to get."
			if (deltaPenis > 5) return "You feel your penis could be a bit bigger."
			return "You consider your penis size to be about right for you."
		} else {
			const pBust = player.statPercent(Stat.Body, BodyPart.Bust);
			const deltaBust = pFuta - pBust;
			if (deltaBust > 90) return "You crave for bigger tits."
			if (deltaBust > 60) return "You feel an urge to grow your boobs."
			if (deltaBust > 30) return "You are pretty sure bigger tits would be a good thing to get."
			if (deltaBust > 5) return "You feel your bust could be a bit bigger."
			return "You consider your bust size to be about right for you."
		}
	}

	/**
     * Replace tokens in string with calculated/derived literals and return it.
	 * @deprecated
     */
	export function tokenizeString(human: Entity.Human, npc: Entity.NPC | undefined, str: string): string {
		if (npc !== undefined) {
			str = str.replaceAll("NPC_NAME's", `<span class='npc'>${npc.name}'s</span>`);
			str = str.replaceAll("NPC_NAME", `<span class='npc'>${npc.name}</span>`);
			str = str.replaceAll(/NPC_([A-Z_]+!?)/g, (_m, c: string) => {
				const propertyName = _.camelCase(c);
				if (!npc.hasKey(propertyName)) {
					return `ERROR: NO VALUE FOR PROPERTY '${c}' FOR NPC '${npc.name}'`;
				}
				return `${npc[propertyName]}`;
			});
		}

		function adjReplacer(_match: string, stat: string) {
			const statName = _.camelCase(stat) as BodyStatStr;
			return Leveling.adjective(Stat.Body, statName, human.stat(Stat.Body, statName), true);
		}
		function nounReplacer(_match: string, stat: string) {
			return Leveling.humanNoun(Stat.Body, _.camelCase(stat) as BodyPart, human, false, true);
		}
		function pReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> statName
			const statName = _.camelCase(stat);
			if (PR.pCharacterRating.hasKey(statName))
				return `${PR.pCharacterRating[statName](human.actor, {brief: true, colorize: true})}${delim}`;
			return prefix + stat + delim;
		}
		function nReplacer(_match: string, prefix: string, brief: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> statName
			const statName = _.camelCase(stat) as BodyPart;
			if (Data.naming.bodyConfig.hasOwnProperty(statName))
				return Leveling.humanNoun(Stat.Body, statName, human, brief !== null, true) + delim;
			return prefix + stat + delim;
		}
		function vReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> statName
			const statName = _.camelCase(stat);
			if (human.statObject(Stat.Body).hasOwnProperty(statName))
				return `${human.stat(Stat.Body, statName as BodyStat)}${delim}`;
			return prefix + stat + delim;
		}

		// Usage: p(Slot1|Slot2|...|$optional default string)
		// Use with player.isEquipped(string|array,bool) for most cases.
		function equipReplacer(_match: string, part: string) {
			const slots = part.split("|");
			for (const s of slots) {
				if (s.startsWith('$')) return s.slice(1); // default string
				const equip = human.equipmentInSlot(s as ClothingSlot);
				if (equip) return equip.description;
			}
			return "<span style='color:red'>bug!</span>";
		}

		// Like pReplacer, but pass an argument instead of using the characters statistic.
		function pReplacer2(_match: string, _prefix: string, stat: string, statValue: string, delim: string) {
			const statName = _.camelCase(stat);
			const statType: Stat = human.coreStats.hasOwnProperty(statName) ? Stat.Core :
				(human.skills.hasOwnProperty(statName) ? Stat.Skill : Stat.Body);

			return Leveling.adjective(statType, statName as any, Number(statValue), true) + delim;
		}

		function skillReplacer(_match: string, skill: string): string {
			const skillString = _.camelCase(skill) as Skills.Any;
			return Leveling.adjective(Stat.Skill, skillString,
				human.stat(Stat.Skill, skillString), true)
		}

		str = str.replaceAll('PLAYER_NAME', `<span class='state-feminity'>${setup.world.character(Entity.CharacterId.Player).slaveName}</span>`);
		str = str.replaceAll('GF_NAME', `<span class='state-girlinness'>${setup.world.character(Entity.CharacterId.Girlfriend).name}</span>`);
		str = str.replaceAll(/NOUN_([A-Z_a-z]+)/g, nounReplacer);
		str = str.replaceAll(/ADJECTIVE_([A-Z_a-z]+)/g, adjReplacer);
		str = str.replaceAll(/s([A-Z][A-Z_]+)/g, skillReplacer);
		str = str.replaceAll('pPHASE', setup.world.phaseName(false));
		str = str.replaceAll(/pEQUIP\(([^\)]*)\)/g, equipReplacer);
		str = str.replaceAll(/(p)([A-Z]+)_([0-9]+)([^0-9]|$)/g, pReplacer2);
		str = str.replaceAll(/(q)([A-Z_]+)([^A-Za-z]|$)/g, pReplacer);
		str = str.replaceAll(/(p)([A-Z_]+)([^A-Za-z]|$)/g, pReplacer);
		str = str.replaceAll(/(n)(b?)([A-Z_]+)([^A-Za-z]|$)/g, nReplacer);
		str = str.replaceAll(/(v)([A-Z_]+)([^A-Za-z]|$)/g, vReplacer);
		// Hack for highlighting NPC speech
		str = str.replaceAll(/s\(([^)]+)\)/g, (_m, p: string) => `<span class='npc-text'>"${p}"</span>`);
		// Important! highlight NPC speech
		str = str.replaceAll(/s!\(([^)]+)\)/g, (_m, p: string) => `<span class='impText'>"${p}"</span>`);
		// Highlighting PC speech
		str = str.replaceAll(/sp\(([^)]+)\)/g, (_m, p: string) => `<span class='pc-text'>"${p}"</span>`);
		// Highlighting PC thoughts
		str = str.replaceAll(/tp\(([^)]+)\)/g, (_m, p: string) => `<span class='pcThought'>"${p}"</span>`);

		return str;
	}

	export function pSkillName(skill: Skills.Any): string {
		return Data.Lists.skillConfig[skill].altName ?? skill.capitalizeFirstLetter();
	}

	export function pShipMapIcon(index: number): void {
		if (index >= Data.Lists.shipRoute.length) index = 0; // Force reset.
		const top = Data.Lists.shipRoute[index].top;
		const left = Data.Lists.shipRoute[index].left;
		console.debug(`Placing map icon at top=${top}, left=${left}`);
		$(document).one(":passageend", () => {$("#mapIcon").css({top: top, left: left});});
	}

	export function phaseName(phase: DayPhase): string {
		return phase >= DayPhase.Morning && phase <= DayPhase.LateNight ?
			["morning", "afternoon", "evening", "night", "late night"][phase] : phase.toString();
	}

	/**
     * Get icon for marking favorite item in inventory or shop lists
     */
	export function getItemFavoriteIcon(isFavorite: boolean): string {
		return isFavorite ? "<span class='action-general'>&#9733;</span>" : "<span>&#9734;</span>";
	}

	/**
     * Get random item from inventory
     */
	export function getRandomItemId(human: Entity.Human): string {
		const itemTypeCount = human.inventoryItemsCount();
		let randomIndex = Math.floor(State.random() * itemTypeCount);
		let id = "";
		human.inventory.everyItemRecord((n, tag, itemClass) => {
			if (randomIndex <= 0) {
				id = Items.makeId(itemClass, tag);
				return false;
			}
			randomIndex -= n;
			return true;
		});
		return id;
	}

	export function refreshTwineMoney(): void {
		try {
			$("#Money").text(setup.world.pc.money.toString());
		} catch {}
	}

	/*
	function refreshTwineTokens(): void {
		try {
			$("#Tokens").text(setup.world.pc.tokens.toString());
		} catch (err) {

		}
	}
	*/

	export function pHormoneSymbol(): string {
		const value = setup.world.pc.coreStats.hormones;

		if (value < 78) {
			return "<span id='hormone-symbol' class='state-masculinity'>♂</span>";
		} else if (value >= 144) {
			return "<span id='hormone-symbol' class='state-feminity'>♀</span>";
		} else {
			return "<span id='hormone-symbol' class='state-neutral'>⚥</span>";
		}
	}

	export function refreshTwineMeter(m: CoreStat): void {
		const meterContainer = document.querySelector(`#${m}`);
		assertIsDefined(meterContainer);
		meterContainer.textContent = '';
		meterContainer.append(UI.rCoreStatMeter(m, setup.world.pc));
		if (m === CoreStat.Hormones) {
			meterContainer.innerHTML += PR.pHormoneSymbol();
		}
	}

	/*
	function refreshSlaveName() {
		try {
			$("#scoreSlaveName").text('"' + setup.world.pc.slaveName + '"');
		} catch (err) {

		}
	}
	*/

	export function refreshTwineScore(): void {
		// Redraw Energy Bars
		try {
			refreshTwineMeter(CoreStat.Health);
			refreshTwineMeter(CoreStat.Energy);
			refreshTwineMeter(CoreStat.Willpower);
			refreshTwineMeter(CoreStat.Perversion);
			refreshTwineMeter(CoreStat.Nutrition);
			refreshTwineMeter(CoreStat.Femininity);
			refreshTwineMeter(CoreStat.Toxicity);
			refreshTwineMeter(CoreStat.Hormones);
		} catch {}
	}

	/**
     * Prints item description for the inventory list
     */
	export function printItem(item: Items.IEquipmentItem | Items.Consumable | Items.QuestItem, player: Entity.Player): string {
		let res = "<span class='inventoryItem'>" + item.description;
		if (settings.inlineItemDetails) {
			res += '<span class="tooltip">' + item.examine(player, false) + '</span></span>';
			res += "<br><div class='inventoryItemDetails'>" + item.examine(player, true) + '</div>';
		} else {
			res += '</span>';
		}
		return res;
	}

	/**
     * Highlight active button in a tabbar
     *
     * Finds the active element and appends " active" to its style, removing " active" from all
     * other children of the tabbar.
     * @param tabbarId  Id of the tab bar element
     * @param activeButtonId Id of the button for the active tab
     * @param activeTabText Text to set for .activeTabCaption children of the tab bar
     *
     * @example <div id="tabbar">
     *  <span class="activeTabCaption">placeholder text</span>
     *  <span class="tablink" id="btn1"><button class="mybutton">Button1</button></span>
     *  <span class="tablink" id="btn2"><button class="mybutton">Button2</button></span>
     * </div>
     *
     * Then calling HighlightActiveTabButton("tabbar", "btn1", "Sample text")
     *
     * will replace "placeholder text" with "Sample text" and append " active" to btn1 class:
     * <span class="tablink" id="btn1"><button class="mybutton active">Button1</button></span>
     *
     * The next call HighlightActiveTabButton("tabbar", "btn2", "Sample text2") will result in:
     * <div id="tabbar">
     *  <span class="activeTabCaption">Sample text2</span>
     *  <span class="tablink" id="btn1"><button class="mybutton">Button1</button></span>
     *  <span class="tablink" id="btn2"><button class="mybutton active">Button2</button></span>
     * </div>
     */
	export function highlightActiveTabButton(tabbarId: string, activeButtonId: string, activeTabText: string): void {
		const tabBar = document.getElementById(tabbarId);
		if (!tabBar) return;
		const tabs = tabBar.getElementsByClassName("tablink");
		for (const element of tabs) {
			if (element.firstChild instanceof HTMLElement) {
				element.firstChild.className = element.firstChild.className.replace(" active", "");
			}
		}
		const linkElement = document.getElementById(activeButtonId);
		if (linkElement && linkElement.firstChild instanceof HTMLElement) linkElement.firstChild.className += " active";
		const activeTab = tabBar.getElementsByClassName("activeTabCaption");
		for (const element of activeTab) {
			if (element instanceof HTMLElement) {
				element.innerText = activeTabText;
			}
		}
	}

	export function risingDialog(element: string | HTMLElement, message: string, color?: string): void;
	export function risingDialog(element: any, message: string, color = "white"): void {
		const root = $(element);
		$('#WhoreDialogDiv2').remove();

		const div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv2');
		const header = $('<h1>').addClass('ml13').html(message);
		header.css('color', color);
		div.append(header);
		root.append(div)

		// Wrap every letter in a span
		$('.ml13').each(function () {
			$(this).html($(this).text().replaceAll(/([^\u0000-\u0080]|\w)/g, "<span class='letter'>$&</span>"));
		});

		anime.timeline({loop: false})
			.add({
				targets: '.ml13 .letter',
				translateY: [100, 0],
				translateZ: 0,
				opacity: [0, 1],
				easing: "easeOutExpo",
				duration: 1000,
				delay: function (_element, i) {
					return 300 + 30 * i;
				},
			}).add({
				targets: '.ml13 .letter',
				translateY: [0, -100],
				opacity: [1, 0],
				easing: "easeInExpo",
				duration: 1000,
				delay: function (_element, i) {
					return 100 + 30 * i;
				},
			});
	}

	export function dialogBox(element: HTMLElement, message: string, properties?: Record<string, string | number>): void;
	export function dialogBox(element: string, message: string, properties?: Record<string, string | number>): void;
	export function dialogBox(element: any, message: string, properties: JQuery.PlainObject<string|number> = {}): void {
		const bgColor = properties.color;
		const fgColor = properties.fgColor;
		const lineProperties: JQuery.PlainObject<string | number> = bgColor !== undefined ? {"background-color": bgColor} : {};
		const textProperties: JQuery.PlainObject<string | number> = fgColor !== undefined ? {color: fgColor} : {};
		const root = $(element);
		$('#WhoreDialogDiv').remove();

		const div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv').css(properties);
		const header = $('<h1>').addClass('ml1');
		const inner = $('<span>').addClass('text-wrapper');

		inner.append($('<span>').addClass('line line1').css(lineProperties));
		inner.append($('<span>').addClass('letters').html(message).css(textProperties));
		inner.append($('<span>').addClass('line line2').css(lineProperties));

		header.append(inner);
		div.append(header);
		root.append(div);

		// Javascript animations.
		$('.ml1 .letters').each(function () {
			$(this).html($(this).text().replaceAll(/([^\u0000-\u0080]|\w)/g, "<span class='letter'>$&</span>"));
		});

		anime.timeline({loop: false})
			.add({
				targets: '.ml1 .letter',
				scale: [0.3, 1],
				opacity: [0, 1],
				translateZ: 0,
				easing: "easeOutExpo",
				duration: 600,
				delay: function (_element, i) {
					return 70 * (i + 1)
				},
			}).add({
				targets: '.ml1 .line',
				scaleX: [0, 1],
				opacity: [0.5, 1],
				easing: "easeOutExpo",
				duration: 700,
				offset: '-=875',
				delay: function (_element, i, l) {
					return 80 * (l - i);
				},
			}).add({
				targets: '.ml1',
				opacity: 0,
				duration: 1000,
				easing: "easeOutExpo",
				// delay: 1000
				// delay: 500
			});
	}

	// Stuff to support fight club

	export function fightClubFlag(club: string, flag?: string): string {
		const base = "FIGHTCLUB_TRACK_" + club.replaceAll(' ', "_");
		return flag ? base + '_' + flag.toUpperCase() : base;
	}

	export function addFightClubResult(player: Entity.Player, club: string, victory: boolean): number {
		const key = victory ? fightClubFlag(club) + "_WINS" : fightClubFlag(club) + "_LOSSES";
		player.flags[key] = player.flags.hasOwnProperty(key) ? 1 + <number>player.flags[key] : 1;

		return <number>player.flags[key];
	}

	export function playerStatChooser<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], steps: number| number[]): string {
		const res: string[] = [];

		let values: number[] = [];
		if (typeof steps === "number") {
			const cfg = statConfig(type)[stat];
			const step = (cfg.max - cfg.min) / steps;
			for (let v = cfg.min; v <= cfg.max; v += step) {
				values.push(v);
			}
		} else {
			values = steps;
		}

		function setStat(type: T, stat: StatTypeMap[T], value: number) {
			setup.world.pc.setStat(type, stat, value);
		}

		for (const v of values) {
			const adj = Leveling.adjective(type, stat, v, true);
			const strValue = adj.length > 0 ? adj : lengthString(statValueToCM(v, stat as BodyStatStr))
			res.push(UI.link(strValue, setStat, [type, stat, v]));
		}
		return res.join(" | ");
	}

	/**
 	 * Handler for the meters 'print numbers' setting
 	 */
	export function handleMetersNumberValueSettingChanged(): void {
		if (settings.displayMeterNumber) {
			// to accommodate longer meters
			$('#ui-bar').css("width", "350px"); // seems to work better on most browsers
		} else {
			$('#ui-bar').css("width", "330px");
		}

		PR.refreshTwineScore();
	}

	export function handleDisplayBodyScoreChanged(): void {
		if (!settings.displayBodyScore) {
			$('#bodyScoreContainer').empty();
		}
	}

	/**
	 * Returns the phase icon for the current phase.
	 */
	export function phaseIcon(phase: DayPhase): string {
		switch (phase) {
			case DayPhase.Morning: return "@@color:yellow;&#9788;@@";
			case DayPhase.Afternoon: return "@@color:orange;&#9728;@@";
			case DayPhase.Evening: return "@@color:azure;&#9734;@@";
			case DayPhase.Night: return "@@color:cornflowerblue;&#9789;@@";
			case DayPhase.LateNight: return "@@color:DeepPink;&#9789;@@";
		}
	}

	export function phaseIconStrip(): string {
		let res = '';
		const phase = setup.world.state.phase;
		for (let i = DayPhase.Morning; i <= DayPhase.LateNight; ++i) {
			res += i === phase ? `[${phaseIcon(i)}]` : phaseIcon(i);
		}
		return res;
	}

	export function numberToWords(value: number): string {
		value = Math.floor(value)

		if (value < 0) throw new Error('Negative numbers are not supported.')
		if (value === 0) return 'zero'

		const ones = [
			'', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
			'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen',
		];
		const tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

		const numberString = value.toString(10);

		switch (true) {
			case (value < 20):
				return ones[value];
			case (value < 1e2):
				return tens[Number(numberString[0])] + ' ' + ones[Number(numberString[1])];
			case (value < 1e3):
				return numberString[1] === '0' && numberString[2] === '0' ?
					ones[Number(numberString[0])] + ' hundred' :
					ones[Number(numberString[0])] + ' hundred and ' + numberToWords(+(numberString[1] + numberString[2]));
			case (value < 1e4): {
				const end = +(numberString[1] + numberString[2] + numberString[3])
				if (end === 0) return ones[Number(numberString[0])] + ' thousand'
				if (end < 100)
					return (
						ones[Number(numberString[0])] +
						' thousand and ' +
						numberToWords(end)
					)
				return (
					ones[Number(numberString[0])] + ' thousand ' + numberToWords(end)
				)
			}
			case (value < 1e6): {
				const thousands = `${numberToWords(value / 1e3)} thousands`;
				return Math.floor(value % 1000) === 0 ? thousands : `${thousands} and ${numberToWords(value % 1000)}`;
			}
			default:
				return numberString;
		}
	}

	export function isGameStateAvailable(): boolean {
		return State.variables.difficultySetting >= GameState.Difficulty.Normal; // this is presumable always defined
	}

	export function gameLeftIntro(): boolean {
		return isGameStateAvailable() && !State.variables.inIntro;
	}

	/**
	 * Is the game in the interactive state (i.e. not in intro or something)
	 */
	export function isGameInInteractiveState(): boolean {
		return gameLeftIntro();
	}

	export function passageTitle(): string | null {
		const passageName = passage();
		if (passageName.startsWith("GI_")) return `Golden Isle ${passageName.slice(3)}`;
		return passageName;
	}
}
