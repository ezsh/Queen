Object.defineProperty(HTMLElement.prototype, 'appendNewElement', {
	value(this: HTMLElement, tag: keyof HTMLElementTagNameMap, properties?: App.UI.ElementProperties) {
		const element = App.UI.makeElement(tag, properties);
		this.append(element);
		return element;
	},
});

Object.defineProperty(HTMLElement.prototype, 'appendTextNode', {
	value(this: HTMLElement, text: string) {
		this.append(document.createTextNode(text));
	},
});

Object.defineProperty(HTMLElement.prototype, 'appendFormattedFragment', {
	value(this: HTMLElement, text: App.UI.FormattedFragment) {
		return this.appendNewElement('span', {content: text.text, classNames: text.style});
	},
});

Object.defineProperty(HTMLElement.prototype, 'appendFormattedText', {
	value(this: HTMLElement, ...text: (string | App.UI.FormattedFragment)[]) {
		for (const t of text) {
			if (typeof t === "string") {
				$(this).wiki(t);
			} else {
				this.appendNewElement('span', {content: t.text, classNames: t.style});
			}
		}
	},
});

interface HTMLElement {
	appendNewElement<T extends keyof HTMLElementTagNameMap>(
		tag: T, properties?: App.UI.ElementProperties): HTMLElementTagNameMap[T];
	appendTextNode(text: string): void;
	appendFormattedFragment(text: App.UI.FormattedFragment): HTMLSpanElement;
	appendFormattedText(...text: (string | App.UI.FormattedFragment)[]): void;
}
