namespace App.UI.Widgets {
	export abstract class Tab {
		readonly #id: string;
		readonly #shortName: string;
		readonly #longName: string;
		readonly #buttonStyle?: string;

		constructor(id: string, shortName: string, longName: string, buttonStyle?: string) {
			this.#id = id;
			this.#shortName = shortName;
			this.#longName = longName;
			this.#buttonStyle = buttonStyle;
		}

		get id(): string {
			return this.#id;
		}

		get shortName(): string {
			return this.#shortName;
		}

		get longName(): string {
			return this.#longName;
		}

		get buttonStyle(): string | undefined {
			return this.#buttonStyle;
		}

		abstract render(): Node;
	}

	interface TabData {
		tab: Tab;
		tabButton: HTMLSpanElement;
		content: HTMLDivElement;
	}

	export class TabWidget {
		readonly #tabs: TabData[] = [];
		readonly #element: HTMLDivElement;
		readonly #content: HTMLDivElement;
		readonly #tabbar: HTMLDivElement;
		readonly #activeTabCaption: HTMLSpanElement;
		readonly #tabButtons: HTMLDivElement;
		#selectedTabId = "";

		constructor() {
			this.#element = document.createElement('div');
			this.#tabbar = appendNewElement('div', this.#element, {classNames: ['tabbar']});
			this.#content = appendNewElement('div', this.#element);
			this.#activeTabCaption = appendNewElement('span', this.#tabbar, {classNames: ['activeTabCaption']});
			this.#tabButtons = appendNewElement('div', this.#tabbar);
			this.#tabButtons.style.float = "right";
		}

		addTab(tab: Tab): void {
			// this.#tabs.push(tab);
			const tabButton = appendNewElement('button', this.#tabButtons, {content: tab.shortName, classNames: ['tablink']});
			if (tab.buttonStyle) {
				tabButton.classList.add(tab.buttonStyle);
			}
			tabButton.addEventListener('click', () => {
				this.selectTab(tab);
			})
			const content = document.createElement('div');
			content.append(tab.render());
			this.#tabs.push({tab: tab, tabButton: tabButton, content: content});
		}

		selectTab(tab: Tab): void {
			const newTabIndex = this.#tabs.findIndex(t => t.tab.id === tab.id);
			if (newTabIndex < 0) {
				throw `Can't find tab with id '${tab.id}'`;
			}

			this.selectTabByIndex(newTabIndex);
		}

		selectTabByIndex(index: number): void {
			const newTab = this.#tabs[index];

			const currentTab = this.#tabs.find(t => t.tab.id === this.#selectedTabId);
			if (currentTab) {
				currentTab.tabButton.classList.remove('active');
			}

			this.#selectedTabId = newTab.tab.id;
			App.UI.replace(this.#content, newTab.content);
			App.UI.replace(this.#activeTabCaption, newTab.tab.longName);
			newTab.tabButton.classList.add('active');
		}

		refreshTab(tab: Tab): void {
			const currentTab = this.#tabs.find(t => t.tab.id === tab.id);
			if (currentTab) {
				App.UI.replace(currentTab.content, currentTab.tab.render());
			} else {
				console.warn("Could not find tab with id", tab.id);
			}
		}

		get element(): HTMLDivElement {
			return this.#element;
		}
	}
}
