namespace App {

	export type GameVariable = boolean | number | string | undefined;

	const enum ProgressTypePrefix {
		Progress = "progress",
		TrackCustomers = "track",
	}

	interface ProgressVariable {
		name: string,
		type: Data.Tasks.ProgressType | null;
	}

	function progressType(name: string): ProgressVariable {
		if (name.startsWith(`${ProgressTypePrefix.Progress}:`)) {
			return {name: name.slice(ProgressTypePrefix.Progress.length + 1), type: Data.Tasks.ProgressType.Progress};
		} else if (name.startsWith(`${ProgressTypePrefix.TrackCustomers}:`)) {
			return {name: name.slice(ProgressTypePrefix.TrackCustomers.length + 1), type: Data.Tasks.ProgressType.TrackCustomers};
		}
		return {name, type: null};
	}

	export function trackCustomersVariableName(key: string): string {
		return `${ProgressTypePrefix.TrackCustomers}:${key}`;
	}

	export function trackProgressVariableName(key: string): string {
		return `${ProgressTypePrefix.Progress}:${key}`;
	}

	/**
	 * Represents a basic task for the player, either a job or a quest.
	 */
	export abstract class Task {
		private readonly _id: string;
		private readonly _tags: Set<string>;
		private readonly _giverId: string;
		private readonly _taskData: Data.Tasks.Task;

		constructor(id: string, data: Data.Tasks.Task, giverOverride?: string) {
			console.assert(data !== undefined);
			this._id = id;
			this._tags = new Set(data.tags ?? []);
			this._giverId = giverOverride ?? ensureIsDefined(data.giver);
			this._taskData = data;
		}

		get taskData(): Data.Tasks.Task {
			return this._taskData;
		}

		get id(): string {return this._id;}
		title(): string {return this.taskData.title;}
		get tags(): Set<string> {return this._tags;}
		get giverId(): string {return this._giverId;}
		get giver(): Entity.NPC {return setup.world.npc(this._giverId);}

		/**
		 * Check to see if the player meets the other requirements to take the task.
		 * Usually skill, stat, body related or quest flags.
		 * @param requirements
		 */
		static evaluateRequirements(requirements: Data.Conditions.Expression,
			definitionContext: DefinitionContext,
			evaluationContext?: Conditions.EvaluationContext
		): Conditions.ComputedExpression {
			return new Conditions.Expression(requirements).compute(
				evaluationContext ?? new Conditions.EvaluationContext(definitionContext, Conditions.EvaluationContext.REQUIREMENTS_CHECK_DEFAULTS));
		}

		static checkRequirements(requirements: Data.Conditions.Expression,
			definitionContext: DefinitionContext,
			evaluationContext?: Conditions.EvaluationContext
		): boolean {
			return new Conditions.Expression(requirements).evaluateFast(
				evaluationContext ?? new Conditions.EvaluationContext(definitionContext, Conditions.EvaluationContext.REQUIREMENTS_CHECK_DEFAULTS));
		}

		protected abstract availableScenes(player: Entity.Player): Scene[];

		get definitionContext(): DefinitionContext {
			return this.makeDefinitionContext();
		}

		makeActionContext(world?: Entity.World): Actions.EvaluationContext {
			return new Actions.EvaluationContext(new Conditions.EvaluationContext(this.makeDefinitionContext(world)));
		}

		/**
		 * Play the selected task scenes.
		 * @virtual
		 */
		playScenes(ctx: Actions.EvaluationContext): CalculatedScene[] {
			Task.debug("PlayScenes", "Started");
			const res: CalculatedScene[] = [];

			const scenes = this.availableScenes(ctx.player);
			for (const s of scenes) {
				const sr = s.calculate(ctx);
				if (sr) {
					res.push(sr);
				}
			}

			Task.debug("PlayScenes", "Ended");
			return res
		}

		/**
		 * Tokenize a string and return result.
		 */
		// eslint-disable-next-line class-methods-use-this
		protected tokenize(ctx: Actions.EvaluationContext, _scenes: CalculatedScene[], str?: string): string {
			if (str === undefined) return "";
			return PR.tokenizeString(ctx.player, this.giver, str);
		}

		state(player: Entity.Player): GameState.TasksState | undefined;
		state(player: Entity.Player, forceCreate: true): GameState.TasksState;
		state(player: Entity.Player, forceCreate: boolean): GameState.TasksState | undefined;
		state(player: Entity.Player, forceCreate = false): GameState.TasksState | undefined {
			const state = this.getState(player);
			if (forceCreate && !state.hasOwnProperty(this._id)) {
				state[this._id] = this.createNewState();
			}
			return state[this._id];
		}

		removeState(player: Entity.Player): void {
			const state = this.getState(player);
			delete state[this._id];
		}

		/**
		 * Retrieve a task related flag.
		 */
		variable<T extends GameVariable = GameVariable>(player: Entity.Player, name: string, defaultValue?: T): T {
			const stateVariable = this.state(player)?.[name];
			if (stateVariable === undefined) {
				const prType = progressType(name);
				if (prType.type !== null && this.taskData.progressMeters) {
					for (const progressVariable of Object.entries(this.taskData.progressMeters)) {
						if (prType.type === progressVariable[1] && progressVariable[0] === prType.name) {
							return 0 as T;
						}
					}
					throw new Error(`No progress meter '${prType.name}' declared for task '${this._id}'`);
				}
				const initialVariable = this.taskData.variables?.[name] ?? defaultValue;
				if (initialVariable === undefined) {
					throw new Error(`Variable ${name} undeclared for task ${this._id}`);
				}
				return initialVariable as T;
			}

			return stateVariable as T;
		}

		/**
		 * Set a quest flag value.
		 */
		setVariable(player: Entity.Player, flag: string, value: GameVariable): void {
			if (value === undefined) {
				delete this.state(player)?.[flag];
			} else {
				this.state(player, true)[flag] = value;
			}
		}

		/**
		 * Helper function to read progress value
		 * @param player - player being checked.
		 * @param key - key from check entry.
		 */
		getProgressValue(player: Entity.Player, key: string): number {
			const res = this.variable(player, trackProgressVariableName(key));
			return res === undefined ? 0. : res as number;
		}

		/**
		 * Helper function to read progress value
		 * @param player - player being checked.
		 * @param key - key from check entry.
		 * @param value value or undefined to remove the flag
		 */
		setProgressValue(player: Entity.Player, key: string, value?: number): void {
			this.setVariable(player, trackProgressVariableName(key), value);
		}

		protected abstract getState(player: Entity.Player): Partial<Record<string, GameState.TasksState>>;

		static get debugPrintEnabled(): boolean {
			return Task._debug;
		}

		static set debugPrintEnabled(v: boolean) {
			Task._debug = v;
		}

		static debug(fun: string, str: string): void {
			if (Task.debugPrintEnabled)
				console.debug(fun + ":" + str + "\n");
		}

		private static _debug: boolean;

		protected createNewState(): GameState.TasksState {
			const res: GameState.TasksState = {};
			for (const k in this.taskData.variables) {
				res[k] = clone(this.taskData.variables[k]);
			}
			return res;
		}

		protected makeDefinitionContext(world?: Entity.World) {
			const w = world ?? setup.world;
			return {world: w, npc: this.giver, task: this};
		}
	}
}
