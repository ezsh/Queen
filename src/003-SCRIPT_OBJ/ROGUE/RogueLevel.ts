
namespace App.Rogue {
	export class Level {
		readonly #depth: number;

		/* FIXME data structure for storing entities */
		readonly #beings: Map<number, Entity> = new Map<number, Being>();

		/* FIXME map data */
		readonly #size = new Rogue.XY(80, 40);
		#map!: InstanceType<typeof ROT.Map.Cellular>;
		readonly #walls: Map<number, Entity> = new Map<number, Entity>();
		readonly #freeCells: Map<number, Entity> = new Map<number, Entity>();
		readonly #borders: Map<number, Entity> = new Map<number, Entity>();
		#entrance!: number; // encoded XY
		#exit!: number; // encoded XY
		readonly #treasure: Map<number, XY> = new Map<number, XY>(); // FIXME get rid of this
		readonly #empty = new Rogue.Entity('', {ch: null, fg: null, bg: null});
		readonly #wall = new Rogue.Entity('wall', {ch: null, fg: null, bg: "#888"});

		constructor(depth: number) {
			this.#depth = depth;
			this.#beings = new Map<number, Being>();
		}

		getSize(): XY {
			return this.#size;
		}

		get freeCells() {
			return this.#freeCells;
		}

		get depth(): number {
			return this.#depth;
		}

		get beings() {
			return this.#beings;
		}

		/**
		 * Create a Cave map.
		 * 0 = free space, 1 = wall;
		 * @param w width (x axis)
		 * @param h height (y axis)
		 */

		generateMap(w: number, h: number) {
			this.#map = new ROT.Map.Cellular(w, h);
			this.#map.randomize(0.6 - this.#depth / 1000);

			/* make a few generations */
			for (let i = 0; i < 5; i++) {
				this.#map.create();
			}

			this.#map.connect((x, y, c) => {this._createCB(x, y, c);}, 1, (from, to) => {this._connectCB(from, to);});
		}

		private _createCB(x: number, y: number, content: number) {
			const xy = encodeXy(x, y);
			if (content === 1) {
				this.#freeCells.set(xy, new Rogue.Entity('', {ch: null, fg: null, bg: null})); // TODO use #empty
				// this._freeCells[xy].setPosition(xy, this);
			} else {
				this.#walls.set(xy, this.#wall);
			}
		}

		private _connectCB(_from:[number, number], _to: [number, number]) {
			// console.log("from="+from+", to="+to);
			// this._cells[from] = new Rogue.Entity( { ch:"X", fg:"#777", bg:null });
			// this._cells[to] = new Rogue.Entity( { ch:"O", fg:"#777", bg:null });
		}

		getWalls() {return this.#walls;}
		getFreeCells() {return this.#freeCells;}
		getBorders() {return this.#borders;}

		/**
		 * Fill in the borders of the map.
		 * Generate an entrance and save it.
		 * @param w width, x axis
		 * @param h height, y axis
		 */
		fillBorders(w: number, h: number) {
			for (let i = 0; i < w; i++) {
				// Top of map.
				let linearIndex = encodeXy(i, 0);
				this.#freeCells.delete(linearIndex);
				this.#walls.delete(linearIndex);
				this.#borders.set(linearIndex, this.#wall);

				// Bottom of map.
				linearIndex = encodeXy(i, h - 1);
				this.#freeCells.delete(linearIndex);
				this.#walls.delete(linearIndex);
				this.#borders.set(linearIndex, this.#wall);
			}

			for (let i = 0; i < h; i++) {
				// Left of map.
				let linearIndex = encodeXy(0, i);
				this.#freeCells.delete(linearIndex);
				this.#walls.delete(linearIndex);
				this.#borders.set(linearIndex, this.#wall);

				// Right of map.
				linearIndex = encodeXy(w - 1, i);
				this.#freeCells.delete(linearIndex);
				this.#walls.delete(linearIndex);
				this.#borders.set(linearIndex, this.#wall);
			}
		}

		cellsAtRadius(x: number, y: number, r: number, free: boolean): XY[] {
			const cells: XY[] = [];

			const startY = Math.max(0, (y - r));
			const endY = Math.min(this.#size.y - 1, (y + r));
			const startX = Math.max(0, x - r);
			const endX = Math.min(this.#size.x - 1, x + r);

			// console.log("startY="+startY+",endY="+endY);

			for (let row = startY; row <= endY; row++) {
				const dy = row - y;
				// const rangeX = r - Math.abs(row - y);
				// console.log("rangeX"+rangeX+",startX="+startX+",endX="+endX);

				for (let col = startX; col <= endX; col++) {
					const dx = col - x;
					if (Math.hypot(dx, dy) > r) continue;
					const xy = new XY(col, row);
					if (!free || (free && this.#freeCells.has(xyToNumber(xy)))) {
						cells.push(xy);
					}
				}
			}

			return cells;
		}

		cellsOutsideRadius(x: number, y: number, r: number, free: boolean) {
			// Get inclusion cells
			const inside = new Set<number>(this.cellsAtRadius(x, y, r, free).map(xy => xyToNumber(xy)));

			let cells = [...this.#freeCells.keys()].filter(key => !inside.has(key));
			if (!free) cells = cells.concat([...this.#walls.keys()].filter(key => !inside.has(key)));

			return cells;
		}

		/**
		 * Each  level can have up to 10 treasure spots on it. The chance is basically 1% per depth for each spot.
		 */
		genTreasure() {
			let count = 0;
			for (let i = 0; i < 10; i++) {
				if ((Math.round(State.random() * 100) + 1) <= 100) count++;
			}

			if (count < 1) return; // noop
			let cells = [...this.#freeCells.keys()];
			// Remove stairs from array
			cells = cells.filter(c => c !== this.#exit && c !== this.#entrance);
			for (let i = 0; i < count; i++) {
				const treasure = cells.randomElement();
				this.#treasure.set(treasure, numberToXY(treasure));
				const trEntity = new Rogue.Entity('dig_spot', {ch: '*', fg: '#A52A2A', bg: null});
				trEntity.type = 'dig_spot';
				this.#freeCells.set(treasure, trEntity);
			}
		}

		genMonsters() {
			console.log("genMonsters() called");
			// Select generic monster encounters.
			const tLevel = this.#depth;
			console.log(`tLevel=${tLevel}`);
			const mobs = Data.Abamond.mobs['COMMON'].filter(o => o.level >= (tLevel - 10) && o.level <= tLevel);

			console.log(mobs);
			let mobPool = ((tLevel * 5) * State.random())

			while (mobPool > 0 && mobs.length > 0) {
				const m = mobs[Math.floor(State.random() * mobs.length)];
				mobPool -= m.level;
				this._placeMonster(m);
			}
		}

		// Place the monster we just generated
		private _placeMonster(mob: Data.ModDesc) {
			const entity = new Being(mob.name, {ch: mob.symbol, fg: mob.color, bg: null});
			entity.encounter = mob.encounter;
			const cells = [...this.#freeCells.keys()].filter((c) => {
				return c !== this.#exit && c !== this.#entrance && !this.#beings.has(c);
			});
			const place = cells.randomElement();
			const xy = numberToXY(place);
			console.log(`Placing mob: ${mob.name} at ${xy.toString()}`);
			this.setEntity(entity, xy);
		}

		isTreasure(xy: XY) {
			return this.#treasure.has(xyToNumber(xy));
		}

		/**
		 * Real gross hack for now, need to fix with future refactor
		 */
		digAt(xy: XY) {
			const linearIndex = xyToNumber(xy);
			const nothing = (50 - this.#depth);
			const genLoot = (coins: number): Data.LootItem => {
				if (coins > 400) { // legendary
					return Data.loot.DUNGEON_LEGENDARY.randomElement();
				} else if (coins > 300) { // rare
					 return Data.loot.DUNGEON_RARE.randomElement();
				} else if (coins > 150) { // uncommon
					return Data.loot.DUNGEON_UNCOMMON.randomElement();
				} else { // common
					return Data.loot.DUNGEON_COMMON.randomElement();
				}
			};

			this.#treasure.delete(linearIndex);
			this.#freeCells.delete(linearIndex);

			this.#freeCells.set(linearIndex, new Entity('', {ch: null, fg: null, bg: null}));

			// no loot
			if (nothing > 0 && (nothing > State.random() * 100)) {
				Engine.instance.textBuffer.write("You find nothing!");
				return;
			}

			// Hack for Quest(s) {
			if (Quest.isActive(setup.world.pc, "FINDING_YOUR_BALLS_2")
				&& (setup.world.pc.getItemById("quest/rare ore") === undefined)
				&& (State.random() * 100 < this.#depth)) {
				setup.world.pc.addItem(Items.Category.Quest, "rare ore", 1);
				Engine.instance.textBuffer.write("You find: a rare ore!");
				return;
			}

			const coins = Math.ceil(1 + (State.random() * (this.#depth * 5)));
			if (State.random() * 100 > 40) {
				// Money
				setup.world.pc.earnMoney(coins, GameState.IncomeSource.Loot);
				Engine.instance.textBuffer.write(`You find ${coins} coins!`);
				PR.refreshTwineMoney();
			} else {
				const loot = genLoot(coins);
				const count = (Math.max(loot.min, (Math.round(State.random() * loot.max))));
				const item = setup.world.pc.addItem(loot.category, loot.tag, count);
				console.assert(item !== null)
				if (item) {
					Engine.instance.textBuffer.write("You find: " + item.name + "!");
				} else {
					Engine.instance.textBuffer.write("You find: " + loot.tag + "(bug)!");
				}
			}
		}

		/**
		 * Pick a random free cell and plop down the staircase up/out
		 */
		genEntrance() {
			const entrance = [...this.#freeCells.keys()].randomElement();
			this.#entrance = entrance;
			const entranceObject = new Rogue.Entity('stairs_up', {ch: 'O', fg: '#3f3', bg: null});
			entranceObject.type = 'stairs_up';
			this.#freeCells.set(entrance, entranceObject);
		}

		/**
		 * Pick a random free cell minimum distance of 30 spaces away
		 */
		genExit() {
			if (this.#depth >= 100) return;

			if (this.#entrance === undefined) this.genEntrance();
			const entranceXY = numberToXY(this.#entrance);
			const cells = this.cellsOutsideRadius(entranceXY.x, entranceXY.y, 30, true);
			const exit = cells.randomElement();
			this.#exit = clone(exit);
			const exitObject = new Rogue.Entity('stairs_down', ({ch: 'X', fg: '#1ABC9C', bg: null}));
			exitObject.type = 'stairs_down';
			this.#freeCells.set(exit, exitObject);
		}

		getEntrance() {return this.#entrance;}
		get entranceXY() {return numberToXY(this.#entrance);}
		getExit() {return this.#exit}
		get exitXY() {return numberToXY(this.#exit);}

		removeBeing(entity: Entity) {
			if (entity.level === this) {
				const key = entity.xy;
				this.#beings.delete(xyToNumber(key));
				if (Engine.instance.level === this) {
					Engine.instance.draw(key);
				}
			}
		}

		setEntity(entity: Entity, xy: XY) {
			/* FIXME remove from old position, draw */
			if (entity.level === this) {
				const oldXY = entity.xy;
				this.#beings.delete(xyToNumber(oldXY));
				if (Engine.instance.level === this) {Engine.instance.draw(oldXY);}
			}

			entity.setPosition(xy, this); /* propagate position data to the entity itself */

			/* FIXME set new position, draw */
			this.#beings.set(xyToNumber(xy), entity);
			if (Engine.instance.level === this) {
				Engine.instance.draw(xy);
				// Engine.instance.textBuffer.write(`An entity moves to ${xy.x},${xy.y}.`);
			}
		}

		getEntityAt(xy: XY) {
			const linearIndex = xyToNumber(xy);
			return this.#beings.get(linearIndex) ?? this.#walls.get(linearIndex) ?? this.#freeCells.get(linearIndex) ?? this.#borders.get(linearIndex);
		}

		getBeings() {
			/* FIXME list of all beings */
			return this.#beings;
		}
	}
}
