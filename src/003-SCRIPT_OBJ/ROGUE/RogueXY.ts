namespace App.Rogue {
	export class XY {
		x: number;
		y: number;

		constructor(x?: number, y?: number) {
			this.x = x ?? 0;
			this.y = y ?? 0;
		}

		setStr(str: string): void {
			const parts = str.split(",");
			this.x = Number.parseInt(parts[0]);
			this.y = Number.parseInt(parts[1]);
		}

		toString(): string {
			return `${this.x},${this.y}`;
		}

		is(xy: XY): boolean {
			return (this.x === xy.x && this.y === xy.y);
		}

		dist8(xy: XY): number {
			const dx = xy.x - this.x;
			const dy = xy.y - this.y;
			return Math.max(Math.abs(dx), Math.abs(dy));
		}

		dist4(xy: XY): number {
			const dx = xy.x - this.x;
			const dy = xy.y - this.y;
			return Math.abs(dx) + Math.abs(dy);
		}

		dist(xy: XY): number {
			const dx = xy.x - this.x;
			const dy = xy.y - this.y;
			return Math.hypot(dx, dy);
		}

		plus(xy: XY): XY {
			return new XY(this.x + xy.x, this.y + xy.y);
		}

		minus(xy: XY): XY {
			return new XY(this.x - xy.x, this.y - xy.y);
		}
	}

	export function encodeXy(x: number, y: number): number {
		return x + 256 * y;
	}

	export function xyToNumber(xy: XY): number {
		return encodeXy(xy.x, xy.y);
	}

	export function numberToXY(v: number): XY {
		return new XY(v % 256, Math.floor(v / 256));
	}
}
