namespace App {
	export interface Subscriber {
		handleMessage: (message: string, source: unknown, data: unknown) => void;
	}

	const subscribers: Record<string, Subscriber[]> = {};

	export function publish(message: string, publisher: unknown, data: unknown): void {
		const subs: Subscriber[] = subscribers[message] ?? [];
		subs.forEach((subscriber) => {
			subscriber.handleMessage(message, publisher, data);
		});
	}

	export function subscribe(message: string, subscriber: Subscriber): void {
		if (!(message in subscribers)) {
			subscribers[message] = [];
		}
		subscribers[message].push(subscriber);
	}

	export function unsubscribe(message: string, subscriber: Subscriber): void {
		const index = subscribers[message].indexOf(subscriber);
		subscribers[message].splice(index, 1);
	}
}
