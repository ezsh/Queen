namespace App.Actions {
	export class EvaluationContext extends App.EvaluationContext<Base, unknown> {
		readonly #conditions: Conditions.EvaluationContext;
		constructor(conditionsEvaluationContext: Conditions.EvaluationContext) {
			super(conditionsEvaluationContext.definition);
			this.#conditions = conditionsEvaluationContext;
		}

		getBool(action: Base) {
			return this.get(action) ? true : false;
		}

		factor(tag: string): number {
			return this.#conditions.tagged(tag).mod;
		}

		get conditionContext(): Conditions.EvaluationContext {
			return this.#conditions;
		}
	}

	export interface Base {
		preCompute(ctx: EvaluationContext): void;
		validate(ctx: EvaluationContext): boolean;
		apply(ctx: EvaluationContext): void;
		acceptVisitor(visitor: Actionvisitor): void;
	}

	class Choice implements Base {
		readonly #options: Base[];
		constructor(options: Base[]) {
			this.#options = options;
		}

		preCompute(ctx: EvaluationContext): void {
			for (const option of this.#options) option.preCompute(ctx);
			this.setChoice(ctx, 0);
		}

		get options(): Base[] {
			return this.#options;
		}

		setChoice(ctx: EvaluationContext, choice: number): void {
			ctx.store(this, choice, true);
		}

		getChoice(ctx: EvaluationContext): number {
			return ctx.get<number>(this);
		}

		validate(ctx: EvaluationContext): boolean {
			const choice = this.getChoice(ctx);
			return choice >= 0 && choice < this.#options.length;
		}

		apply(ctx: EvaluationContext): void {
			this.#options[this.getChoice(ctx)].apply(ctx);
		}

		acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitChoice(this);
		}
	}

	abstract class Constant implements Base {
		// eslint-disable-next-line @typescript-eslint/no-empty-function, class-methods-use-this
		preCompute(_ctx: EvaluationContext): void {}
		// eslint-disable-next-line @typescript-eslint/no-empty-function, class-methods-use-this
		validate(_ctx: EvaluationContext): boolean {return true;}
		abstract apply(ctx: EvaluationContext): void;
		abstract acceptVisitor(visitor: Actionvisitor): void;
	}

	abstract class ConstantData<TData> extends Constant {
		readonly #data: TData;

		constructor(data: TData) {
			super();
			this.#data = data;
		}

		get data(): TData {
			return this.#data;
		}
	}

	abstract class Numeric<
		TType extends string,
		TData extends Data.Actions.BaseValue<TType, number>
	> implements Base {
		readonly #data: TData;
		constructor(data: TData) {
			this.#data = data;
		}

		get data(): TData {
			return this.#data;
		}

		preCompute(ctx: EvaluationContext): void {
			ctx.store(this, this.numericValue(ctx));
		}

		value(ctx: EvaluationContext) {
			return ctx.get<number>(this);
		}

		// eslint-disable-next-line class-methods-use-this
		validate(): boolean {
			return true;
		}

		abstract apply(ctx: EvaluationContext): void;
		abstract acceptVisitor(visitor: Actionvisitor): void;

		protected numericValue(ctx: EvaluationContext): number {
			let res = this.data.value;
			if (this.data.opt === "random") {
				res = Math.floor((res * State.random()) + 1);
			}
			// value refers to a result of a previous check
			if (this.data.factor) {
				// Retrieve the result of another check and modify the value of this reward by that scaling percentage.
				res *= ctx.factor(this.data.factor);
			}
			return Math.floor(res);
		}
	}

	// type Numeric2DataType<T> = T extends Numeric<infer _, infer TData> ? TData : never;

	abstract class NumericHumanAction<
		TType extends string,
		TName extends string,
		TData extends Data.Actions.Named<TType, TName>
	> extends Numeric<TType, TData> {
		// eslint-disable-next-line class-methods-use-this
		protected human(ctx: EvaluationContext): Entity.Player {
			return ctx.player;
		}
	}

	class Consumable extends NumericHumanAction<Data.Actions.Consumable['type'], Data.Actions.Consumable['name'], Data.Actions.Consumable> {
		override apply(ctx: EvaluationContext): void {
			this.human(ctx).addItem(this.data.type, this.data.name, ctx.get<number>(this));
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitConsumable(this);
		}
	}

	class Clothing extends NumericHumanAction<Data.Actions.Clothing['type'], Data.Actions.Clothing['name'], Data.Actions.Clothing> {
		override apply(ctx: EvaluationContext): void {
			this.human(ctx).addItem(this.data.type, this.data.name, undefined, this.data.wear);
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitClothing(this);
		}
	}

	class Item extends NumericHumanAction<Data.Actions.Item['type'], Data.Actions.Item['name'], Data.Actions.Item> {
		override apply(ctx: EvaluationContext): void {
			const sid = Items.splitId(this.data.name);
			this.human(ctx).addItem(sid.category, sid.tag, ctx.get<number>(this));
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitItem(this);
		}
	}

	interface StoredItemPick {
		name: Data.ItemNameTemplateAny;
		value: number;
	}

	class PickItem implements Base {
		readonly #data: Data.Actions.PickItem;
		constructor(data: Data.Actions.PickItem) {
			this.#data = data;
		}

		preCompute(ctx: EvaluationContext): void {
			const item = Items.pickItem(this.#data.name, this.#data.value);
			console.debug("Selected item", item);
			if (item != null) {
				if (item.cat === Items.Category.Clothes && ctx.player.ownsWardrobeItem(item.tag)) {
					const itemCost = Math.floor(Items.calculateBasePrice(item.cat, item.tag) * 0.3);
					ctx.store(this, itemCost);
				} else {
					const res: StoredItemPick = {
						name: Items.makeId(item.cat, item.tag),
						value: 1,
					}
					ctx.store(this, res);
				}
			}
		}

		// eslint-disable-next-line class-methods-use-this
		validate(_ctx: EvaluationContext): boolean {
			return true;
		}

		apply(ctx: EvaluationContext): void {
			const value = this.value(ctx);
			if (typeof value === 'number') { // item was converted into money
				ctx.player.earnMoney(value, commercialCategory(ctx) ?? GameState.IncomeSource.Unknown);
			} else {
				const n = Items.splitId(value.name);
				if (value.value > 0) {
					const wear = n.category === Items.Category.Clothes ? this.#data.wear : undefined;
					if (Items.isEquipmentCategory(n.category)) {
						ctx.player.addItem(n.category, n.tag, undefined, wear);
					} else {
						ctx.player.addItem(n.category, n.tag, value.value);
					}
				} else {
					ctx.player.takeItem(value.name, 0 - value.value);
				}
			}
		}

		value(ctx: EvaluationContext): StoredItemPick | number {
			return ctx.get<unknown>(this) as StoredItemPick | number;
		}

		acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitPickItem(this)
		}
	}

	class SetClothingLock extends ConstantData<Data.Actions.SetClothingLock> {
		override apply(ctx: EvaluationContext): void {
			ctx.player.clothing.setLock(this.data.slot, this.data.value);
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitSetClothingLock(this);
		}
	}

	type HumanStatActionData = Data.Actions.Body | Data.Actions.Stat | Data.Actions.Skill;
	class HumanStat extends NumericHumanAction<HumanStatActionData['type'], HumanStatActionData['name'], HumanStatActionData> {
		override apply(ctx: EvaluationContext): void {
			switch (this.data.type) {
				case Stat.Core:
					this.human(ctx).adjustCoreStat(this.data.name, ctx.get<number>(this));
					break;
				case Stat.Body:
					this.human(ctx).adjustBody(this.data.name, ctx.get<number>(this));
					break;
				case Stat.Skill:
					this.human(ctx).adjustSkill(this.data.name, ctx.get<number>(this));
					break;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitHumanStat(this);
		}
	}

	type HumanStatXPActionData = Data.Actions.BodyXp | Data.Actions.StatXp | Data.Actions.SkillXp;
	class HumanStatXP extends NumericHumanAction<HumanStatXPActionData['type'], HumanStatXPActionData['name'], HumanStatXPActionData> {
		override apply(ctx: EvaluationContext): void {
			if (this.data.name === CoreStat.Willpower) {
				this.human(ctx).corruptWillPower(ctx.get<number>(this), 75); // default scaling for jobs.
			} else {
				const nameTypeMap = {
					statXp: Stat.Core,
					bodyXp: Stat.Body,
					skillXp: Stat.Skill,
				};
				this.human(ctx).adjustXP(nameTypeMap[this.data.type], this.data.name, ctx.get<number>(this));
			}
		}

		protected override numericValue(ctx: EvaluationContext): number {
			return this.data.name === Skills.Charisma.Courtesan && ctx.player.stat(Stat.Skill, Skills.Charisma.Courtesan) <= 0
				? 0
				: super.numericValue(ctx);
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitHumanStatXP(this);
		}
	}

	class CorruptWillpower extends Numeric<Data.Actions.CorruptWillpower['type'], Data.Actions.CorruptWillpower> {
		override apply(ctx: EvaluationContext): void {
			ctx.player.corruptWillPower(ctx.get<number>(this), this._difficulty());
		}

		private _difficulty(): number {
			switch (this.data.name) {
				case 'low': return 20;
				case 'medium': return 50;
				case 'high': return 75;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitCorruptWillpower(this);
		}
	}

	class BodyEffect extends ConstantData<Data.Actions.BodyEffect> {
		override apply(ctx: EvaluationContext): void {
			if (this.data.value) {
				ctx.player.addBodyEffect(this.data.name);
			} else {
				ctx.player.removeBodyEffect(this.data.name);
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitBodyEffect(this);
		}
	}

	class Effect extends ConstantData<Data.Actions.Effect> {
		override apply(ctx: EvaluationContext): void {
			Data.effectLibrary[this.data.name].fun(ctx.player);
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitEffect(this);
		}
	}

	class SlotUnlock extends Constant {
		// eslint-disable-next-line class-methods-use-this
		apply(ctx: EvaluationContext): void {
			ctx.player.unlockSlot();
		}

		acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitSlotUnlock(this);
		}
	}

	class NpcStat extends Numeric<Data.Actions.NpcStat['type'], Data.Actions.NpcStat> {
		override apply(ctx: EvaluationContext): void {
			const value = this.value(ctx);
			for (const npc of ctx.npc(this.data.npc)) {
				npc.adjustCoreStat(this.data.name, value);
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitNpcStat(this);
		}
	}

	class Variable extends ConstantData<Data.Actions.Variable> {
		override preCompute(ctx: EvaluationContext): void {
			super.preCompute(ctx);
			if (this.data.immediate) { // HACK
				this._applyImpl(ctx);
			}
		}

		override apply(ctx: EvaluationContext): void {
			if (!this.data.immediate) {
				this._applyImpl(ctx);
			}
		}

		private _applyImpl(ctx: EvaluationContext): void {
			const op = this.data.op ?? "set";
			switch (op) {
				case "set":
					this._variableMap(ctx)[this.data.name] = this.data.value;
					break;
				case "add": {
					const currentValue = ctx.gameVariableValue(this.data.name, ctx.definedVariableScope(this.data.scope));
					const add = this.data.value;
					if ((typeof currentValue !== "number") || (typeof add !== "number")) {
						throw new TypeError(`Could not add ${this.data.value} to ${currentValue}`);
					}
					this._variableMap(ctx)[this.data.name] = currentValue + add;
				}
					break;
				case "reset":
					delete this._variableMap(ctx)[this.data.name];
					break;
			}
		}

		private _variableMap(ctx: EvaluationContext): GameState.GameVariableMap {
			const scope = ctx.definedVariableScope(this.data.scope);
			switch (scope.type) {
				case "player":
					return ctx.player.flags;
				case "job":
					return App.Job.byId(scope.id).state(ctx.player, true);
				case "quest":
					return App.Quest.byId(scope.id).state(ctx.player, true);
			}
		}

		acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitVariable(this);
		}
	}

	class Quest extends ConstantData<Data.Actions.Quest> {
		override apply(ctx: EvaluationContext): void {
			switch (this.data.value) {
				case 'start':
					ctx.quest(this.data.name).accept(ctx.world);
					break;
				case 'cancel':
					ctx.quest(this.data.name).cancel(ctx.player);
					break;
				case 'complete':
					// TODO test for canComplete()?
					ctx.quest(this.data.name).complete(ctx.world);
					break;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitQuest(this);
		}
	}

	function commercialCategory(ctx: EvaluationContext): GameState.CommercialActivity | null {
		if (ctx.isTaskDefined) {
			return ctx.task instanceof App.Quest ? GameState.CommercialActivity.Quests : GameState.CommercialActivity.Jobs;
		}
		return null;
	}

	class Money extends Numeric<Data.Actions.Money['type'], Data.Actions.Money> {
		override apply(ctx: EvaluationContext): void {
			const value = this.value(ctx);
			if (value >= 0) {
				ctx.player.earnMoney(value, commercialCategory(ctx) ?? GameState.IncomeSource.Unknown);
			} else {
				ctx.player.spendMoney(-value, commercialCategory(ctx) ?? GameState.SpendingTarget.Unknown);
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitMoney(this);
		}
	}

	class Tokens extends Numeric<Data.Actions.Tokens['type'], Data.Actions.Tokens> {
		override apply(ctx: EvaluationContext): void {
			ctx.player.adjustTokens(ctx.get<number>(this));
		}

		protected override numericValue(ctx: EvaluationContext): number {
			return ctx.player.stat(Stat.Skill, Skills.Charisma.Courtesan) > 0 ? super.numericValue(ctx) : 0;
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitTokens(this);
		}
	}

	function defaultStoreName(ctx: EvaluationContext): string {
		const npc = ctx.npc().first();
		if (!npc) {
			throw new Error("Default NPC is not defined at this evaluation context");
		}
		const name = npc.storeName;
		if (!name) {
			throw new Error("Default NPC for this evaluation context has not store");
		}
		return name;
	}

	class Store extends ConstantData<Data.Actions.Store> {
		override apply(ctx: EvaluationContext): void {
			/* const npc = ctx.npc(this.data.name)[0];
			if (ctx.world.storeInventory[this.data.name].inventory.length < 1) {
				storeEngine.openStore(ctx.player, npc);
			} */

			switch (this.data.opt) {
				case "lock":
					storeEngine.toggleStoreItem(this.data.name ?? defaultStoreName(ctx), this.data.value, 1);
					break;
				case "unlock":
					storeEngine.toggleStoreItem(this.data.name ?? defaultStoreName(ctx), this.data.value, 0);
					break;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitStore(this);
		}
	}

	class ResetStore extends ConstantData<Data.Actions.ResetStore> {
		override apply(ctx: EvaluationContext): void {
			const storeName = this.data.name ?? defaultStoreName(ctx);
			if (ctx.world.storeInventory[storeName].inventory.length > 1) {
				ctx.world.storeInventory[storeName].lastStocked = 0;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitResetStore(this);
		}
	}

	class SailDays extends Numeric<Data.Actions.SailDays['type'], Data.Actions.SailDays> {
		override apply(ctx: EvaluationContext): void {
			ctx.player.advanceSailDays(ctx.get<number>(this));
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitSailDays(this);
		}
	}

	class TrackCustomers extends ConstantData<Data.Actions.TrackCustomers> {
		override apply(ctx: EvaluationContext): void {
			// Let's set a tag in the player to start tracking their history
			ctx.setGameVariableValue(trackCustomersVariableName(this.data.name), ctx.player.getHistory("customers", this.data.name),
				ctx.definedVariableScope(this.data.scope));
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitTrackCustomers(this);
		}
	}

	class TrackProgress extends Numeric<Data.Actions.TrackProgress['type'], Data.Actions.TrackProgress> {
		override apply(ctx: EvaluationContext): void {
			ctx.setGameVariableValue(trackProgressVariableName(this.data.name), this._value(ctx), ctx.definedVariableScope(this.data.scope));
		}

		private _value(ctx: EvaluationContext): number {
			switch (this.data.op) {
				case 'add':
				case undefined:
					return ctx.gameVariableValue<number>(trackProgressVariableName(this.data.name), this.data.scope)
						+ super.value(ctx);
				case 'multiply':
					return ctx.gameVariableValue<number>(trackProgressVariableName(this.data.name), this.data.scope) * super.value(ctx);
				case 'set':
					return super.value(ctx);
				case 'reset':
					return 0;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitTrackProgress(this);
		}
	}

	class SaveDate extends ConstantData<Data.Actions.SaveDate> {
		override apply(ctx: EvaluationContext): void {
			ctx.task.setVariable(ctx.player, this.data.name, ctx.world.day);
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitSaveDate(this);
		}
	}

	class WorldState extends Numeric<Data.Actions.WorldState['type'], Data.Actions.WorldState> {
		override apply(ctx: EvaluationContext): void {
			switch (this.data.name) {
				case 'dayPhase':
					ctx.world.advancePhaseTo(ctx.get<number>(this));
					break;
			}
		}

		override acceptVisitor(visitor: Actionvisitor): void {
			visitor.visitWorldState(this);
		}
	}

	/**
	 * This is an action which silently performs and is not displayed in the game UI.
	 * It is also not handled by visitors because it provides no useful data for them;
	 */
	abstract class Silent implements Base {
		/* eslint-disable class-methods-use-this, @typescript-eslint/no-empty-function */
		preCompute(_ctx: EvaluationContext): void {}
		validate(_ctx: EvaluationContext): boolean {
			return true;
		}
		abstract apply(ctx: EvaluationContext): void;

		acceptVisitor(_visitor: Actionvisitor): void {}
		/* eslint-enable */
	}

	export class SilentFunction extends Silent {
		readonly #action: (ctx: EvaluationContext) => void;
		constructor(action: (ctx: EvaluationContext) => void) {
			super();
			this.#action = action;
		}

		override apply(ctx: EvaluationContext): void {
			this.#action(ctx);
		}
	}

	function makeAction(data: Data.Actions.SingleAny): Base {
		// TODO consider making action classes completely stateless, then we would not need to create action instances
		switch (data.type) {
			case Items.Category.Food:
			case Items.Category.Drugs:
			case Items.Category.Cosmetics:
			case Items.Category.LootBox:
				return new Consumable(data);
			case Items.Category.Clothes:
			case Items.Category.Weapon:
				return new Clothing(data);
			case 'item':
				return new Item(data);
			case 'pickItem':
				return new PickItem(data);
			case 'slot':
				return new SlotUnlock();
			case 'setClothingLock':
				return new SetClothingLock(data);
			case 'bodyEffect':
				return new BodyEffect(data);
			case 'effect':
				return new Effect(data);
			case Stat.Body:
			case Stat.Core:
			case Stat.Skill:
				return new HumanStat(data);
			case 'bodyXp':
			case 'statXp':
			case 'skillXp':
				return new HumanStatXP(data);
			case 'npcStat':
				return new NpcStat(data);
			case 'var':
				return new Variable(data);
			case 'quest':
				return new Quest(data);
			case 'money':
				return new Money(data);
			case 'tokens':
				return new Tokens(data);
			case 'store':
				return new Store(data);
			case 'resetShop':
				return new ResetStore(data);
			case 'corruptWillpower':
				return new CorruptWillpower(data);
			case 'sailDays':
				return new SailDays(data);
			case 'trackCustomers':
				return new TrackCustomers(data);
			case 'trackProgress':
				return new TrackProgress(data);
			case 'saveDate':
				return new SaveDate(data);
			case 'worldState':
				return new WorldState(data);
		}
		// assertUnreachable(data.type);
	}

	export class CostSilentAction extends Silent {
		readonly #data: Data.Tasks.Costs.Any;
		constructor(data: Data.Tasks.Costs.Any) {
			super();
			this.#data = data;
		}

		override apply(ctx: EvaluationContext): void {
			switch (this.#data.type) {
				case Stat.Core:
					ctx.player.adjustCoreStat(this.#data.name, Math.floor(this.#data.value * -1.0));
					break;
				case Stat.Skill:
					ctx.player.adjustSkill(this.#data.name, Math.floor(this.#data.value * -1.0));
					break;
				case Stat.Body:
					ctx.player.adjustBody(this.#data.name, Math.floor(this.#data.value * -1.0));
					break;
				case "money":
					ctx.player.spendMoney(Math.floor(this.#data.value), GameState.CommercialActivity.Jobs);
					break;
				case "tokens":
					ctx.player.adjustTokens(Math.floor(this.#data.value * -1.0));
					break;
				case "item":
					if (Items.isEquipment(this.#data.name)) {
						// TODO
						// player.clothing.removeItem(c.name);
					} else {
						ctx.player.inventory.removeItem(this.#data.name);
					}
					break;
				case "time":
					setup.world.nextPhase(this.#data.value);
					break;
			}
		}
	}

	function isChoice(action: Data.Actions.Any): action is Data.Actions.Choice {
		return action.hasOwnProperty('choice');
	}

	export function parseActions(actions: ReadonlyArray<Data.Actions.Any>): Base[] {
		const res: Base[] = [];
		for (const a of actions) {
			if (isChoice(a)) {
				res.push(new Choice(parseActions(a.choice)));
			} else {
				res.push(makeAction(a))
			}
		}
		return res;
	}

	interface Actionvisitor {
		visitChoice(action: Choice): void;
		visitConsumable(action: Consumable): void;
		visitClothing(action: Clothing): void;
		visitItem(action: Item): void;
		visitPickItem(action: PickItem): void;
		visitBodyEffect(action: BodyEffect): void;
		visitEffect(action: Effect): void;
		visitSetClothingLock(action: SetClothingLock): void;
		visitSlotUnlock(action: SlotUnlock): void;
		visitHumanStat(action: HumanStat): void;
		visitHumanStatXP(action: HumanStatXP): void;
		visitNpcStat(action: NpcStat): void;
		visitVariable(action: Variable): void;
		visitQuest(action: Quest): void;
		visitMoney(action: Money): void;
		visitTokens(action: Tokens): void;
		visitStore(action: Store): void;
		visitResetStore(action: ResetStore): void;
		visitCorruptWillpower(action: CorruptWillpower): void;
		visitSailDays(action: SailDays): void;
		visitTrackCustomers(action: TrackCustomers): void;
		visitTrackProgress(action: TrackProgress): void;
		visitSaveDate(action: SaveDate): void;
		visitWorldState(action: WorldState): void;
	}

	class ActionVisitorDummy implements Actionvisitor {
		/* eslint-disable class-methods-use-this, @typescript-eslint/no-empty-function */
		visitChoice(_action: Choice): void {}
		visitConsumable(_action: Consumable): void {}
		visitClothing(_action: Clothing): void {}
		visitItem(_action: Item): void {}
		visitPickItem(_action: PickItem): void {}
		visitBodyEffect(_action: BodyEffect): void {}
		visitEffect(_action: Effect): void {}
		visitSetClothingLock(_action: SetClothingLock): void {}
		visitSlotUnlock(_action: SlotUnlock): void {}
		visitHumanStat(_action: HumanStat): void {}
		visitHumanStatXP(_action: HumanStatXP): void {}
		visitNpcStat(_action: NpcStat): void {}
		visitVariable(_action: Variable): void {}
		visitQuest(_action: Quest): void {}
		visitMoney(_action: Money): void {}
		visitTokens(_action: Tokens): void {}
		visitStore(_action: Store): void {}
		visitResetStore(_action: ResetStore): void {}
		visitCorruptWillpower(_action: CorruptWillpower): void {}
		visitSailDays(_action: SailDays): void {}
		visitTrackCustomers(_action: TrackCustomers): void {}
		visitTrackProgress(_action: TrackProgress): void {}
		visitSaveDate(_action: SaveDate): void {}
		visitWorldState(_action: WorldState): void {}
		/* eslint-enable */
	}

	function itemLabel(name: Data.ItemNameTemplateAny, count: number) {
		const desc = Items.factoryById(name).description;
		return count === 1 ? desc : `${desc} x ${count}`;
	}

	function itemLabelAction(ctx: EvaluationContext, action: Item) {
		return itemLabel(action.data.name, action.value(ctx))
	}

	function nakedItemLabelAction(ctx: EvaluationContext, action: Consumable | Clothing) {
		return itemLabel(Items.makeId(action.data.type, action.data.name), action.value(ctx))
	}

	class ChoiceRenderer extends ActionVisitorDummy {
		readonly #ctx: EvaluationContext;
		readonly #parent: ParentNode;
		readonly #name: string;
		readonly#choice: Choice;
		#index = 0;
		constructor(ctx: EvaluationContext, parent: ParentNode, name: string, choiceGroup: Choice) {
			super();
			this.#ctx = ctx;
			this.#parent = parent;
			this.#name = name;
			this.#choice = choiceGroup;
		}

		override visitItem(action: Item): void {
			this._appendDivChoice(itemLabelAction(this.#ctx, action));
		}

		override visitConsumable(action: Consumable): void {
			this._appendDivChoice(nakedItemLabelAction(this.#ctx, action));
		}

		override visitClothing(action: Clothing): void {
			this._appendDivChoice(nakedItemLabelAction(this.#ctx, action));
		}

		override visitSlotUnlock(_action: SlotUnlock): void {
			this._appendDivChoice("<span style='color:cyan'>A slot reel unlock!</span>");
		}

		private _appendDivChoice(labelText: string) {
			const div = UI.appendNewElement('div', this.#parent);
			const inputId = `${this.#name}-${this.#index}`;
			const input = UI.appendNewElement('input', div);
			input.type = 'radio';
			input.id = inputId;
			input.name = this.#name;
			input.value = `${this.#index}`;
			if (this.#index === 0) {
				input.checked = true;
			}
			input.addEventListener('change', this._onChange.bind(this));

			this.#index++;
			const label = UI.appendNewElement('label', div);
			UI.appendFormattedText(label, labelText); //
			label.htmlFor = inputId;
		}

		private _onChange(event: Event) {
			const choice = Number.parseInt((event.target as HTMLInputElement).value);
			this.#choice.setChoice(this.#ctx, choice);
		}
	}

	export class TaskRewardRenderer extends ActionVisitorDummy {
		readonly #ctx: EvaluationContext;
		readonly #parents: ParentNode[] = [];
		#choiceIndex = 0;

		#coins = 0;
		#tokens = 0;
		#slotUnlockCount = 0;
		#itemsGiven = 0;

		constructor(ctx: EvaluationContext, actions: Base[], parent: ParentNode, job?: Job) {
			super();
			this.#ctx = ctx;
			this.#parents.push(parent);

			if (job) { // process static payments
				this.#coins += job.pay;
				this.#tokens += job.tokens;
			}

			for (const a of actions) {
				a.acceptVisitor(this);
			}

			if (this.#coins !== 0) {
				this._appendTextItem(`<span class='item-money'>${this.#coins} coins</span>`)
			}

			if (this.#tokens > 0) {
				this._appendTextItem(`<span class='item-courtesan-token'>${this.#tokens} courtesan tokens</span>`);
			}

			for (let i = 0; i < this.#slotUnlockCount; ++i) {
				this._appendTextItem("<span style='color:cyan'>A slot reel unlock!</span>");
			}
		}

		get isEmpty(): boolean {
			return this.#coins === 0 &&
				this.#tokens === 0 &&
				this.#slotUnlockCount === 0 &&
				this.#choiceIndex === 0 &&
				this.#itemsGiven === 0;
		}

		private get _currentParent() {
			return this.#parents.last();
		}

		override visitChoice(action: Choice): void {
			const fieldset = UI.appendNewElement('fieldset', this._currentParent);
			UI.appendNewElement('legend', fieldset, {content: "Choose your reward:"});

			const choiceRenderer = new ChoiceRenderer(this.#ctx, fieldset, `choice-${this.#choiceIndex}`, action);
			for (const o of action.options) {
				o.acceptVisitor(choiceRenderer);
			}
			this.#choiceIndex++;
		}

		override visitMoney(action: Money): void {
			this.#coins += this.#ctx.get<number>(action);
		}

		override visitTokens(action: Tokens): void {
			this.#tokens += this.#ctx.get<number>(action);
		}

		override visitItem(action: Item): void {
			this._appendTextItem(itemLabelAction(this.#ctx, action));
			this.#itemsGiven++;
		}

		override visitConsumable(action: Consumable): void {
			this._appendTextItem(nakedItemLabelAction(this.#ctx, action));
			this.#itemsGiven++;
		}

		override visitClothing(action: Clothing): void {
			this._appendTextItem(nakedItemLabelAction(this.#ctx, action));
			this.#itemsGiven++;
		}

		override visitPickItem(action: PickItem): void {
			const value = action.value(this.#ctx);
			if (typeof value === 'number') {
				this.#coins += value;
			} else {
				this._appendTextItem(itemLabel(value.name, value.value));
				this.#itemsGiven++;
			}
		}

		override visitSlotUnlock(_action: SlotUnlock): void {
			this.#slotUnlockCount += 1;
		}

		private _appendTextItem(text: string) {
			const div = UI.appendNewElement('div', this._currentParent);
			UI.appendFormattedText(div, text);
		}
	}

	export class PayCalculatorVisitor extends ActionVisitorDummy {
		#money = 0;
		#courtesanTokens = 0;
		readonly #ctx: Actions.EvaluationContext;

		constructor(ctx: Actions.EvaluationContext) {
			super();
			this.#ctx = ctx;
		}

		get money() {
			return this.#money;
		}

		get courtesanTokens() {
			return this.#courtesanTokens;
		}

		override visitMoney(action: Money): void {
			this.#money += action.value(this.#ctx);
		}

		override visitTokens(action: Tokens): void {
			this.#courtesanTokens += action.value(this.#ctx);
		}
	}
}
