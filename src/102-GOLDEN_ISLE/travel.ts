/* eslint-disable @typescript-eslint/naming-convention */
namespace App.Data {
	const enterTheMansion = "Enter the Mansion";
	registerPassageDisplayNames({
		Bazaar: "The Bazaar",
		GI_Apothecary: "Apothecary",
		GI_BackStage: "Backstage",
		GI_CheapBrothel: "The Saucy Slattern",
		GI_Clothiers: "Bella's Boudoir",
		GI_DanceHall: "Minxy's Dance Hall",
		GI_GeneralGoods: "Golden Goods",
		GI_GovernorsMansion: "The Governor's Mansion",
		GI_GovernorsMansionInside: enterTheMansion,
		GI_GuardStation: "Guard Station",
		GI_Jail: "Jail House",
		GI_Salon: "Madame Yvonne's Salon",
		GoldenIsle: "The Docks",
		GoldenIsleFightClub: "Exhibition Yard",
		HighHill: "High Hill District",
		Portside: "Portside District",
		SmugglersDen: "Smuggler's Den",
	});

	registerTravelDestination({
		GoldenIsle: [
			{
				caption: "Back to The Mermaid",
				destination: "Deck",
			},
			"SmugglersDen", "Portside",
		],
		SmugglersDen: ["GoldenIsle"],
		Portside: ["GoldenIsle", "GI_GeneralGoods", "GI_CheapBrothel", "Bazaar", "GI_GuardStation"],
		Bazaar: ["Portside", "GI_Apothecary", "GoldenIsleFightClub"],
		GI_GeneralGoods: ["Portside"],
		GI_CheapBrothel: ["Portside"],
		GI_GuardStation: [
			"Portside", "GI_Jail",
			{
				available: [{type: 'quest', name: "JUSTUS_WHORING", property: "status", value: App.QuestStatus.Completed, condition: '!='}],
				caption: "High Hill District",
				scene: "highHillDistrictGuarded",
			},
			{
				available: [{type: 'quest', name: "JUSTUS_WHORING", property: "status", value: App.QuestStatus.Completed}],
				destination: "HighHill",
			},
		],
		GI_Jail: ["GI_GuardStation"],
		GI_Apothecary: ["Bazaar"],
		GoldenIsleFightClub: ["Bazaar"],
		HighHill: ["GI_GuardStation", "GI_Salon", "GI_Clothiers", "GI_DanceHall", "GI_GovernorsMansion"],
		GI_Salon: ["HighHill"],
		GI_Clothiers: ["HighHill"],
		GI_DanceHall: ["HighHill", "GI_BackStage"],
		GI_StudyCrowd: ["GI_BackStage"],
		GI_GovernorsMansion: [
			"HighHill",
			{
				available: [{type: 'quest', name: "ROYAL_ENTRANCE", property: "status", value: App.QuestStatus.Completed, condition: '!='}],
				caption: enterTheMansion,
				scene: "goldenIsleGovernorMansionGuarded",
			},
			{
				available: [
					{type: 'quest', name: "BERTIE_QUEEN_PT1", property: "status", value: App.QuestStatus.Completed, condition: "!="},
					{type: 'quest', name: "ROYAL_ENTRANCE", property: "status", value: App.QuestStatus.Completed},
					{type: 'job', name: "UTICUS_ANAL", property: 'lastCompletedFor', value: 0, condition: '>'},
					{type: 'npcStat', option: "Uticus", name: App.NpcStat.Mood, value: 70, condition: '<'},
				],
				caption: enterTheMansion,
				scene: "goldenIsleGovernorMansionJobNotDone",
			},
			{
				available: [
					{type: 'quest', name: "BERTIE_QUEEN_PT1", property: "status", value: App.QuestStatus.Completed, condition: "!="},
					{type: 'quest', name: "ROYAL_ENTRANCE", property: "status", value: App.QuestStatus.Completed},
					{type: 'job', name: "UTICUS_ANAL", property: 'lastCompletedFor', value: 0, condition: '=='},
					{type: 'npcStat', option: "Uticus", name: App.NpcStat.Mood, value: 70, condition: '<'},
				],
				caption: enterTheMansion,
				scene: "goldenIsleGovernorMansionUticusUnsatisfied",
			},
			{
				available: {
					op: "||",
					args: [
						{type: 'quest', name: "BERTIE_QUEEN_PT1", property: "status", value: App.QuestStatus.Completed, condition: "=="},
						{
							args: [
								{type: 'quest', name: "ROYAL_ENTRANCE", property: "status", value: App.QuestStatus.Completed},
								{type: 'job', name: "UTICUS_ANAL", property: 'lastCompletedFor', value: 0, condition: '=='},
								{type: 'npcStat', option: "Uticus", name: App.NpcStat.Mood, value: 70, condition: '>='},
							],
						},
					],
				},
				destination: "GI_GovernorsMansionInside",
				caption: enterTheMansion,
			},
		],
		GI_GovernorsMansionInside: [
			{
				caption: "Exit",
				destination: "GI_GovernorsMansion",
			},
		],
	});
}
/* eslint-enable @typescript-eslint/naming-convention */
