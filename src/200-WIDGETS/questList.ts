namespace App.UI {
	function questDialog(quest: App.Quest, npcId: string): Node {
		const res = new DocumentFragment();
		const ctx = quest.makeActionContext(setup.world);
		const qStatus = quest.status(ctx.world.pc, npcId);
		const scenes = quest.playScenes(ctx);
		for (const s of scenes) {
			res.append(s.text);
			App.UI.appendNewElement('br', res);
		}

		const applySceneActions = () => {
			for (const s of scenes) {
				for (const a of s.actions) {
					a.apply(ctx);
				}
			}
		}

		const buttonDiv = makeElement('div', {classNames: 'task-button-div'});

		const buttonCssClasses = ['cmd-button', 'task-button'];

		if (qStatus === App.QuestStatus.Available) {
			const acceptButton = App.UI.appendNewElement('button', buttonDiv, {content: "Accept", classNames: buttonCssClasses});
			acceptButton.addEventListener('click', () => {
				applySceneActions();
				App.UI.replace("#QuestUI", questList(npcId));
			});
			res.append('  ');
			const declineButton = App.UI.appendNewElement('button', buttonDiv, {content: "Decline", classNames: buttonCssClasses});
			declineButton.addEventListener('click', () => {
				App.UI.replace("#QuestUI", questList(npcId));
			});
		} else if (qStatus === App.QuestStatus.Active) {
			const okayButton = App.UI.appendNewElement('button', buttonDiv, {content: "Okay, fine", classNames: buttonCssClasses});
			okayButton.addEventListener('click', () => {
				App.UI.replace("#QuestUI", questList(npcId));
			});
		} else { // "cancomplete"
			const allActions: App.Actions.Base[] = [];
			for (const s of scenes) {
				for (const a of s.actions) {
					allActions.push(a);
				}
			}
			if (allActions.length > 0) {
				const rewardsDiv = App.UI.makeElement('div');
				rewardsDiv.style.margin = '1ex';
				App.UI.appendNewElement('span', rewardsDiv, {content: "Quest Rewards", classNames: 'task-cancomplete'});
				const rewardsRenderer = new App.Actions.TaskRewardRenderer(ctx, allActions, rewardsDiv);
				if (!rewardsRenderer.isEmpty) {
					res.append(rewardsDiv);
				}
			}
			const completeButton = App.UI.appendNewElement('button', buttonDiv, {content: "Complete Quest", classNames: 'cmd-button'});
			completeButton.addEventListener('click', () => {
				applySceneActions();
				if (App.Quest.byId("GAME_WON").isCompleted(setup.world.pc)) {
					Engine.play("GameWon");
				}
				Engine.play(variables().gameBookmark);
			});
		}

		res.append(buttonDiv);

		return res
	}

	function questList(npcId: string): Node {
		const res = new DocumentFragment();
		const npc = setup.world.npc(npcId);
		const ql = App.Quest.list("any", setup.world.pc, npcId);
		$(res).wiki(`You approach ${npc.pName} to ask if there are any 'special tasks' that need to be done.`);
		for (const [q, qState] of ql) {
			const qDiv = App.UI.appendNewElement('div', res);
			if (qState === App.QuestStatus.Completed) {
				App.UI.appendNewElement('span', qDiv, {content: q.title(), classNames: 'state-disabled'});
				App.UI.appendNewElement('span', qDiv, {content: " (COMPLETED)", classNames: 'state-neutral'});
			} else {
				const questLink = App.UI.appendNewElement('a', qDiv, {content: q.title()});
				questLink.addEventListener('click', () => {
					App.UI.replace("#QuestUI", questDialog(q, npcId));
				});
				if (qState === App.QuestStatus.Active) {
					App.UI.appendNewElement('span', qDiv, {content: " (IN PROGRESS)", classNames: 'item-time'});
				} else if (qState === App.QuestStatus.CanComplete && npcId === q.receiverId) {
					App.UI.appendNewElement('span', qDiv, {content: " (IN PROGRESS)", classNames: 'task-cancomplete'});
				}
			}
		}
		$(res).wiki(App.UI.pInteractLinkStrip());
		return res;
	}

	Macro.add("questList", {
		handler() {
			this.output.append(questList(variables().menuAction as string));
		},
	});
}
