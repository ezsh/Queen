Macro.add("DrawAvatar", {
	skipArgs: true,
	handler() {
		const elementId = 'avatarUI';
		App.UI.appendNewElement('div', this.output).id = elementId;
		setup.avatar.drawCanvas(elementId, 800, 360);
	},
});
