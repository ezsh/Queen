namespace App.UI.Widgets {
	function appendDiv(parent: ParentNode, id: string, classList?: string[]): HTMLDivElement {
		return appendNewElement('div', parent, {id: id, classNames: classList});
	}

	function appendBetDiv(parent: ParentNode, id: string, label: string): void {
		const res = appendNewElement('div', parent, {id: `CoffinBet${id}`});
		res.appendNewElement('div', {content: `${label} Bet`, id: `betLabel${id}`});
		res.appendNewElement('div', {content: "Him", id: `betLabel${id}b`});
		res.appendNewElement('div', {id: `betIcon${id}a`});
		res.appendNewElement('div', {content: "You", id: `betLabel${id}c`});
		res.appendNewElement('div', {id: `betIcon${id}b`});
		appendNewElement('div', parent, {id: `CoffinBet${id}Blocker`});
	}

	function appendCofBox(parent: ParentNode, id: string, label: string): HTMLDivElement {
		const res = appendNewElement('div', parent, {id: `cofBoxWrapper${id}`});
		for (let i = 0; i < 8; ++i) {
			appendNewElement('div', res, {id: `${label}box${i}`, classNames: 'coffinNumBox', content: (i + 2).toString()});
		}
		return res;
	}

	function appendBetButtons(parent: ParentNode, groupId: string, buttons: [string, string][]) {
		const group = App.UI.appendNewElement('div', parent, {classNames: "cofUIButtons", id: groupId});
		for (const d of buttons) {
			group.appendNewElement('button', {content: d[1], classNames: "cofButton", id: d[0]});
		}
	}

	Macro.add("coffinUI", {
		handler() {
			const div = appendDiv(this.output, 'GamblingGUI', ['GamblingGUI']);

			const left = appendDiv(div, "CoffinContainerLeft", ['CoffinContainerLeft']);
			const dice = appendDiv(left, "CoffinDiceContainer");
			appendNewElement('div', dice, {id: "RoundContainer"});
			appendNewElement('div', dice, {id: "GamesPlayedContainer"});
			const bet = appendNewElement('div', left, {id: "CoffinBetContainer"});

			const pirateSel = appendDiv(bet, "coffinPirateSelect");
			const buttonCmdPreviousBet = pirateSel.appendNewElement('button', {classNames: "coffinRoundButton", id: "cmdPrevBet"});
			buttonCmdPreviousBet.innerHTML = '&#8249';
			pirateSel.appendNewElement('span').id = "CoffinNPCName";
			const buttonCmdNextBet = pirateSel.appendNewElement('button', {classNames: "coffinRoundButton", id: "cmdNextBet"});
			buttonCmdNextBet.innerHTML = '&#8250';

			appendBetDiv(bet, "1", "Top");
			appendBetDiv(bet, "2", "Bottom");

			const right = appendDiv(div, "CoffinContainerRight", ['CoffinContainerRight']);
			const coffinImg = right.appendNewElement('img', {id: "coffinBoard"});
			coffinImg.dataset.passage = "img_coffin";
			coffinImg.src = Story.get('img_coffin').text;
			appendCofBox(div, "1", "pc");
			appendCofBox(div, "2", "npc");

			appendDiv(div, "cofPlayerName").textContent = "You";
			appendDiv(div, "cofNPCName").textContent = "Opponent";

			const tray = appendDiv(div, "cofUItray");
			tray.appendFormattedText(
				"Welcome @@color:cyan;Shut the Coffin@@! The game is easy to play - roll the dice and if you hit an empty number, \
				mark it and roll again. The first player to mark five nails is the winner! If the board is filled until the \
				very last spot, then you enter <span style='color:orange'>SUDDEN DEATH</span> and the first person to hit a double \
				on the dice will win!\n\
				\n\
				You can play even if you don't have money by literally betting your ass. Just be careful about losing too much or \
				you might be a vegetable after your opponents are done with you!"
			);

			appendBetButtons(div, "startButtons", [["cmdStart", "START GAME"], ["cmdQuit", "WALK AWAY"]]);
			appendBetButtons(div, "betButtons", [["cmdTakeBet", "TAKE BET"], ["cmdQuitBet", "WALK AWAY"]]);
			appendBetButtons(div, "playButtons", [["cmdRollDice", "ROLL DICE"]]);
			appendBetButtons(div, "endButtons", [["cmdPlayAgain", "PLAY AGAIN"], ["cmdQuitPlay", "WALK AWAY"]]);
		},
	});
}
