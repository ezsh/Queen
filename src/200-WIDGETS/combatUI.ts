namespace App.UI.Widgets {
	function appendDiv(parent: ParentNode, id: string, classList?: string[]) {
		return App.UI.appendNewElement('div', parent, {id: id, classNames: classList});
	}

	const appendNewStatCell = (parent: HTMLDivElement, label: string, classList?: string[]) => {
		const cell = parent.appendNewElement('div', {classNames: 'combat-stat-cell'});
		cell.appendNewElement('div', {content: label + ':', classNames: 'combat-stat-name'});
		return cell.appendNewElement('div', {classNames: classList});
	};

	Macro.add("combatUI", {
		handler() {
			setup.combat.drawUI();
			const combatUI = appendDiv(this.output, "CombatGUI", ['CombatGUI']);
			const combatPanel = appendDiv(combatUI, "combat-panel")
			appendDiv(combatPanel, "EnemyGUI", ["EnemyGUI"]);
			appendDiv(combatUI, "InitiativeBar");
			const combatStatsContainer = appendDiv(combatPanel, "PlayerCombatStatContainer", ["combat-stats-container"]);
			appendNewStatCell(combatStatsContainer, "Combat Style").appendNewElement('select').id = "combatStyles";
			appendNewStatCell(combatStatsContainer, "Stamina", ['combat-stat-meter'])
				.appendNewElement('div').id = "PlayerStaminaBar";
			appendNewStatCell(combatStatsContainer, "Combo", ['combat-stat-meter'])
				.appendNewElement('div').id = "PlayerComboBar";

			const cmdDiv = combatPanel.appendNewElement('div');
			cmdDiv.style.display = 'flex';
			cmdDiv.style.marginLeft = '5px';
			const commands = cmdDiv.appendNewElement('div');
			commands.appendNewElement('button', {content: "Restore Stamina", classNames: 'combatButton', id: "cmdRestoreStamina"});
			commands.appendNewElement('br');
			commands.appendNewElement('button', {content: "Defend", classNames: 'combatButton', id: "cmdDefend"});
			commands.appendNewElement('br');
			commands.appendNewElement('button', {content: "Flee", classNames: 'combatButton', id: "cmdFlee"});

			appendDiv(cmdDiv, "CombatCommands");
		},
	});

	Macro.add("combatResults", {
		handler() {
			App.UI.appendFormattedText(this.output, {
				text: "Combar Results:",
				style: ["combat-results", "action-general"]
			});
			App.UI.appendNewElement('div', this.output).id = "WinDiv";
			setup.combat.drawResults();
		},
	});
}
