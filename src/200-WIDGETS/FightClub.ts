namespace App.UI.Widgets {
	export class FightClubTable {
		constructor(club: string, maxBet: number, player: Entity.Player) {
			const playerFlag = App.PR.fightClubFlag(club);
			const winsKey = playerFlag + "_WINS";
			const lossesKey = playerFlag + "_LOSSES";
			const wins = player.flags[winsKey] ?? 0;
			const losses = player.flags[lossesKey] ?? 0;

			this._elem = document.createElement('div');
			this._table = App.UI.appendNewElement('table', this._elem);
			this._table.style.width = "600px";

			const tbody = App.UI.appendNewElement('tbody', this._table);

			const headRow = App.UI.appendNewElement('tr', tbody, {classNames: 'section-header'});
			const headCell = App.UI.appendNewElement('td', headRow);
			headCell.colSpan = 2;
			headCell.style.textAlign = 'center';
			App.UI.appendNewElement('span', headCell, {content: `The ${club} Fight Club!`, classNames: 'location-name'});

			const logRow = App.UI.appendNewElement('tr', tbody, {classNames: 'section-header'});
			const logCell = App.UI.appendNewElement('td', logRow,
				{content: `Your Personal Record is ${wins} wins and ${losses} losses.\nYou can choose to bet on, or participate in any of the fights below.`});
			logCell.colSpan = 2;
			logCell.style.textAlign = 'center';

			const skip = App.UI.appendNewElement('td', App.UI.appendNewElement('tr', tbody));
			skip.colSpan = 2;

			const rowJoinFight = App.UI.appendNewElement('tr', tbody);

			const fightLevel = App.UI.appendNewElement('td', rowJoinFight, {content: "Fight Level"});
			fightLevel.style.width = "450px";

			const join = App.UI.appendNewElement('td', rowJoinFight, {content: "Join Fight"});
			join.style.width = "150px";

			FightClubTable._fightClubMenu(player, club, tbody);

			const betDiv = App.UI.appendNewElement('div', this._elem);
			betDiv.style.marginTop = '2ex';
			if (setup.world.phase < DayPhase.LateNight) {
				betDiv.appendTextNode("Or instead of fighting yourself, you could ");
				const link = betDiv.appendNewElement('a', {content: "Place a bet or spectate"});
				betDiv.appendTextNode(" on the next fight.");
				link.addEventListener('click', () => {
					State.variables.gameBookmark = passage();
					setup.spectator.loadEncounter(club, maxBet);
					Engine.play('FightBetUI');
				});
			} else {
				betDiv.textContent = "Fighting is over for the night. Come back next time if you want to participate or place a bet.";
			}
		}

		private static _fightClubMenu(player: Entity.Player, club: string, tableBody: HTMLTableSectionElement) {
			const clubFlag = PR.fightClubFlag(club);
			const winFlag = clubFlag + "_WINS";

			const wins = player.flags[winFlag] as number ?? 0;
			const rows = Data.Combat.clubData[club];

			for (let i = 0; i < rows.length; i++) {
				const r = rows[i];
				const tRow = tableBody.appendNewElement('tr');
				const title = tRow.appendNewElement('td');
				title.appendFormattedText(PR.colorizeString((i + 1), r.title, rows.length));
				const fight = UI.appendNewElement('td', tRow);
				// Fight
				if (wins < r.winsRequired) {
					fight.appendFormattedText({
						text: `Need ${r.winsRequired} wins`,
						style: 'state-disabled',
					});
				} else if (wins > r.maxWins && r.maxWins !== 0) {
					fight.appendFormattedText({
						text: "Too Experienced",
						style: 'attention',
					});
				} else if (setup.world.phase > DayPhase.Night) {
					fight.appendFormattedText({
						text: "CLOSED",
						style: 'state-negative',
					});
				} else {
					const link = fight.appendNewElement('a', {content: 'Fight!'});
					link.addEventListener('click', () => {
						setup.combat.initializeScene({noWeapons: true});
						setup.combat.loadEncounter(r.encounter);
						Engine.play('Combat');
					});
				}
			}
		}

		render(): HTMLDivElement {
			return this._elem;
		}

		private readonly _table: HTMLTableElement;
		private readonly _elem: HTMLDivElement;
	}
}

Macro.add('FightClubMenu', {
	handler() {
		const club = this.args[0] as string;
		const maxBet = this.args[1] as number;
		this.output.append(new App.UI.Widgets.FightClubTable(club, maxBet, setup.world.pc).render());
	},
});
