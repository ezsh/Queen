Macro.add("shipPortLink", {
	handler() {
		const port = setup.world.pc.getShipLocation();
		if (port.passage !== "") {
			const controlQuest = App.Quest.byId("BOARDINGPASS");
			const res = new DocumentFragment();
			const wrappedResult = $(res);
			if (controlQuest.isCompleted(setup.world.pc)) {
				wrappedResult.wiki(App.UI.pTravelLinkStrip([[port.title, port.passage]], "Disembark"));
			} else if (controlQuest.isActive(setup.world.pc)) {
				wrappedResult.wiki(App.UI.pTravelLinkStrip([[port.title, null]], "Disembark"));
				App.UI.appendNewElement('span', res, {content: "(LOCKED)", classNames: 'state-negative'});
			} else {
				App.UI.appendNewElement('span', res, {content: "Disembark", classNames: 'action-travel'});
				res.append(": ");
				const link = App.UI.appendNewElement('a', res, {content: port.title, classNames: 'link-internal'});
				link.addEventListener('click', () => {
					const q = App.Quest.byId("PRE_BOARDINGPASS");
					q.accept();
					q.complete();
					App.UI.replace('#BoardingPass', "As you approach the gangplank to leave the @@.npc;Salty Mermaid@@, two burly armed Pirates bar your way. Even with the magical geas on you, it appears it won't be possible for you to leave the ship without permission.")
				});
				App.UI.appendNewElement('div', res).id = 'BoardingPass';
			}
			this.output.append(res);
		}
	},
});
