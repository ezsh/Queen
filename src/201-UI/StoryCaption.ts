namespace App.UI.Passages {
	export class StoryCaption extends DOMPassage {
		constructor() {
			super("StoryCaption");

			$('<div id="my-story-caption"></div>').appendTo($('#ui-bar-body'));
			// Setup some divs that will not get refreshed on passage navigation.
			if ($('#extended-story-stats').length === 0) {
				const extendedStoryStats = $("<div id='extended-story-stats'></div>").appendTo($('#ui-bar-body'));
				const container = $("<div id='avatarContainer'></div>").appendTo(extendedStoryStats);
				$("<div id='avatarFace'></div").appendTo(container);
				$("<div id='bodyScoreContainer'></div>").appendTo(extendedStoryStats);

				const menu = makeElement('nav', {id: 'menu'});
				// repeat SugarCube ids and classes to apply its CSS
				const menuList = appendNewElement('ul', menu, {id: 'menu-core'});
				const savesItem = appendNewElement('li', menuList, {id: 'menu-item-saves'});
				appendNewElement('a', savesItem, {content: "Saves"}).addEventListener('click', () => {SugarCube.UI.saves();});

				const settingsItem = appendNewElement('li', menuList, {id: 'menu-item-settings'});
				appendNewElement('a', settingsItem, {content: "Settings"}).addEventListener('click', () => {SugarCube.UI.settings();});

				const helpItem = appendNewElement('li', menuList, {id: 'menu-item-help'});
				const helpAnchor = appendNewElement('a', helpItem, {content: "Help", classNames: 'link-internal'});
				helpAnchor.dataset.passage = "Help";
				helpAnchor.addEventListener('click', (ev) => {
					Engine.play((ev.target as HTMLElement).dataset.passage!)
				})

				const restartItem = appendNewElement('li', menuList, {id: 'menu-item-restart'});
				appendNewElement('a', restartItem, {content: "Settings"}).addEventListener('click', () => {SugarCube.UI.restart();});

				$(menu).insertAfter(extendedStoryStats);
			}
		}

		public static updateGameScore(): void {
			const d = $("#game-score");
			d.empty();
			d.append(rGameScore());
		}

		// eslint-disable-next-line class-methods-use-this
		override render() {
			const res = new DocumentFragment();
			if (!State.variables.inIntro) {
				const gameScore = appendNewElement("div", res, {id: 'game-score'});
				gameScore.append(UI.rGameScore());
			}

			return res;
		}
	}
}

new App.UI.Passages.StoryCaption();
