namespace App.UI.Passages {
	class FightBetUI extends DOMPassage {
		constructor() {
			super("FightBetUI");
		}

		render(): DocumentFragment { // eslint-disable-line class-methods-use-this
			const res = document.createDocumentFragment();

			setup.spectator.drawUI();

			appendTextNode(res, "The next fight up is:");
			appendNewElement('br', res);
			const uiDiv = appendNewElement('div', res, {classNames: 'EnemyBetGUI'});
			uiDiv.id = 'EnemyGUI';
			res.append(
				setup.spectator.betALink(),
				document.createElement('br'),
				setup.spectator.betBLink(),
				document.createElement('br')
			);

			appendNewElement("span", res, {content: "Travel: ", classNames: "action-travel"});
			res.append(setup.spectator.watchLink());

			return res
		}
	}

	class FightBetOverUI extends DOMPassage {
		constructor() {
			super("FightBetOverUI");
		}

		render(): DocumentFragment { // eslint-disable-line class-methods-use-this
			const res = document.createDocumentFragment();

			appendTextNode(res, "The fighters step into the ring and and square off against each other…");
			appendNewElement('br', res);
			const title = appendFormattedFragment(res, {text: 'Combat Results:', style: 'state-neutral'});
			title.style.fontSize = 'larger';
			appendNewElement('br', res);
			appendNewElement('div', res).id = "WinDiv";
			setup.spectator.drawResults();
			appendNewElement('br', res);
			appendFormattedFragment(res, {text: setup.spectator.winner, style: 'npc'});
			appendTextNode(res, ' is declared the winner!');
			appendNewElement('br', res);
			const payout = setup.spectator.payoutStr;
			if (payout) {
				res.append(payout);
			}
			appendNewElement('br', res);

			appendNewElement("span", res, {content: "Travel: ", classNames: "action-travel"});
			res.append(UI.passageLink("Continue", State.variables.gameBookmark));

			return res;
		}
	}

	new FightBetUI();
	new FightBetOverUI();

}
