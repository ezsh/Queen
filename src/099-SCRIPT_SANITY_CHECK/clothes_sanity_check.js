(function () {
	if (App == null) {
		alert("App == null!");
		return;
	}
	if (App.Data == null) {
		alert("App.Data == null!");
		return;
	}
	if (App.Data.clothes == null) {
		alert("App.Data.Clothes == null!");
		return;
	}

	for (const clothId in App.Data.clothes) {
		if (App.Data.clothes.hasOwnProperty(clothId)) {
			const clothingItem = App.Data.clothes[clothId];
			if (clothingItem == null) {
				alert("Clothing item \"" + clothId + "\" is null!");
			}
			if (clothingItem.name !== clothId) {
				alert("Clothing item \"" + clothId + "\" has a different name: \"" + clothingItem.name + "\"!");
			}
		}
	}
})();
