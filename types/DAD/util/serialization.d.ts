declare global {
	interface JSON {
	// Utility method to allow users to easily wrap their code in the revive wrapper.
		reviveWrapper(code: string, data:any): string[];
	}
}

// Serialize data into a JSON-encoded string.
export function serialize(value: any): string;
export function deserialize(text: string): any;
export function loadSerialization(): void;
