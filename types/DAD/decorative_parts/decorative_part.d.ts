import {DrawingExports} from "../draw/draw";
import {BasePart} from "../parts/part";
import {Player} from "../player/player";
/**
 * Base class for non-combat parts for show
 */
export class DecorativePart extends BasePart{
	constructor(...data: object[]);
	stroke(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;

    // how thick the stroke line should be
	getLineWidth(avatar: Player): number;
}
