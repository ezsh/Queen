import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {Player} from "../player/player";
import {DecorativePart} from "./decorative_part";

declare class Outline extends DecorativePart {
	constructor(...data: object[]);

	stroke(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
}


declare class MuscleOutline extends Outline {
	constructor(...data: object[]);
	getLineWidth(avatar: Player): number;
}


export class BellyButtonOutline extends Outline {
	constructor(...data: object[]);
	getLineWidth(): number;
	calcDrawPoints(ex: DrawingExports, ignore: any, calculate: boolean): DrawPoint[];
}


export class DeltoidsOutline extends MuscleOutline {
	constructor(...data: object[]);
	calcDrawPoints(ex: DrawingExports, ignore: any, calculate: boolean): DrawPoint[];
}


export class CollarboneOutline extends MuscleOutline {
	constructor(...data: object[]);
	calcDrawPoints(ex: DrawingExports, ignore: any, calculate: boolean): DrawPoint[];
}


export class PectoralOutline extends MuscleOutline {
	constructor(...data: object[]);
	calcDrawPoints(ex: DrawingExports, ignore: any, calculate: boolean): DrawPoint[];
}


export class AbdominalOutline extends MuscleOutline {
	constructor(...data: object[]);
	calcDrawPoints(ex: DrawingExports, ignore: any, calculate: boolean): DrawPoint[];
}


export class QuadricepsOutline extends MuscleOutline {
	constructor(...data: object[]);

	fill(ctx?: CanvasRenderingContext2D, ex?: DrawingExports): string;
	clipFill(): void;
	calcDrawPoints(ex: DrawingExports, ignore: any, calculate: boolean): DrawPoint[];
}
