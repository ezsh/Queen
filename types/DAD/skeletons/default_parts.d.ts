import {
	AbdominalOutline, BellyButtonOutline,
	CollarboneOutline, DeltoidsOutline,
	PectoralOutline,
	QuadricepsOutline
} from "../decorative_parts/outline";
import {EarsHuman} from "../face_parts/ears";
import {BrowHuman} from "../face_parts/eyebrow";
import {EyelashHuman} from "../face_parts/eyelash";
import {EyelidHuman} from "../face_parts/eyelid";
import {EyesHuman} from "../face_parts/eyes";
import {IrisHuman} from "../face_parts/iris";
import {LipsHuman} from "../face_parts/lips";
import {MouthHuman} from "../face_parts/mouth";
import {NoseHuman} from "../face_parts/nose";
import {PupilHuman} from "../face_parts/pupil";
import {ArmHuman} from "../parts/arm";
import {ButtHuman} from "../parts/butt";
import {ChestHuman, NipplesHuman} from "../parts/chest";
import {FeetHuman} from "../parts/feet";
import {GroinHuman} from "../parts/groin";
import {HandHuman} from "../parts/hand";
import {HeadHuman} from "../parts/head";
import {LegHuman} from "../parts/leg";
import {NeckHuman} from "../parts/neck";
import {SideValue} from "../parts/part";
import {PenisHeadHuman, PenisHuman} from "../parts/penis";
import {TesticlesHuman} from "../parts/testicles";
import {TorsoHuman} from "../parts/torso";
import {VaginaHuman} from "../parts/vagina";

/**
 * Create the default body parts
 * Can be modified before loading
 */
export namespace Skeleton {
	export const human: {
		maleParts: [
			{
				partGroup: "parts",
				side: null,
				part: TesticlesHuman,
			},
			{
				partGroup: "parts",
				side: null,
				part: PenisHuman,
			},
			{
				partGroup: "decorativeParts",
				side: null,
				part: PenisHeadHuman,
			},
		],
		femaleParts: [
			{
				partGroup: "parts",
				side: null,
				part: VaginaHuman,
			},
		],
		defaultParts: [
			{
				side: null,
				part: HeadHuman
			},
			{
				side: null,
				part: NeckHuman
			},
			{
				side: SideValue.LEFT,
				part: ArmHuman
			},
			{
				side: SideValue.RIGHT,
				part: ArmHuman
			},
			{
				side: SideValue.LEFT,
				part: HandHuman
			},
			{
				side: SideValue.RIGHT,
				part: HandHuman
			},
			{
				side: null,
				part: TorsoHuman
			},
			{
				side: SideValue.LEFT,
				part: LegHuman
			},
			{
				side: SideValue.RIGHT,
				part: LegHuman
			},
			{
				side: SideValue.LEFT,
				part: FeetHuman
			},
			{
				side: SideValue.RIGHT,
				part: FeetHuman
			},
			{
				side: null,
				part: GroinHuman
			},
			{
				side: null,
				part: ButtHuman
			},
			{
				side: null,
				part: ChestHuman
			},
			{
				side: null,
				part: NipplesHuman
			},
		],
		// face
		defaultFaceParts: [
			{
				side: SideValue.LEFT,
				part: EarsHuman
			},
			{
				side: SideValue.RIGHT,
				part: EarsHuman
			},
			{
				side: null,
				part: NoseHuman
			},
			{
				side: null,
				part: LipsHuman
			},
			{
				side: null,
				part: MouthHuman
			},
			{
				side: SideValue.LEFT,
				part: EyesHuman
			},
			{
				side: SideValue.RIGHT,
				part: EyesHuman
			},
			{
				side: SideValue.LEFT,
				part: IrisHuman
			},
			{
				side: SideValue.RIGHT,
				part: IrisHuman
			},
			{
				side: SideValue.LEFT,
				part: PupilHuman
			},
			{
				side: SideValue.RIGHT,
				part: PupilHuman
			},
			{
				side: SideValue.LEFT,
				part: EyelidHuman
			},
			{
				side: SideValue.RIGHT,
				part: EyelidHuman
			},
			{
				side: SideValue.LEFT,
				part: EyelashHuman
			},
			{
				side: SideValue.RIGHT,
				part: EyelashHuman
			},
			{
				side: SideValue.LEFT,
				part: BrowHuman
			},
			{
				side: SideValue.RIGHT,
				part: BrowHuman
			},
		],
		defaultDecorativeParts: [
			{
				side: null,
				part: BellyButtonOutline
			},
			{
				side: SideValue.LEFT,
				part: DeltoidsOutline
			},
			{
				side: SideValue.RIGHT,
				part: DeltoidsOutline
			},
			{
				side: null,
				part: CollarboneOutline
			},
			{
				side: null,
				part: PectoralOutline
			},
			{
				side: null,
				part: AbdominalOutline
			},
			{
				side: SideValue.LEFT,
				part: QuadricepsOutline
			},
			{
				side: SideValue.RIGHT,
				part: QuadricepsOutline
			},
		]
	}
}
