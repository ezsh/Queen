import {clone} from "drawpoint/dist-esm";

/**
 * Facial expressions
 * @namespace Expression
 * @memberof module:da
 */
export namespace Expression {

	interface ExpressionObject {
		Mods: {
			[x: string]: number;
		}
	}

    /**
     * Create an expression to a certain degree
     * @memberof module:da
     * @param {object} expression Expression object with modifiers in Mods
     * @param {number} degree How fully the expression should be displayed; > 0
     * @returns {object} The expression to be applied
     */
	export function create(expression: ExpressionObject, degree?: number): ExpressionObject;
	export const neutral: {Mods: {}};
	export const suspicious: {
		Mods: {
			eyeBotSize: -7,
			browHeight: -2,
			lipBotSize: -20,
			lipTopSize: -8,
			lipWidth: -25,
		}
	};

	export const angry: {
		Mods: {
			browTilt: 7,
			browCloseness: -3,
			browHeight: -4,
			eyeBotSize: 1,
			eyeTopSize: 1,
			lipCurl: -6,
		},
	};

	export const sad: {
		Mods: {
			browTilt: -7,
			eyeBotSize: 2,
			eyeTopSize: -1,
			lipCurl: -5,
			eyeTilt: -1,
		}
	};

	export const surprised: {
		Mods: {
			browTilt: -5,
			eyeBotSize: 2,
			eyeTopSize: 1,
			lipParting: 15,
			browHeight: 5,
		},
	};

	export const mischievous: {
		Mods: {
			lipCurl: 10,
			eyeBotSize: -4,
			eyeTopSize: -1,
		},
	};

	export const happy: {
		Mods: {
			lipCurl: 12,
			eyeBotSize: -2,
		},
	};

	export const sleepy: {
		Mods: {
			eyeTopSize: -4,
			eyelidHeight: -1,
		},
	};

	export const aroused: {
		Mods: {
			eyelidHeight: -2.5,
			lipParting: 13,
		},
	};

	export const bliss: {
		Mods: {
			irisHeight: 2,
			lipParting: 20,
			eyelidHeight: -2,
		},
	};
}
