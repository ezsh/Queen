import {styles} from "../hair_parts/styles";
import {Location} from "../util/part";
import {Player} from "./player";

/**
 * Any physical property that needs calculation and is tangible should go in here
 */
export const baseDimDesc:  {
    human: {
        areolaSize     : {
            linkedPart: Location.CHEST,
            units      :"mm",
            low       : 0,
            high      : 50,
            avg       : 20,
            stdev     : 2,
            bias      : 3,
			calc(this: Player): number;
        },
        armThickness   : {
            linkedPart: Location.ARM,
            units     : "mm",
            low       : 45,
            high      : 95,
            avg       : 65,
            stdev     : 3,
            bias      : -7,
            calc(this: Player): number;
        },
        armLength      : {
            linkedPart: Location.ARM,
            units     : "cm",
            low       : 30,
            high      : 80,
            avg       : 45,
            stdev     : 2,
            bias      : 0,
            calc(this: Player): number;
        },
        bellyProtrusion: {
            linkedPart: Location.TORSO,
            units     : "cm",
            low       : 0,
            high      : 60,
            avg       : 0,
            stdev     : 0,
            bias      : 0,
            calc(this: Player): number;
        },
        breastSize     : {
            linkedPart: Location.CHEST,
            units     : "cm",
            low       : -10,
            high      : 50,
            avg       : -1,
            stdev     : 5,
            bias      : 20,
            calc(this: Player): number;
        },
        buttFullness   : {
            linkedPart: Location.BUTT,
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 10,
            stdev     : 2,
            bias      : 4,
            calc(this: Player): number;
        },
        chinWidth      : {
            linkedPart: Location.HEAD,
            units     : "mm",
            low       : 30,
            high      : 140,
            avg       : 70,
            stdev     : 2,
            bias      : -2
        },
        eyelashLength  : {
            linkedPart: Location.EYELASH,
            units     : "mm",
            low       : 0,
            high      : 15,
            avg       : 3,
            stdev     : 2,
            bias      : 3,
        },
        eyeSize        : {
            linkedPart: Location.EYES,
            units     : "mm",
            low       : 0,
            high      : 40,
            avg       : 15,
            stdev     : 2,
            bias      : 3,
        },
        faceFem        : {
            linkedPart: Location.HEAD,
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 15,
            stdev     : 2,
            bias      : 14,
            calc(this: Player): number;
        },
        // how long is your face
        faceLength     : {
            linkedPart: Location.HEAD,
            units     : "mm",
            low       : 180,
            high      : 270,
            avg       : 230,
            stdev     : 5,
            bias      : -2,
        },
        // defined as relative to the center of the face (so face half width)
        faceWidth      : {
            linkedPart: Location.HEAD,
            units     : "mm",
            low       : 75,
            high      : 105,
            avg       : 93,
            stdev     : 2,
            bias      : -2,
            calc(this: Player): number;
        },


        hairLength: {
            linkedPart: Location.HAIR,
            units     : "cm",
            low       : 0,
            high      : 110,
            avg       : 6,
            stdev     : 2,
            bias      : 10,
            calc(this: Player): number;
        },
        hairStyle : {
            linkedPart: Location.HAIR,
            desc      : "Index of base hair style to be worn (individual parts can be swapped afterwards)",
            units     : "index",
            low       : 0,
            high      : number,
            avg       : number,
            stdev     : 2,
            bias      : 0,
            // needs to be integer
			calc(this: Player): number;
        },


        hairHue       : {
            linkedPart: Location.HAIR,
            units     : "degree",
            low       : 0,
            high      : 360,
            avg       : 30,
            stdev     : 30,
            bias      : 0,
        },
        hairSaturation: {
            linkedPart: Location.HAIR,
            units     : "%",
            low       : 0,
            high      : 100,
            avg       : 50,
            stdev     : 10,
            bias      : 0,
        },
        hairLightness : {
            linkedPart: Location.HAIR,
            units     : "%",
            low       : 0,
            high      : 100,
            avg       : 30,
            stdev     : 10,
            bias      : 0,
        },
        handSize      : {
            linkedPart: Location.HAND,
            units     : "mm",
            low       : 0,
            high      : 200,
            avg       : 100,
            stdev     : 5,
            bias      : -10,
            calc(this: Player): number;
        },
        height        : {
            units: "cm",
            low  : 50,
            high : 270,
            avg  : 167,
            stdev: 3,
            bias : -5,
        },
        hipWidth      : {
            linkedPart: Location.TORSO,
            units     : "mm",
            low       : 70,
            high      : 200,
            avg       : 125,
            stdev     : 2.5,
            bias      : 3,
            calc(this: Player): number;
        },


        legFem     : {
            linkedPart: Location.LEG,
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 13,
            stdev     : 2,
            bias      : 9,
            calc(this: Player): number;
        },
        legFullness: {
            linkedPart: Location.LEG,
            desc      : "Approximately how thick the leg is as a combination of fat and muscle",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 10,
            stdev     : 3,
            bias      : 2,
            calc(this: Player): number;
        },
        legLength  : {
            linkedPart: Location.LEG,
            units     : "cm",
            low       : 20,
            high      : 200,
            avg       : 95,
            stdev     : 2.5,
            bias      : 4,
            calc(this: Player): number;
        },
        lipSize    : {
            linkedPart: Location.LIPS,
            units     : "mm",
            low       : 0,
            high      : 40,
            avg       : 14,
            stdev     : 1.5,
            bias      : 2,
            calc(this: Player): number;
        },
        lowerMuscle: {
            linkedPart: Location.LEG,
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 16,
            stdev     : 3,
            bias      : -17,
            calc(this: Player): number;
        },


        neckLength: {
            linkedPart: Location.NECK,
            units     : "mm",
            low       : 0,
            high      : 120,
            avg       : 72,
            stdev     : 5,
            bias      : 0,
        },
        neckWidth : {
            linkedPart: Location.NECK,
            units     : "mm",
            low       : 35,
            high      : 75,
            avg       : 45,
            stdev     : 2,
            bias      : -2,
        },


        penisSize    : {
            linkedPart: Location.GENITALS,
            units     : "mm",
            low       : 0,
            high      : 200,
            avg       : 20,
            stdev     : 5,
            bias      : -20,
            calc(this: Player): number;
        },
        shoulderWidth: {
            linkedPart: Location.CHEST,
            units     : "mm",
            low       : 0,
            high      : 200,
            avg       : 73,
            stdev     : 2,
            bias      : -7,
            calc(this: Player): number;
        },
        // translucent (-20) to porcelein (-10) to fair (-5) to tanned (5) to brown (15) pure
        // black (50)
        skin         : {
            units: "arbitrary",
            low  : -20,
            high : 50,
            avg  : 7,
            stdev: 5,
        },
        testicleSize : {
            linkedPart: Location.GENITALS,
            units     : "mm",
            low       : 0,
            high      : 100,
            avg       : 35,
            stdev     : 4,
            bias      : -20,
            calc(this: Player): number;
        },
        upperMuscle  : {
            linkedPart: Location.CHEST,
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 14,
            stdev     : 3,
            bias      : -15,
            calc(this: Player): number;
        },
        vaginaSize   : {
            linkedPart: Location.GENITALS,
            units     : "mm",
            low       : 0,
            high      : 100,
            avg       : 40,
            stdev     : 10,
            bias      : 5,
            calc(this: Player): number;
        },
        waistWidth   : {
            linkedPart: Location.TORSO,
            units     : "mm",  //(center to closest point horizontally)
            low       : 70,
            high      : 150,
            avg       : 120,
            stdev     : 3,
            bias      : -3,
            calc(this: Player): number;
        },
    },
};

// first element of discrete pools is the default
export const basedimDiscretePool: {
	eyecolor: ["white"],
};

/**
 * Dimension calculation based on avatar statistics. These are callback methods that
 * the user should define to link extended gameplay stats on the Player to physical dimensions.
 * @this avatar object
 * @param  base this dimension as calculated by all previous calculations
 */
type dimensionCalculation = (this: Player, base: number) => number;

/**
 * Extend the way a dimension is calculated to plug in a user statistics system
 * @param dimDesc Either the dimension descriptor object, or
 * a string in the format of "skeleton.dimension"
 * @param newCalc User method for calculating dimension
 */
export function extendDimensionCalc(dimDesc: string | object, newCalc: dimensionCalculation): ReturnType<dimensionCalculation>;

export function loadDimensionDescriptions(): void;
