import {Player} from "../player/player";
import {DrawingExports} from "../draw/draw";

declare interface ItemData {
	name: string;
	src: string;
}

/**
 * Items are like clothing but do not need to be dynamically drawn
 * They do not scale in form with body dimensions, so can be predrawn in svg, png, and other forms
 * Positioning and sizing of an item optionally depends on draw exports (ie. relative to parts of body)
 * May have body modifiers under Mods property
 */
export class Item implements ItemData{
	constructor(...data: ItemData[]);

	name: string;
	src: string;
    /**
     * Return the starting location for drawing the item
     * @param ex Drawing exports
     */
	renderItemLocation(ex:DrawingExports, width?: number, height?: number): null | {x: number, y: number};

    /**
     * Modify the Player pose (getPartInLocation inside function and modify pose Mods directly)
     */
	modifyPose(this: Player): void;
    /**
     * Restore the effect of modifying pose when item was first wielded
     * @this Player
     */
	restorePose(this: Player): void;
}

export namespace Items {
	export function getItemRender(item: Item): any;

    /**
     * Similar to getItem, load the resource if not cached
     * @param item
     */
	export function loadItem(item: Item): void;

	interface ItemConstructor {
		new(...data: ItemData[]): Item;
	}
    /**
     * Create a Item instance
     * @memberof module:da.Items
     * @param {Item} Item Item prototype to instantiate
     * @param {object} data Overriding data
     * @returns {Item} Instantiated clothing object
     */
	export function create(item: ItemConstructor, ...data: ItemData[]): Item;
}
