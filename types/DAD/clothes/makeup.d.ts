import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class MascaraPart extends ClothingPart {
	constructor(...data: object[]);
	topFill: string;
	botFill: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class Makeup extends Clothing {
	constructor(...data: object[]);
}


export class Mascara extends Makeup {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
