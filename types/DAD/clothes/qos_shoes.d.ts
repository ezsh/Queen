import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";


export class LeftBaseBootShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


export class RightBaseBootShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}

export class BootBasePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class QueenShoes extends Clothing {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
}

export class QueenBoots extends QueenShoes {
	constructor(...data: object[]);

	legCoverage: number;
	shoeHeight: number;
	tongueDeflection: number;
	toeHeight: number;
	Mods: {
		feetLength: number;
		feetWidth: number;
	}

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
