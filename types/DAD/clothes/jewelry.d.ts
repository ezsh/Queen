import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";


/**
 * ClothingPart drawn classes/components
 */
export class RingBotPart extends ClothingPart {
	constructor(...data: object[]);

	raduis: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class RingTopPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class StudPart extends ClothingPart {
	constructor(...data: object[]);
	radius: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class ChainPart extends ClothingPart {
	constructor(...data: object[]);

	thickness: number;
	slack: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Jewelry extends Clothing {
	constructor(...data: object[]);

	//stroke: string;
	//fill: string;
}

export class Piercing extends Jewelry {
	constructor(...data: object[]);

	rotation: number;
}
/**
 * Concrete Clothing classes
 */
export class RingPiercing extends Piercing {
	constructor(...data: object[]);
	relativeLocation: {
		drawpoint: "ear.mid",
		dx: 0,
		dy: 0
	};

	requiredParts: string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class StudPiercing extends Piercing {
	constructor(...data: object[]);
	relativeLocation: {
		drawpoint: "nose.out",
		dx: 0,
		dy: 0
	};

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ChainJewelry extends Piercing {
	constructor(...data: object[]);

	relativeLocation: {
		drawpoint: "nose.out",
		dx: 0,
		dy: 0
	};
	endRelativeLocation: {
		drawpoint: "ear.mid",
		dx: 0,
		dy: 0
	};

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
