import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class SkirtBasePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}
/**
 * Base Clothing classes
 */
export class QueenBottoms extends Clothing {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
}

export class QueenSkirt extends QueenBottoms {
	constructor(...data: object[]);

	waistCoverage: number;
	legCoverage: number;
	legLoose: number;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
