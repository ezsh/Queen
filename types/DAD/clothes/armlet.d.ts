import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Jewelry} from "./jewelry";


export class SimpleArmletPart extends ClothingPart {
	constructor(...data: object[]);

	armCoverageTop: number;
	armCoverage: number;
	thickness: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class CrossedArmletPart extends ClothingPart {
	constructor(...data: object[]);

	armCoverageTop: number;
	armCoverage: number;
	crossings: number;
	thickness: number;
	doubled: boolean;
	fullArmlet: boolean;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class SpiralArmletPart extends ClothingPart {
	constructor(...data: object[]);

	armCoverage: number;
	coils: number;
	distance: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Armlet extends Jewelry {
	constructor(...data: object[])
}
/**/



export class SpiralArmlet extends Armlet {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class SimpleArmlet extends Armlet {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class CrossedArmlet extends Armlet {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
