import {
	DrawPoint,
	Point
} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Hat} from "./hats";


//TODO - shading
export class MagicHatFrontPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class MagicHatBackPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export function calcMagicHat(ex: DrawingExports): {
	center: Point,
	inBot: DrawPoint,
	out: DrawPoint,
	inTop: DrawPoint,
	coneBase: DrawPoint,
	coneTop: DrawPoint,
	forhead: DrawPoint
};


export class MaidHeadpiecePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/*

*/

export class MagicHat extends Hat {
	constructor(...data: object[]);

	centerOffset: number;
	brimWidth: number;
	brimAngle: number;
	coneHeight: number;
	coneWidth: number;

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class MaidHeadpiece extends Hat {
	constructor(...data: object[]);

	centerOffset: number;
	height: number;
	width: number;
	drop: number;

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
