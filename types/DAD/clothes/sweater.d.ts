import {Clothes, ClothingPart, Clothing, ClothingPartConstructor} from "./clothing";
import {Part, SideValue} from "../parts/part";
import {connectEndPoints, coverNipplesIfHaveNoBreasts, DrawingExports} from "../draw/draw";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    none,
    splitCurve,
    adjust,
    clone,
    breakPoint,
	Point,
	DrawPoint,
} from "drawpoint/dist-esm";

/**
 * ClothingPart drawn classes/components
 */
export class SweaterBasePart extends ClothingPart {
    constructor(...data: object[]);

    renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Calculate the drawpoints for the torso base of a sweater
 * @this {ClothingPart}
 * @param {number} this.neckCoverage [0,1] proportion of the neck that's covered by the sweater
 * @param {number} this.stomachCoverage [0,1] proportion of the stomach that's covered
 * @param {number} this.thickness how defined the outline should be
 * @param ex
 * @returns {{top: {x: number, y: number}, out: *|p2|{x, y}|l|i, cusp: *, collarbone: *, waist: *, bot: {}}}
 */
export function calcSweaterBase(ex: DrawingExports): {
	top: Point
	out: DrawPoint,
	cusp: Point,
	collarbone: DrawPoint,
	waist: DrawPoint
	bot: DrawPoint
};

export class CoveredBreastPart extends ClothingPart {
    constructor(...data: object[]);

    renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Long sleeve means between elbow and wrist
 */
export class LongSleevePart extends ClothingPart {
    constructor(...data: object[]);
	sleeveLength: number;

    renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Calculate the draw points for a long sleeve part
 * @param ex
 * @returns {{collarbone: ({x, y}|*), deltoids: *, shoulder: Object, elbowOut: Object, out, bot, elbow, armpit: Object}}
 */
export function calcLongSleeve(ex: DrawingExports): {
	collarbone:Point,
	deltoids: any;
	shoulder: DrawPoint;
	elbowOut: DrawPoint;
	out: DrawPoint;
	bot: DrawPoint;
	elbow: DrawPoint;
	armpit: DrawPoint;
};

/**
 * Short sleeve means between shoulder and elbow
 */
export class ShortSleevePart extends ClothingPart {
    constructor(...data: object[]);

    renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class Sweater extends Clothing {
    constructor(...data: object[]);

	/**
	 * How much of the stomach should be covered (1 means fully)
	 */
	stomachCoverage: number;
	/**
	 * How much of the neck will the collar cover
	 */
	neckCoverage: number;
	/**
	 * How far to extend the sleeve (between 0 and 1)
	 */
	sleeveLength: number;

}


/**
 * Concrete Clothing classes
 */
export class LongSleevedSweater extends Sweater {
    constructor(...data: object[]);

	stroke(): string;
	fill(): string;
    get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class AsymmetricSleevedSweater extends Sweater {
	constructor(...data: object[]);

    stroke(): string;
	fill(): string;
    get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
