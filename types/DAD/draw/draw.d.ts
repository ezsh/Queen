import * as dp from 'drawpoint/dist';
import {BasePart, PartSideNumeric, SideValue} from "../parts/part"
import {Player} from '../player/player';

//export as namespace da;

/* Copyright 2016 Johnson Zhong */

export interface DrawUserConfig {
	nameColor?: string;
	genderColor?: string;
	heightColor?: string;
	heightBarColor?: string;
	printAdditionalInfo?: boolean;
	printVitals?: boolean;
	printHeight?: boolean;
	renderShoeSideView?: boolean;
	offsetX?: number;
	offsetY?: number;
	center?: dp.Point;
	width?: number;
	height?: number;
}

export const defaultConfig: {
    nameColor          : "#000",
    genderColor        : "#000",
    heightColor        : "#000",
    heightBarColor     : "#000",
    printAdditionalInfo: true,
    printVitals        : true,
    printHeight        : true,
    renderShoeSideView : true,
    offsetX            : 0,
    offsetY            : 0,
}

export interface DrawingExportsGeneric<TPlayer extends Player> {
	avatar: TPlayer;
	cx: number;
	cy: number;
	height: number;
	width: number;
	canvasGroup: HTMLElement[];
	clip: object;
	ctx: CanvasRenderingContext2D;
	ctxGroup: CanvasRenderingContext2D[];
	[SideValue.LEFT]: any;
	[SideValue.RIGHT]: any;
}

export type DrawingExports = DrawingExportsGeneric<Player>;

type DrawnParts = Record<string, dp.DrawPoint[]>;

type CustomDrawingFunction = (canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D, config: DrawUserConfig, avatar: Player) => void;

/**
 * Draw an avatar to a canvasGroup
 * @param canvasGroupObj HTML DOM element holding all the canvases, gotten with
 * da.getCanvasGroup
 * @param avatar Player object to draw
 * @param userConfig Configuration object to override defaultConfig
 * @param customDrawingFunction {function} Custom function for drawing on the base layer
 * @returns Exports
 */
export function draw<TPlayer extends Player>(canvasGroupObj: HTMLElement, avatar: TPlayer, userConfig?: DrawUserConfig,
	customDrawingFunction?: CustomDrawingFunction|CustomDrawingFunction[]): Promise<DrawingExportsGeneric<TPlayer>>;
export function renderBase(ctxGroup: CanvasRenderingContext2D[], ex: DrawingExports): void;
export function renderParts(ctxGroup: CanvasRenderingContext2D[], ex: DrawingExports, config: DrawUserConfig): void;
export function drawPart(ex: DrawingExports, parts: BasePart[], partIndex: number, layer: number, side: PartSideNumeric,
	drawnParts: DrawnParts): dp.DrawPoint[];

/**
 * draw all avatar body parts on this layer and side (but not stroke or fill it)
 * returns all drawpoints for this layer and side
 */
export function drawPartsLayer(ex: DrawingExports, layer: number, side: PartSideNumeric, ctx: CanvasRenderingContext2D): dp.DrawPoint[];
export function connectEndPoints(firstPoint: dp.Point, lastPoint: dp.Point, deflection?:number): dp.Point;
export function coverNipplesIfHaveNoBreasts(ex: DrawingExports, ctx: CanvasRenderingContext2D, part: BasePart): boolean;
export function drawFocusedWindow(focusedCanvas: HTMLCanvasElement, ex: DrawingExports, userConfig: DrawUserConfig): void;
