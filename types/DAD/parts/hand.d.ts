import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {BodyPart} from "./part";

declare class Hand extends BodyPart {
	constructor(...data: object[]);
}


// hand exports hand.knuckle, hand.tip, hand.palm, thumb.tip, thumb.out
export class HandHuman extends Hand {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
