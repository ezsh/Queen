import stylisticJs from "@stylistic/eslint-plugin-js";
import stylisticTs from "@stylistic/eslint-plugin-ts";
import typescriptEslint from "@typescript-eslint/eslint-plugin";
import globals from "globals";
import tsParser from "@typescript-eslint/parser";
import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all
});

export default [{
    ignores: ["node_modules/**/*", "dist/**/*", "types/**/*.ts"],
}, ...compat.extends(
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:you-dont-need-lodash-underscore/compatible",
    // "plugin:sonarjs/recommended",
    "plugin:unicorn/recommended",
), {
    plugins: {
        "@stylistic/js": stylisticJs,
        "@stylistic/ts": stylisticTs,
        "@typescript-eslint": typescriptEslint,
    },

    languageOptions: {
        globals: {
            ...globals.browser,
        },

        parser: tsParser,
        ecmaVersion: "latest",
        sourceType: "script",

        parserOptions: {
            tsconfigRootDir: "",
            project: ["./tsconfig.json"],
        },
    },

    rules: {
        eqeqeq: ["error", "always", {
            null: "ignore",
        }],

        "init-declarations": "off",
        "no-var": "error",
        "prefer-const": "error",
        "@typescript-eslint/init-declarations": ["error"],
        "@typescript-eslint/ban-ts-comment": "warn",

        "@typescript-eslint/naming-convention": ["error", {
            format: ["camelCase"],
            selector: "default",
        }, {
            format: ["camelCase"],
            leadingUnderscore: "allow",
            selector: "parameter",
        }, {
            format: ["camelCase"],
            leadingUnderscore: "require",
            modifiers: ["private"],
            selector: "memberLike",
        }, {
            format: ["PascalCase"],
            selector: "typeLike",
        }, {
            format: ["StrictPascalCase"],
            selector: "enumMember",
        }, {
            format: ["camelCase"],
            selector: "objectLiteralProperty",
        }, {
            selector: "classProperty",
            modifiers: ["static", "readonly"],

            filter: {
                regex: "^(_|#).+",
                match: false,
            },

            format: ["UPPER_CASE"],
        }, {
            selector: "classProperty",
            modifiers: ["static", "readonly", "private", "#private"],

            filter: {
                regex: "^(_|#).+",
                match: false,
            },

            format: ["camelCase"],
        }],

        "@typescript-eslint/no-namespace": "off",

        "@typescript-eslint/no-unused-vars": ["error", {
            argsIgnorePattern: "^_",
            varsIgnorePattern: "^App$|_",
        }],

        "@stylistic/js/array-bracket-newline": ["error", {
            multiline: true,
        }],

        "@stylistic/js/array-bracket-spacing": ["error", "never"],

        "@stylistic/js/arrow-parens": ["error", "as-needed", {
            requireForBlockBody: true,
        }],

        camelcase: "off",
        "class-methods-use-this": "warn",

        "dot-notation": ["error", {
            allowPattern: "^([A-Z]+)|([A-Za-z]+(_[A-Za-z])+)+$",
        }],

        "@stylistic/js/eol-last": ["error", "always"],
        "@stylistic/js/implicit-arrow-linebreak": ["error", "beside"],

        "@stylistic/ts/indent": ["error", "tab", {
            ArrayExpression: 1,
            flatTernaryExpressions: true,
            SwitchCase: 1,
        }],

        "@stylistic/js/key-spacing": ["error", {
            mode: "strict",
        }],

        "@stylistic/js/keyword-spacing": "off",

        "@stylistic/ts/keyword-spacing": ["error", {
            before: true,
            after: true,
        }],

        "@stylistic/js/lines-between-class-members": "off",

        "@stylistic/ts/lines-between-class-members": ["error", "always", {
            exceptAfterSingleLine: true,
        }],

        "@stylistic/js/max-len": ["warn", {
            code: 150,
            ignoreStrings: true,
            ignoreTemplateLiterals: true,
            ignoreUrls: true,
        }],

        "@stylistic/js/no-mixed-spaces-and-tabs": ["error", "smart-tabs"],

        "@stylistic/js/no-multiple-empty-lines": ["error", {
            max: 1,
        }],

        "no-unused-vars": "off",

        "@stylistic/js/object-curly-newline": ["error", {
            ObjectExpression: {
                multiline: true,
            },

            ObjectPattern: {
                multiline: true,
            },

            ImportDeclaration: "never",

            ExportDeclaration: {
                multiline: true,
                minProperties: 3,
            },
        }],

        "@stylistic/js/object-curly-spacing": "off",
        "@stylistic/ts/object-curly-spacing": "error",
        "@stylistic/js/padded-blocks": ["error", "never"],
        "prefer-arrow-callback": "error",
        "@stylistic/js/quote-props": ["error", "as-needed"],

        "@stylistic/js/space-before-function-paren": ["error", {
            anonymous: "always",
            named: "never",
        }],

        "@stylistic/js/spaced-comment": ["error", "always", {
            block: {
                balanced: true,
                exceptions: ["*"],
                markers: ["!"],
            },

            line: {
                exceptions: ["-", "+", "="],
                markers: ["/", "#region", "#endregion"],
            },
        }],

        "@stylistic/js/comma-dangle": ["warn", {
            arrays: "always-multiline",
            objects: "always-multiline",
            imports: "never",
            exports: "never",
            functions: "never",
        }],

        "@typescript-eslint/unbound-method": ["error", {
            ignoreStatic: true,
        }],

        "@typescript-eslint/no-floating-promises": ["error", {
            ignoreVoid: true,
        }],

        "@typescript-eslint/no-empty-interface": ["error", {
            allowSingleExtends: true,
        }],

        "@typescript-eslint/prefer-nullish-coalescing": "warn",
        "@typescript-eslint/prefer-optional-chain": "warn",
        "@typescript-eslint/prefer-readonly": "error",
        "@typescript-eslint/prefer-string-starts-ends-with": "warn",
        "@typescript-eslint/prefer-ts-expect-error": "error",
        "@stylistic/ts/type-annotation-spacing": "warn",

        "@typescript-eslint/restrict-template-expressions": ["error", {
            allowNumber: true,
            allowBoolean: true,
            allowAny: true,
            allowNullish: true,
        }],

        "@stylistic/js/comma-spacing": "off",
        "@stylistic/ts/comma-spacing": ["warn"],
        "@stylistic/js/no-trailing-spaces": "error",
        "@stylistic/js/brace-style": "off",

        "@stylistic/ts/brace-style": ["error", "1tbs", {
            allowSingleLine: true,
        }],

        "@typescript-eslint/no-unnecessary-boolean-literal-compare": "error",
        "sonarjs/cognitive-complexity": ["warn"],
        "sonarjs/no-small-switch": "off",
        "sonarjs/no-nested-template-literals": "off",
        "unicorn/better-regex": ["warn"],
        "unicorn/no-array-for-each": ["warn"],
        "unicorn/no-array-reduce": "off",
        "unicorn/no-array-callback-reference": "off",
        "unicorn/filename-case": "off",
        "unicorn/no-for-loop": ["warn"],
        "unicorn/prefer-query-selector": "off",
        "unicorn/no-null": "off",
        "unicorn/no-zero-fractions": "off",

        "unicorn/numeric-separators-style": ["error", {
            onlyIfContainsSeparator: true,
        }],

        "unicorn/switch-case-braces": ["error", "avoid"],
        "unicorn/prefer-spread": "off",

        "unicorn/prevent-abbreviations": ["error", {
            allowList: {
                i: true,
                j: true,
                ev: true,
                arg: true,
                args: true,
                ctx: true,
                mod: true,
                obj: true,
                res: true,
                str: true,
                Str: true,
                Tit: true,
            },
        }],

        "you-dont-need-lodash-underscore/capitalize": "off",
    },
}];
